package com.app.MagicOfBooksUsingSpringMVC.pojo;

public class Book 
{
	private int bookId;
	private String bookName;
	private String authorName;
	private String description;
	private String genre;
	private double price;
	private int noOfCopies;

	public int getBookId()
	{
		return bookId;
	}

	public void setBookId(int bookId) 
	{
		this.bookId = bookId;
	}

	public String getBookName()
	{
		return bookName;
	}

	public void setBookName(String bookName)
	{
		this.bookName = bookName;
	}

	public String getAuthorName() 
	{
		return authorName;
	}

	public void setAuthorName(String authorName) 
	{
		this.authorName = authorName;
	}

	public String getDescription() 
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getGenre()
	{
		return genre;
	}

	public void setGenre(String genre)
	{
		this.genre = genre;
	}

	public double getPrice() 
	{
		return price;
	}

	public void setPrice(double price)
	{
		this.price = price;
	}

	public int getNoOfCopies() 
	{
		return noOfCopies;
	}

	public void setNoOfCopies(int noOfCopies) 
	{
		this.noOfCopies = noOfCopies;
	}

}
