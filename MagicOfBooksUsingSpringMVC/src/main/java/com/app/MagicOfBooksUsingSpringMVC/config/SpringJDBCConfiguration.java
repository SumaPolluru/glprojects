package com.app.MagicOfBooksUsingSpringMVC.config;

import javax.sql.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import com.app.MagicOfBooksUsingSpringMVC.dao.AdminDAO;
import com.app.MagicOfBooksUsingSpringMVC.dao.AdminDAOImpl;
import com.app.MagicOfBooksUsingSpringMVC.dao.BookDAOImpl;

@SuppressWarnings("deprecation")
@Configuration
@ComponentScan(basePackages = "com.app.MagicOfBooksUsingSpringMVC") //Here,Defining base package instead of in dispatch servlet.xml
@EnableWebMvc
public class SpringJDBCConfiguration extends WebMvcConfigurerAdapter
{
	/*By using WebMvcConfigureAdapter, It can override the methods which u want from subclasses
	 * No need to define in dispatch-servlet.xml
	 */

	@Bean
	public ViewResolver getViewResolver() //web application directions to use of the resources(jsp)
	{ 
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/jsp/");
		resolver.setSuffix(".jsp");
		return resolver;
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}

	@Bean
	public DataSource getDataSource()  //DataConnection JDBC to Web application
	{ 
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/Bookmanagement");
		dataSource.setUsername("root");
		dataSource.setPassword("@Chintu12345");

		return dataSource;
	}

	@Bean
	public AdminDAO getAdminDAO() 
	{ 
		return new AdminDAOImpl(getDataSource());
	}

	@Bean
	public BookDAOImpl getBookDAO() 
	{
		return new BookDAOImpl(getDataSource());
	}
}
