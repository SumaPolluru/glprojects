package com.app.MagicOfBooksUsingSpringMVC.dao;
import java.util.List;


import com.app.MagicOfBooksUsingSpringMVC.pojo.Book;

public interface BookDAO  //Methods for Book operations 
{
	public Book retrieve(int bookId);

	public List<Book> list();

	public void update(Book book);

	public void delete(int bookId);
}
