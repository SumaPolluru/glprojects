package com.app.MagicOfBooksUsingSpringMVC.dao;

import java.util.List;
import com.app.MagicOfBooksUsingSpringMVC.pojo.Admin;
import com.app.MagicOfBooksUsingSpringMVC.pojo.User;

public interface AdminDAO 
{
	public User retrieve(int userId); //For User Operations performed by Admin

	public List<User> list();

	public void update(User user); 

	public void delete(int userId);

	public void register(Admin admin);  //Admin Login operations

	public Admin validateAdmin(Admin admin);

}
