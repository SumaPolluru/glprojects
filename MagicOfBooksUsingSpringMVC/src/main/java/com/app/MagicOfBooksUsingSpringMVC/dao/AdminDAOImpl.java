package com.app.MagicOfBooksUsingSpringMVC.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import com.app.MagicOfBooksUsingSpringMVC.pojo.Admin;
import com.app.MagicOfBooksUsingSpringMVC.pojo.User;

public class AdminDAOImpl implements AdminDAO {
	
	private JdbcTemplate jdbcTemplate;

	public AdminDAOImpl(DataSource dataSource) 
	{
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
    @Override
	public void register(Admin admin) 
    {
		String sql = "INSERT INTO admin (adminName,password)" + " VALUES (?, ?)";
		jdbcTemplate.update(sql, admin.getAdminName(), admin.getPassword());
	}
    @Override
    public Admin validateAdmin(Admin admin) 
    {
    	String sql="select * from admin where adminname='"+ admin.getAdminName() + "' and password='" + admin.getPassword() + "'";
    	List<Admin> adminobj=jdbcTemplate.query(sql, new AdminMapper());
    	
		return adminobj.size()>0 ? adminobj.get(0) : null;
    	
    }
    
	@Override
	public void update(User user)  //Performing both operations in single method due to using single jsp file
	{
		if (user.getUserId() > 0) 
		{
			// update
			String sql = "UPDATE user SET username=?, email=?,noOfBooksBought=? " + " WHERE userId=?";
			jdbcTemplate.update(sql, user.getUserName(), user.getEmail(), user.getNoOfBooksBought(),user.getUserId());
		} else 
		{
			// insert
			String sql = "INSERT INTO user (userName,email,noOfBooksBought)" + " VALUES (?,?,?)";
			jdbcTemplate.update(sql, user.getUserName(), user.getEmail(),user.getNoOfBooksBought());
		}

	}

	@Override
	public void delete(int userId) 
	{
		String sql = "DELETE FROM user WHERE userId=?";
		jdbcTemplate.update(sql, userId);
	}

	@Override
	public List<User> list() 
	{
		String sql = "SELECT * FROM User order by userId";
		List<User> listUser = jdbcTemplate.query(sql, new RowMapper<User>() 
		{
			@Override
			public User mapRow(ResultSet rs, int rowNum) throws SQLException 
			{
				User aobj = new User();
				aobj.setUserId(rs.getInt("userId"));
				aobj.setUserName(rs.getString("userName"));
				aobj.setEmail(rs.getString("email"));
				aobj.setNoOfBooksBought(rs.getInt("noOfBooksBought"));
				return aobj;
			}

		});
		return listUser;
	}

	@Override
	public User retrieve(int userId)
	{
		String sql = "SELECT * FROM User WHERE userId=" + userId +" order by userId";
		return jdbcTemplate.query(sql, new ResultSetExtractor<User>()
		{
			@Override
			public User extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) 
				{
					User aobj = new User();
					aobj.setUserId(rs.getInt("userId"));
					aobj.setUserName(rs.getString("userName"));
					aobj.setEmail(rs.getString("email"));
					aobj.setNoOfBooksBought(rs.getInt("noOfBooksBought"));
					return aobj;
				}

				return null;
			}

		});
	}
	class AdminMapper implements RowMapper<Admin> //For Read the Admin data 
	{
				public Admin mapRow(ResultSet rs, int arg1) throws SQLException
				{
					Admin aobj = new Admin();
					aobj.setAdminId(rs.getInt("adminId"));
					aobj.setAdminName(rs.getString("adminName"));
					aobj.setPassword(rs.getString("password"));
					return aobj;
				}
	}
}
