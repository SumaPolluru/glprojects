package com.app.MagicOfBooksUsingSpringMVC.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import com.app.MagicOfBooksUsingSpringMVC.pojo.Book;

public class BookDAOImpl implements BookDAO 
{
	private JdbcTemplate jdbcTemplate;

	public BookDAOImpl(DataSource dataSource) 
	{
		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public void update(Book book)  //Performing both operations in single method due to using single jsp file
	{
		if (book.getBookId() > 0) 
		{
			// update
			String sql = "UPDATE book SET bookName=?,authorName=?,Description=?,Genre=?,Price=?,NoOfCopies=? "
					+ " WHERE bookId=?";
			jdbcTemplate.update(sql, book.getBookName(), book.getAuthorName(), book.getDescription(), book.getGenre(),
					book.getPrice(), book.getNoOfCopies(), book.getBookId());
		} 
		else 
		{
			// insert
			String sql = "INSERT INTO book (bookName,authorName,Description,Genre,Price,NoOfCopies)"
					+ " VALUES (?, ?,?,?,?,?)";
			jdbcTemplate.update(sql, book.getBookName(), book.getAuthorName(), book.getDescription(), book.getGenre(),
					book.getPrice(), book.getNoOfCopies());
		}

	}

	@Override
	public void delete(int bookId) 
	{
		String sql = "DELETE FROM Book WHERE bookId=?";
		jdbcTemplate.update(sql, bookId);
	}

	@Override
	public List<Book> list() 
	{
		String sql = "SELECT * FROM Book order by bookId";
		List<Book> listBook = jdbcTemplate.query(sql, new RowMapper<Book>() 
		{		
			@Override
			public Book mapRow(ResultSet rs, int rowNum) throws SQLException 
			{
				Book bobj = new Book();
				bobj.setBookId(rs.getInt("bookId"));
				bobj.setBookName(rs.getString("bookName"));
				bobj.setAuthorName(rs.getString("authorName"));
				bobj.setDescription(rs.getString("description"));
				bobj.setGenre(rs.getString("genre"));
				bobj.setPrice(rs.getDouble("price"));
				bobj.setNoOfCopies(rs.getInt("noOfCopies"));
				
				return bobj;
			}

		});

		return listBook;
	}

	@Override
	public Book retrieve(int bookId)
	{
		String sql = "SELECT * FROM Book WHERE bookId=" + bookId;
		return jdbcTemplate.query(sql, new ResultSetExtractor<Book>()
		{
			@Override
			public Book extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) 
				{
					Book bobj = new Book();
					bobj.setBookId(rs.getInt("bookId"));
					bobj.setBookName(rs.getString("bookName"));
					bobj.setAuthorName(rs.getString("authorName"));
					bobj.setDescription(rs.getString("description"));
					bobj.setGenre(rs.getString("genre"));
					bobj.setPrice(rs.getDouble("price"));
					bobj.setNoOfCopies(rs.getInt("noOfCopies"));

					return bobj;
				}

				return null;
			}

		});
	}

}
