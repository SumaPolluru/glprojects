package com.app.MagicOfBooksUsingSpringMVC.controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.app.MagicOfBooksUsingSpringMVC.dao.BookDAO;

import com.app.MagicOfBooksUsingSpringMVC.pojo.Book;

@Controller
public class BookController
{
	@Autowired
	private BookDAO bookDAO;

	@RequestMapping(value = "/Book") //Request Mapping for Book operations
	public ModelAndView listContact(ModelAndView model) throws IOException 
	{
		List<Book> listBook = bookDAO.list();
		model.addObject("listBook", listBook);
		model.setViewName("retrieveBookForm");
		return model;
	}

	@RequestMapping(value = "/newBook", method = RequestMethod.GET) //New Book
	public ModelAndView newBook(ModelAndView model)
	{
		model.addObject("book", new Book());
		model.setViewName("bookForm");
		return model;	

	}
	
  /* Whenever, U submit the saveBook button then
   * It redirects to first page of book operations 
   */
	@RequestMapping(value = "/saveBook", method = RequestMethod.POST) 
	public ModelAndView saveContact(@ModelAttribute Book book)
	{
		bookDAO.update(book);
		return new ModelAndView("redirect:/Book");
	}

	@RequestMapping(value = "/deleteBook", method = RequestMethod.GET)
	public ModelAndView deleteContact(HttpServletRequest request) {
		int bookId = Integer.parseInt(request.getParameter("bookId"));
		bookDAO.delete(bookId);
		return new ModelAndView("redirect:/Book");
	}

	@RequestMapping(value = "/editBook", method = RequestMethod.GET)
	public ModelAndView editContact(HttpServletRequest request) {
		int bookId = Integer.parseInt(request.getParameter("bookId"));
		Book book = bookDAO.retrieve(bookId);
		ModelAndView model = new ModelAndView("bookForm");
		model.addObject("book", book);
		return model;
	}
}
