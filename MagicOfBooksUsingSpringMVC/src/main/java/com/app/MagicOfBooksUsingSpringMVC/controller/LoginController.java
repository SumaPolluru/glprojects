package com.app.MagicOfBooksUsingSpringMVC.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.app.MagicOfBooksUsingSpringMVC.dao.AdminDAO;
import com.app.MagicOfBooksUsingSpringMVC.pojo.Admin;

@Controller
public class LoginController {
	@Autowired
	private AdminDAO adminDAO;
	

//Request mapping for login operation
@RequestMapping(value = "/login", method = RequestMethod.GET)
public ModelAndView showLogin(HttpServletRequest request, HttpServletResponse response) {
	ModelAndView model = new ModelAndView("login");
	model.addObject("admin", new Admin());
	return model;
}

//After taking the login credentials, It will verify from the database
@RequestMapping(value = "/loginprocess", method = RequestMethod.POST)
public ModelAndView loginprocess(HttpServletRequest request, HttpServletResponse response,
		@ModelAttribute("admin") Admin admin) 
{
	ModelAndView model=null;
	Admin adobj=adminDAO.validateAdmin(admin);
	if(null !=adobj)
	{
	model=new ModelAndView("welcome");
	model.addObject("adminName", "............Welcome "+adobj.getAdminName()+"...............");
	}
	else
	{
		model=new ModelAndView("login");
		model.addObject("message","Username or Password is Invalid!!");
	
	}
	return model;	
}

}
