package com.app.MagicOfBooksUsingSpringMVC.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.app.MagicOfBooksUsingSpringMVC.dao.AdminDAO;
import com.app.MagicOfBooksUsingSpringMVC.pojo.Admin;
import javax.servlet.http.HttpServletRequest;

@Controller
public class RegisterController 
{
	@Autowired
	private AdminDAO adminDAO; //Fetching the Admin data in AdminDAO class
	
	//Request mapping for register operation
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public ModelAndView showRegister(HttpServletRequest request, HttpServletRequest response) 
	{
		
		ModelAndView model = new ModelAndView("register");
		model.addObject("admin", new Admin());
		return model;
	}

	//After taking the register credentials, It will stores into the database
	@RequestMapping(value = "/registerprocess", method = RequestMethod.POST)
	public ModelAndView addAdmin(HttpServletRequest request, HttpServletRequest response,
			@ModelAttribute("admin") Admin admin)
	{
		adminDAO.register(admin);
		return new ModelAndView("login", "adminName", "...........Welcome "+admin.getAdminName()+"...............");
	}

}
