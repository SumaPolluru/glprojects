package com.app.MagicOfBooksUsingSpringMVC.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.app.MagicOfBooksUsingSpringMVC.dao.AdminDAO;
import com.app.MagicOfBooksUsingSpringMVC.pojo.Admin;
import com.app.MagicOfBooksUsingSpringMVC.pojo.User;

@Controller
public class MagicOfBooksController  //AdminController Class
{
	@Autowired
	private AdminDAO adminDAO;
	@RequestMapping("/")
	public String getPage() 
	{
		return "home";// It will search for home.jsp under WEb-inf/jsp folder
	}

	@RequestMapping(value = "/User")
	public ModelAndView listContact(ModelAndView model) throws IOException
	{
		List<User> listUser = adminDAO.list();
		model.addObject("listUser", listUser);
		model.setViewName("retrieveAdminForm");
		return model;
	}

	@RequestMapping(value = "/newUser", method = RequestMethod.GET)
	public ModelAndView newAdmin(ModelAndView model) 
	{
		model.addObject("user", new User());
		model.setViewName("adminForm");
		return model;

	}

	@RequestMapping(value = "/saveUser", method = RequestMethod.POST)
	public ModelAndView saveUser(@ModelAttribute User user) 
	{
		adminDAO.update(user);
		return new ModelAndView("redirect:/User");
	}

	@RequestMapping(value = "/deleteUser", method = RequestMethod.GET)
	public ModelAndView deleteUser(HttpServletRequest request) 
	{
		int userId = Integer.parseInt(request.getParameter("userId"));
		adminDAO.delete(userId);
		return new ModelAndView("redirect:/User");
	}

	@RequestMapping(value = "/editUser", method = RequestMethod.GET)
	public ModelAndView editUser(HttpServletRequest request) 
	{
		int userId = Integer.parseInt(request.getParameter("userId"));
		User user = adminDAO.retrieve(userId);
		ModelAndView model = new ModelAndView("adminForm");
		model.addObject("user", user);
		return model;
	}
}
