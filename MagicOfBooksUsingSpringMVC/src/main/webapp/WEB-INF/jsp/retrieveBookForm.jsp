<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Home</title>
</head>
<style>
body {
	font-family: Calibri;
	background-color: #c7ddcc;
}

h3 {
	font-size: 25px;
}

td {
	text-align: center;
}

tr:nth-child(even) {
	background-color: #f2f2f2;
}

tr:nth-child(odd) {
	background-color: yellow;
}

th {
	background-color: yellow
}

table, th {
	color: black;
	size: 5;
}
</style>
<body>
	<div align="center">
		<h1>DISPLAYING BOOK DETAILS</h1>
		<h3>
			<a href="newBook">Add New Book Details</a>
		</h3>
		<table border="1">
			<th><font color=Black size=5>No</font></th>
			<th><font color=Black size=5>BookName</font></th>
			<th><font color=Black size=5>AuthorName</font></th>
			<th><font color=Black size=5>Description</font></th>
			<th><font color=Black size=5>Genre</font></th>
			<th><font color=Black size=5>Price</font></th>
			<th><font color=Black size=5>NoOfCopies</font></th>
			<th><font color=Black size=5>Update</font></th>
			<th><font color=Black size=5>Delete</font></th>

			<c:forEach var="book" items="${listBook}" varStatus="status">
				<tr>
					<td><font color=Brown size=4>${status.index + 1}</font></td>
					<td><font color=Brown size=4>${book.bookName}</font></td>
					<td><font color=Brown size=4>${book.authorName}</font></td>
					<td><font color=Brown size=4>${book.description}</font></td>
					<td><font color=Brown size=4>${book.genre}</font></td>
					<td><font color=Brown size=4>${book.price}</font></td>
					<td><font color=Brown size=4>${book.noOfCopies}</font></td>
					<td><a href="editBook?bookId=${book.bookId}"><font size=4>Update</font></a></td>
					<td><a href="deleteBook?bookId=${book.bookId}"><font size=4>Delete</font></a></td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>
