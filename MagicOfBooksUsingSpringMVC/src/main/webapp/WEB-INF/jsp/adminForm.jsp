<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>User Details</title>
</head>
<style>
body {
	font-family: Calibri;
	background-color: #c7ddcc;
}
</style>
<body>
	<div align="center">
		<h1>NEW/EDIT USER DETAILS</h1>
		<form:form action="saveUser" method="post" modelAttribute="user">
			<table>
				<form:hidden path="userId" />
				<tr>
					<td><font color=Black size=5>UserName</font></td>
					<td><form:input path="userName" /></td>
				</tr>
				<tr>
					<td><font color=Black size=5>Email</font></td>
					<td><form:input path="email" /></td>
				</tr>
				<tr>
					<td><font color=Black size=5>NoOfBooksBought</font></td>
					<td><form:input path="noOfBooksBought" /></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input type="submit"
						value="Save"></td>
				</tr>
			</table>
		</form:form>
	</div>
</body>
</html>