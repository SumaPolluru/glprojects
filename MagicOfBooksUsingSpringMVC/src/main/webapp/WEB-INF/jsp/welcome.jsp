<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Operations</title>
</head>
<style>
body {
	font-family: Calibri;
	background-color: #c7ddcc;
}
</style>
<body>
	<center>
		<h1>WELCOME TO CRUD OPERATIONS</h1>
	</center>
	<table align="center">
		<tr>
			<td style="font-style: italic; font-size: 25px; color: Brown;">${adminName}</td>
		</tr>
	</table>
	<table align="center">
		<tr>
			<td><a href="Book"><font color=Blue size=5>Book
						Operations</font></a></td>
		</tr>
		<tr>
			<td><a href="User"><font color=Blue size=5>User
						Operations</font></a></td>
		</tr>

		<tr>
			<td><a href="logout"><font color=Blue size=5>Logout</font></a></td>
		</tr>
	</table>

</body>
</html>