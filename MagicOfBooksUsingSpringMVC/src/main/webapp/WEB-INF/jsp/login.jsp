<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Login</title>
</head>
<style>
body {
	font-family: Calibri;
	background-color: #c7ddcc;
}
</style>
<body>
	<div align="center">
		<center><h1>WELCOME TO LOGIN PAGE</h1></center>
		<form:form action="loginprocess" method="post" modelAttribute="admin">
			<table>
				<form:hidden path="adminId" />
				<tr>
					<td><font color=Black size=5>AdminName</font> </td>
					<td><form:input path="adminName" /></td>
				</tr>
				<tr>
					<td><font color=black size=5>Password</font></td>
					<td><form:input path="password" type="password" /></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input type="submit"
						value="Login"></td>
				</tr>
			</table>
		</form:form>
	</div>
	<table align="center">
		<tr>
			<td style="font-style:italic; font-size:20px;color:Red;">${message}</td>
	</table>
</body>
</html>