<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Book Details</title>
</head>
<style>
body {
	font-family: Calibri;
	background-color: #c7ddcc;
}
</style>
<body>
	<div align="center">
		<h1>NEW/EDIT BOOK DETAILS</h1>
		<form:form action="saveBook" method="post" modelAttribute="book">
			<table>
				<form:hidden path="bookId" />
				<tr>
					<td><font color=Black size=5>Book Name</font></td>
					<td><form:input path="bookName" /></td>
				</tr>
				<tr>
					<td><font color=Black size=5>Author Name</font></td>
					<td><form:input path="authorName" /></td>
				</tr>
				<tr>
					<td><font color=Black size=5>Description</font></td>
					<td><form:input path="description" /></td>
				</tr>
				<tr>
					<td><font color=Black size=5>Genre</font></td>
					<td><form:input path="genre" /></td>
				</tr>
				<tr>
					<td><font color=Black size=5>Price</font></td>
					<td><form:input path="price" /></td>
				</tr>
				<tr>
					<td><font color=Black size=5>NoOfCopies</font></td>
					<td><form:input path="noOfCopies" /></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input type="submit"
						value="Save"></td>
				</tr>
			</table>
		</form:form>
	</div>
</body>
</html>