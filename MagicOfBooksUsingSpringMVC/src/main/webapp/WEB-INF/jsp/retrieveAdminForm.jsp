<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Display</title>
</head>
<style>
body {
	font-family: Calibri;
	background-color: #c7ddcc;
}

h3 {
	font-size: 25px;
}

td {
	text-align: center;
}

tr:nth-child(even) {
	background-color: #f2f2f2;
}

tr:nth-child(odd) {
	background-color: yellow;
}

th {
	background-color: yellow
}
</style>
<body>
	<div align="center">
		<h1>DISPLAYING USER DETAILS</h1>
		<h3>
			<a href="newUser">Add New User</a>
		</h3>
		<table border="1">
			<th><font color=Black size=5>No</font></th>
			<th><font color=Black size=5>UserName</font></th>
			<th><font color=Black size=5>Email</font></th>
			<th><font color=Black size=5>NoOfBooksBought</font></th>
			<th><font color=Black size=5>Update</font></th>
			<th><font color=Black size=5>Delete</font></th>

			<c:forEach var="user" items="${listUser}" varStatus="status">
				<tr>
					<td><font color=brown size=4>${status.index + 1}</font></td>
					<td><font color=brown size=4>${user.userName}</font></td>
					<td><font color=brown size=4>${user.email}</font></td>
					<td><font color=brown size=4>${user.noOfBooksBought}</font></td>
					<td><a href="editUser?userId=${user.userId}"><font size=4>Update</font></a></td>
					<td><a href="deleteUser?userId=${user.userId}"><font
							size=4>Delete</font></a></td>

				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>
