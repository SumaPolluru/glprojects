mysql> CREATE DATABASE BOOKMANAGEMENT;
Query OK, 1 row affected (0.01 sec)

mysql>
mysql> USE BOOKMANAGEMENT;
Database changed

mysql> CREATE TABLE Admin (
    ->   adminId int(11) NOT NULL AUTO_INCREMENT,
    ->   adminName varchar(45) NOT NULL,
    ->   password varchar(45) NOT NULL,
    ->   PRIMARY KEY (adminId)
    -> ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
Query OK, 0 rows affected, 2 warnings (0.04 sec)

mysql>
mysql> desc admin;
+-----------+-------------+------+-----+---------+----------------+
| Field     | Type        | Null | Key | Default | Extra          |
+-----------+-------------+------+-----+---------+----------------+
| adminId   | int         | NO   | PRI | NULL    | auto_increment |
| adminName | varchar(45) | NO   |     | NULL    |                |
| password  | varchar(45) | NO   |     | NULL    |                |
+-----------+-------------+------+-----+---------+----------------+
3 rows in set (0.00 sec)

CREATE TABLE Book (
  bookId int(11) NOT NULL AUTO_INCREMENT,
  bookName varchar(45) NOT NULL,
  AuthorName varchar(45) NOT NULL,
  Description varchar(100) NOT NULL,
  Genre varchar(45) NOT NULL,
  price decimal(10,2) NOT NULL,
  NoOfCopies int(11) NOT NULL,
  PRIMARY KEY (bookId)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8;

mysql> desc book;
+-------------+---------------+------+-----+---------+----------------+
| Field       | Type          | Null | Key | Default | Extra          |
+-------------+---------------+------+-----+---------+----------------+
| bookId      | int           | NO   | PRI | NULL    | auto_increment |
| bookName    | varchar(45)   | NO   |     | NULL    |                |
| AuthorName  | varchar(45)   | NO   |     | NULL    |                |
| Description | varchar(100)  | NO   |     | NULL    |                |
| Genre       | varchar(45)   | NO   |     | NULL    |                |
| price       | decimal(10,2) | NO   |     | NULL    |                |
| NoOfCopies  | int           | NO   |     | NULL    |                |
+-------------+---------------+------+-----+---------+----------------+
7 rows in set (0.00 sec)


CREATE TABLE User (
  userId int(11) NOT NULL AUTO_INCREMENT,
  userName varchar(45) NOT NULL,
  Email varchar(45) NOT NULL,
  NoOfBooksBought int(11) NOT NULL,
  PRIMARY KEY (userId)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8;



insert into  admin values(1,"Admin","AdminSuma");

mysql> select * from admin;
+---------+-----------+-----------+
| adminId | adminName | password  |
+---------+-----------+-----------+
|       1 | Admin     | AdminSuma |
+---------+-----------+-----------+
1 row in set (0.00 sec)



insert into book values(10,"Wings of fire","Dr. Abdul Kalam","This book explore important issues of family","Science Friction",200,10);
insert into book values(20,"The Key to Success","Jim Rohn","This book focus with a positive attitude","Historical Friction",150,8);
insert into book values(30,"You Can","George Adams","This book promote personal growth & well-being","Dystopian",120,6);
insert into book values(40,"A Million Thoughts","Om Swami","This book shows how to meditate correctly","Young Adult",250,4);
insert into book values(50,"Believe in Yourself","Joseph Murphy","This book is a explore about self-esteem","Thriller",220,8);
insert into book values(60,"The Mind and its Control","Budhananda","This book is an excellent about Yoga.","Autobiography",260,5);
insert into book values(70,"Master Your Emotions","Leonard","This book is guide to overcome negativity","Thriller",140,9);
insert into book values(80,"Think and Grow Rich","Napoleon Hill","This book promote wealthy common habits","Autobiography",210,6);
insert into book values(90,"Atomic Habits","James Clear","This book guide to breaking bad behaviors","Fairy Tale",170,8);
insert into book values(100,"One Day life will Change","Umakanthan","This book about love to win life","Children�s Book",260,9);
insert into book values(110,"Do It Today","Darius Foroux","This book explore the goals is of big help","Autobiography",300,10);
insert into book values(120,"Mindest","Carol","This book about success comes from having the right mindset","Thriller",170,8);
insert into book values(130,"The Secret","George Adams","This book promote personal growth and well-being","Motivational",360,23);
insert into book values(140,"A Girl that had to be Strong","Om Swami","This book shows how to meditate correctly","Science",150,7);
insert into book values(150,"The Practicing Mind","Joseph","This book is a self-help book about confidence","Historical",250,9);

mysql> select * from book;
+--------+------------------------------+-----------------+-------------------------------------------------------------+---------------------+--------+------------+
| bookId | bookName                     | AuthorName      | Description                                                 | Genre               | price  | NoOfCopies |
+--------+------------------------------+-----------------+-------------------------------------------------------------+---------------------+--------+------------+
|    100 | Wings of fire                | Dr. Abdul Kalam | This book explore important issues of family                | Science Friction    | 200.00 |         10 |
|    101 | The Key to Success           | Jim Rohn        | This book focus with a positive attitude                    | Historical Friction | 150.00 |          8 |
|    102 | You Can                      | George Adams    | This book promote personal growth & well-being              | Dystopian           | 120.00 |          6 |
|    103 | A Million Thoughts           | Om Swami        | This book shows how to meditate correctly                   | Young Adult         | 250.00 |          4 |
|    104 | Believe in Yourself          | Joseph Murphy   | This book is a explore about self-esteem                    | Thriller            | 220.00 |          8 |
|    105 | The Mind and its Control     | Budhananda      | This book is an excellent about Yoga.                       | Autobiography       | 260.00 |          5 |
|    106 | Master Your Emotions         | Leonard         | This book is guide to overcome negativity                   | Thriller            | 140.00 |          9 |
|    107 | Think and Grow Rich          | Napoleon Hill   | This book promote wealthy common habits                     | Autobiography       | 210.00 |          6 |
|    108 | Atomic Habits                | James Clear     | This book guide to breaking bad behaviors                   | Fairy Tale          | 170.00 |          8 |
|    109 | One Day life will Change     | Umakanthan      | This book about love to win life                            | Children?s Book     | 260.00 |          9 |
|    110 | Do It Today                  | Darius Foroux   | This book explore the goals is of big help                  | Autobiography       | 300.00 |         10 |
|    120 | Mindest                      | Carol           | This book about success comes from having the right mindset | Thriller            | 170.00 |          8 |
|    130 | The Secret                   | George Adams    | This book promote personal growth and well-being            | Motivational        | 360.00 |         23 |
|    140 | A Girl that had to be Strong | Om Swami        | This book shows how to meditate correctly                   | Science             | 150.00 |          7 |
|    150 | The Practicing Mind          | Joseph          | This book is a self-help book about confidence              | Historical          | 250.00 |          9 |
+--------+------------------------------+-----------------+-------------------------------------------------------------+---------------------+--------+------------+
15 rows in set (0.00 sec)

insert into user values(1001,"Suma","Suma@gmail.com",3);
insert into user values(1002,"Chintu","Chintu@gmail.com",8);
insert into user values(1003,"Riya","Riya@gmail.com",6);
insert into user values(1004,"Maya","Maya@gmail.com",4);
insert into user values(1005,"Tina","Tina@gmail.com",2);
insert into user values(1006,"Sree","Sree@gmail.com",3);
insert into user values(1007,"Riya","Riya@gmail.com",8);
insert into user values(1008,"Rithu","Rithu@gmail.com",15);

mysql> select * from user;
+--------+----------+------------------+-----------------+
| userId | userName | Email            | NoOfBooksBought |
+--------+----------+------------------+-----------------+
|   1001 | Suma     | Suma@gmail.com   |               3 |
|   1002 | Chintu   | Chintu@gmail.com |               8 |
|   1003 | Riya     | Riya@gmail.com   |               6 |
|   1004 | Maya     | Maya@gmail.com   |               4 |
|   1005 | Tina     | Tina@gmail.com   |               2 |
|   1006 | Sree     | Sree@gmail.com   |               3 |
|   1007 | Riya     | Riya@gmail.com   |               8 |
|   1008 | Rithu    | Rithu@gmail.com  |              15 |
+--------+----------+------------------+-----------------+
8 rows in set (0.00 sec)


mysql> select * from admin;
+---------+-----------+-----------+
| adminId | adminName | password  |
+---------+-----------+-----------+
|       1 | Admin     | AdminSuma |
+---------+-----------+-----------+
1 row in set (0.00 sec)