package com.app.Demo.service;

import java.util.ArrayList;  
import java.util.List;  
  
  
public class ToDoBusiness {  
  
    public ToDoService doService;  
  
    public ToDoBusiness(ToDoService doService) {  
        this.doService = doService;  
    }  
      
    public List<String> getTodosforSpring(String user) {  
          
        List<String> retrievedtodos = new ArrayList<String>();  
        List<String> todos = doService.getTodos(user);  
          
        for(String todo :todos) {  
            if(todo.contains("Spring")) {  
                retrievedtodos.add(todo);  
            }  
        }  
        return retrievedtodos;  
        }  
    
    public List<String> getTodosforEmployee(String employee) {  
        
        List<String> retrievedtodos = new ArrayList<String>();  
        List<String> todos = doService.getTodos(employee);  
          
        for(String todo :todos) {  
            if(todo.contains("John")) {  
                retrievedtodos.add(todo);  
            }  
        }  
        return retrievedtodos;  
        }  
 }
