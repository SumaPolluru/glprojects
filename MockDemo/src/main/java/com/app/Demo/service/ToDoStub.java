package com.app.Demo.service;

import java.util.Arrays;  
import java.util.List;  
  
public class ToDoStub implements ToDoService   {  
  
    public List<String> getTodos(String user) {  
      
    return Arrays.asList(" Use Core Java ", " Use Spring Core ", " Use Hibernate ", " Use Spring MVC ");  
    }

	@Override
	public List<String> getEmployees(String employee) {
		 return Arrays.asList(" John Garvier ", " Ruther James ", " Gosling John ", " Sushi varma "); 
	}  
 }
