package com.app.Demo.service;
import java.util.*;
public interface ToDoService {  
	   
    public List<String> getTodos(String user); 
    
    public List<String> getEmployees(String employee); 
 }
