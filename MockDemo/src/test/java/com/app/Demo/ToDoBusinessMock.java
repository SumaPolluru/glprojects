package com.app.Demo;

import static org.junit.Assert.assertEquals;  
import static org.mockito.Mockito.mock;  
import static org.mockito.Mockito.when;  
  
import java.util.Arrays;  
import java.util.List;  
import org.junit.Test;

import com.app.Demo.service.ToDoBusiness;
import com.app.Demo.service.ToDoService;

public class ToDoBusinessMock {  
  
    @Test  
    public void testusing_Mocks() {  
          
        ToDoService doService = mock(ToDoService.class);  
           
        List<String> combinedlist = Arrays.asList(" Use Core Java ",
        		" Use Spring Core ", " Use w3eHibernate ", " Use Spring MVC ");  
        when(doService.getTodos("dummy")).thenReturn(combinedlist);  
          
        ToDoBusiness business = new ToDoBusiness(doService);  
      
        List<String> alltd = business.getTodosforSpring("dummy");   
          
        System.out.println(alltd);  
        assertEquals(2, alltd.size());  
    }  
    
    @Test  
    public void testEmployee_Mocks() {  
          
        ToDoService doService = mock(ToDoService.class);  
           
        List<String> combinedlist = Arrays.asList("John Garvier", " Ruther James", " Gosling John", " Sushi varma");  
        when(doService.getTodos("dummy")).thenReturn(combinedlist);  
          
        ToDoBusiness business = new ToDoBusiness(doService);  
      
        List<String> alltd = business.getTodosforEmployee("dummy");   
          
        System.out.println(alltd);  
        assertEquals(2, alltd.size());  
    }  
 }
