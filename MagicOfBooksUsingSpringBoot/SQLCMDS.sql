
create database MagicOfBooksDBM;
use MagicOfBooksDBM;

Insert Admin:

insert into admin values(1,"admin","admin");

Insert Book:

insert into book values(10,"Dr. Abdul Kalam","Wings of fire","This book explore important issues of family","Science Friction",10,200);
insert into book values(20,"Jim Rohn","The Key to Success","This book focus with a positive attitude","Historical Friction",8,150);
insert into book values(30,"George Adams","You Can","This book promote personal growth & well-being","Dystopian",6,120);
insert into book values(40,"Om Swami","A Million Thoughts","This book shows how to meditate correctly","Young Adult",4,250);
insert into book values(50,"Joseph Murphy","Believe in Yourself","This book is a explore about self-esteem","Thriller",8,220);
insert into book values(60,"Budhananda","The Mind and its Control","This book is an excellent about Yoga.","Autobiography",5,260);
insert into book values(70,"Leonard","Master Your Emotions","This book is guide to overcome negativity","Thriller",9,140);
insert into book values(80,"Napoleon Hill","Think and Grow Rich","This book promote wealthy common habits","Autobiography",6,210);
insert into book values(90,"James Clear","Atomic Habits","This book guide to breaking bad behaviors","Fairy Tale",8,170);
insert into book values(100,"Umakanthan","One Day life will Change","This book about love to win life","Children’s Book",9,260);
insert into book values(110,"Darius Foroux","Do It Today","This book explore the goals is of big help","Autobiography",10,300);
insert into book values(120,"Carol","Mindest","This book about success comes from having the right mindset","Thriller",8,170);
insert into book values(130,"George Adams","The Secret","This book promote personal growth and well-being","Motivational",23,360);
insert into book values(140,"Om Swami","A Girl that had to be Strong","This book shows how to meditate correctly","Science",7,150);
insert into book values(150,"Joseph","The Practicing Mind","This book is a self-help book about confidence","Historical",9,250);
