package com.app.MagicOfBooksUsingSpringBoot;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import com.app.MagicOfBooksUsingSpringBoot.model.Admin;
import com.app.MagicOfBooksUsingSpringBoot.repositories.AdminRepository;
import com.app.MagicOfBooksUsingSpringBoot.service.AdminService;
import java.util.*;

//MockiTio Testing
@RunWith(SpringRunner.class)
@SpringBootTest
public class MagicOfBooksUsingSpringBootApplicationTests 
{

	@Autowired
	private AdminService adminService;
	
	@Mock
	private List<Admin> admin;

	@MockBean
	private AdminRepository adminRepository;
	

	//Retrieve Testing
	@Test
	public void getAdminsTest()
	{
		when(adminRepository.findAll()).thenReturn(Stream.of(new Admin("admin@gmail.com", "admin"), 
		new Admin("admin", "admin"), new Admin("adminSuma@gmail.com", "admin")).collect(Collectors.toList()));
	   assertEquals(3, adminService.getAdmins().size());
	}
	

	//Save Testing
	@Test
	public void saveAdminTest() 
	{
		Admin user = new Admin("adminSuma", "admin");
		when(adminRepository.save(user)).thenReturn(user);
		assertEquals(user, adminService.saveAdmin(user));
	}
	
	//Login Testing
	@Test
	public void getAdminLoginTest() 
	{
		Admin admin = new Admin("admin", "admin");
		when(adminRepository.findByAdminNameAndAdminPassword("admin","admin")).thenReturn(admin);
		assertEquals(admin, adminService.findByAdminNameAndAdminPassword(admin.getAdminName(),admin.getAdminPassword()));
	}

}
