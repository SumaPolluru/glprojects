<!DOCTYPE html >
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="sat, 01 Dec 2001 00:00:00 GMT">
<title>Welcome</title>
<link href="static/css/bootstrap.min.css" rel="stylesheet">
<link href="static/css/style.css" rel="stylesheet">


</head>
<style>
body {
	background-color: #E8E8E8;
	text-size: 5px;
}
</style>
<body>
	<center>
		<h3>Enter Book Title</h3>
		<hr>
		<form class="form-horizontal" method="POST" action="searchByBookTitle">
			Filter: <input type="text" name="bookName" id="bookName" size="50"
				th:value="${book.bookName}" required /> &nbsp; <input type="submit"
				value="Search" />
		</form>
	</center>

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="static/js/jquery-1.11.1.min.js"></script>
	<script src="static/js/bootstrap.min.js"></script>
</body>
</html>