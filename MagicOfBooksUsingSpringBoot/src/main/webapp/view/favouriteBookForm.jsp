<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<title>Web Application Using Spring Boot</title>
</head>
<style>

body {
	background-color: #E8E8E8;
	text-size: 6px;
}
</style>

<body>
<center>
	<h1>Add To Favorite Book</h1>
	<form:form method="POST" action="save-favouriteBook" modelAttribute="bobj">
		<table>
			<tr>
				UserId: <form:select path="user">

					<form:options items="${bobj.userIdlist}" />

				</form:select>
			</tr>
			
			<tr>
				BookId: <form:select path="book">

					<form:options items="${bobj.bookIdlist}" />

				</form:select>
			</tr>
			<td colspan="2"><input type="submit" value="Favourite Book" /></td>
			</tr>
		</table>
	</form:form>
	</center>
</body>

</html>