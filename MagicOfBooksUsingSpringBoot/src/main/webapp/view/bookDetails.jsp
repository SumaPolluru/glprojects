<!DOCTYPE html >
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="sat, 01 Dec 2001 00:00:00 GMT">
<title>Welcome</title>
<link href="static/css/bootstrap.min.css" rel="stylesheet">
<link href="static/css/style.css" rel="stylesheet">

</head>
<style>
th {
	background-color: black;
	color:white;
	text-align: center;
	text-size:5px;
}
</style>
<body>
	<div role="navigation">
		<div class="navbar navbar-inverse">
			<a href="/userHome" class="navbar-brand"><font color=white
				size=4px>User</font></a>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="/bookDetails"><font color=white size=4px>Show
								Books</font></a></li>
					<li><a href="/bookId"><font color=white size=4px>
								By Book Id</font></a></li>
					<li><a href="/bookAuthor"><font color=white size=4px>Search
								By Author Name</font></a></li>
					<li><a href="/bookTitle"><font color=white size=4px>
								By Book Title</font></a></li>
					<li><a href="/bookPublication"><font color=white size=4px>
								By Publication</font></a></li>
					<li><a href="/searchByBookPrice"><font color=white
							size=4px> By Price</font></a></li>
					<li><a href="/searchByBookPriceLowToHigh"><font
							color=white size=4px>By Price(Low To High)</font></a></li>

					<li><a href="/logout"><font color=white size=4px>Logout</font></a></li>
				</ul>
			</div>
		</div>
	</div>
	<c:choose>
		<c:when test="${userobj=='HOME' }">
			<div class="container" id="homediv">
				<div class="jumbotron text-center">
					<h2>
						<font size=8px>Welcome to User Operations Page</font>
					</h2>
					<h3>These Operations are Performed By User</h3>
				</div>
			</div>
		</c:when>
		<c:when test="${bookobj=='SEARCHDATA'}">
			<div class="container text-center" id="tasksDiv">
				<h3>BOOKS</h3>
				<hr>
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>SNo.</th>
								<th>Book Id</th>
								<th>Book Title</th>
								<th>Author Name</th>
								<th>Description</th>
								<th>Publication</th>
								<th>Price</th>
								<th>Total No.Of Copies</th>
								<th>Add to Favorite</th>
								<th>Add to read Later</th>

							</tr>
						</thead>
						<tbody>
							<c:forEach var="bookdata" items="${bookdata}" varStatus="status">
								<tr>
									<td>${status.index + 1}</td>
									<td>${bookdata.bookId }</td>
									<td>${bookdata.bookName}</td>
									<td>${bookdata.authorName}</td>
									<td>${bookdata.description}</td>
									<td>${bookdata.genre}</td>
									<td>${bookdata.price}</td>
									<td>${bookdata.noOfCopies}</td>
									<td><a href="/addToFavorite">Favorite Book</a></td>
									<td><a href="/addToReadLater">Read Later</a></td>

								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</c:when>
	</c:choose>
	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="static/js/jquery-1.11.1.min.js"></script>
	<script src="static/js/bootstrap.min.js"></script>
</body>
</html>