<!DOCTYPE html >
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="sat, 01 Dec 2001 00:00:00 GMT">
<title>Welcome</title>
<link href="static/css/bootstrap.min.css" rel="stylesheet">
<link href="static/css/style.css" rel="stylesheet">
</head>
<style>
th {
	background-color: black;
	color: white;
	text-align: center;
	text-size: 5px;
}
</style>
<body>
	<c:choose>
		<c:when test="${bookobj=='BOOKDATA'}">
			<div class="container text-center" id="tasksDiv">
				<h3>Spring Security(JWT) is working!!</h3>
				<p>User Login Successfully.</p>
				<h5>For User Operations, We are not using Microservices in this
					Token Application,So that, It's not possible for Further User Operations in
					this port number</h5>
				<h3>For That, Use this Url for User Operations
					http://localhost:8090/bookDetails</h3>
				<h3>BOOKS</h3>
				<hr>
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>SNo.</th>
								<th>Book Id</th>
								<th>Book Title</th>
								<th>Author Name</th>
								<th>Description</th>
								<th>Publication</th>
								<th>Price</th>
								<th>Total No.Of Copies</th>

							</tr>
						</thead>
						<tbody>
							<c:forEach var="bookdata" items="${bookdata}" varStatus="status">
								<tr>
									<td>${status.index + 1}</td>
									<td>${bookdata.bookId }</td>
									<td>${bookdata.bookName}</td>
									<td>${bookdata.authorName}</td>
									<td>${bookdata.description}</td>
									<td>${bookdata.genre}</td>
									<td>${bookdata.price}</td>
									<td>${bookdata.noOfCopies}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</c:when>
	</c:choose>
	<script src="static/js/jquery-1.11.1.min.js"></script>
	<script src="static/js/bootstrap.min.js"></script>
</body>
</html>