package com.app.MagicOfBooksUsingSpringBoot.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import com.app.MagicOfBooksUsingSpringBoot.model.Book;
import com.app.MagicOfBooksUsingSpringBoot.model.UserDTO;
import com.app.MagicOfBooksUsingSpringBoot.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@CrossOrigin()
public class UserController
{

	@Autowired
	UserService userService;
	
	//Swagger
	@ApiOperation(value = "UserData List", notes = "This method will retreive list from database", nickname = "getUser")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Server error"),
	@ApiResponse(code = 404, message = "UserData not found"),
	@ApiResponse(code = 200, message = "Successful retrieval", response = Book.class, responseContainer = "List") })

	//UserData Home
	@RequestMapping("/welcome")
	public String Welcome(HttpServletRequest request)
	{
		request.setAttribute("mode", "MODE_HOME");
		return "welcomepage";
	}
	
	@RequestMapping("/userHome")
	public String Admin(HttpServletRequest request)
	{
		request.setAttribute("userobj", "HOME");
		return "bookDetails";
	}

	/*@RequestMapping("/register")   //register
	public String registration(HttpServletRequest request)
	{
		request.setAttribute("mode", "MODE_REGISTER");
		return "welcomepage";
	}

	@PostMapping("/save-user")   //Checking Register Credentials
	public String registerUser(@ModelAttribute UserData user, BindingResult bindingResult, HttpServletRequest request)
	{
	if (userService.findByUserNameAndPassword(user.getUserName(), user.getPassword()) != null) 
	{
		request.setAttribute("error", "UserName already Exist");
		request.setAttribute("mode", "MODE_REGISTER");
		return "welcomepage";
	}
	else
	{
		userService.saveMyUser(user);
		request.setAttribute("mode", "MODE_HOME");
		return "welcomepage";
	}
}

	@RequestMapping("/login")
	public String login(HttpServletRequest request) 
	{
		request.setAttribute("mode", "MODE_LOGIN");
		return "welcomepage";
	}

	@RequestMapping("/login-user")  //Checking Login Credentials
	public String loginUser(@ModelAttribute UserData user, HttpServletRequest request) 
	{
		if (userService.findByUserNameAndPassword(user.getUserName(), user.getPassword()) != null)
		{
			return "bookDetails";
		}
		else
		{
			request.setAttribute("error", "Invalid Username or Password");
			request.setAttribute("mode", "MODE_LOGIN");
			return "welcomepage";

		}
	}
	
	@GetMapping("/saveuser")  //Saving Registration Data
	public String saveUser(@RequestParam String userName, @RequestParam String firstname, @RequestParam String lastname,
			@RequestParam String mobileNo, @RequestParam String address, @RequestParam String password)
	{
		UserData userDetails = new UserData(userName, firstname, lastname, mobileNo, address, password);
		userService.saveMyUser(userDetails);
		return "UserData Saved";
	}*/

	//Logout
	@RequestMapping("/logout")
	public String logout(HttpServletRequest request) 
	{
		HttpSession httpSession = request.getSession();
		httpSession.invalidate();
		return "redirect:/";
	}

	//User Register
	@RequestMapping("/UserDataRegister")
	public ModelAndView insertForm(HttpServletRequest request) 
	{
		request.setAttribute("mode", "MODE_REGISTER");
		return new ModelAndView("welcomepage","user",new UserDTO());
	}
	
	//User Login
	@RequestMapping("/login")
	public ModelAndView loginForm(HttpServletRequest request)
	{
		request.setAttribute("mode", "MODE_LOGIN");
		return new ModelAndView("welcomepage","user",new UserDTO());
	}
}
