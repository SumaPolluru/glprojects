package com.app.MagicOfBooksUsingSpringBoot.controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.app.MagicOfBooksUsingSpringBoot.model.Book;
import com.app.MagicOfBooksUsingSpringBoot.model.FavouriteBooks;
import com.app.MagicOfBooksUsingSpringBoot.model.ReadLaterBooks;
import com.app.MagicOfBooksUsingSpringBoot.repositories.BookRepository;
import com.app.MagicOfBooksUsingSpringBoot.repositories.UserRepository;
import com.app.MagicOfBooksUsingSpringBoot.service.BookService;
import com.app.MagicOfBooksUsingSpringBoot.service.FavouriteBookService;
import com.app.MagicOfBooksUsingSpringBoot.service.ReadLaterService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Controller
public class BookController
{

	@Autowired
	BookService bookService;
	
	@Autowired
	FavouriteBookService favBookService;
	
	@Autowired
	ReadLaterService readLaterService;

	@Autowired
	private BookRepository bookRepository;
	
	@Autowired
	private UserRepository userRepository;

	//Swagger
	@ApiOperation(value = "Book List", notes = "This method will retreive list from database", nickname = "getBook")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Server error"),
	@ApiResponse(code = 404, message = "Book not found"),
	@ApiResponse(code = 200, message = "Successful retrieval", response = Book.class, responseContainer = "List") })

	
	@RequestMapping("/addBook") //Insert Book
	public String addMenuDetails(HttpServletRequest request) 
	{
		request.setAttribute("bookobj", "ADD_BOOK");
		return "adminWelcomePage";
	}

	@PostMapping("/save-book")
	public String menuDetails(@ModelAttribute Book book, BindingResult bindingResult, HttpServletRequest request) 
	{
		bookService.saveBook(book);
		request.setAttribute("bookobj", "ADMIN_HOME");
		return "bookSaved";
	}

	@GetMapping("/saveBook")
	public String saveMenuDate(@RequestParam String bookName, @RequestParam String authorName,
			@RequestParam String description, @RequestParam String genre, @RequestParam double price,
			@RequestParam int noOfBooks) 
	{
		Book book = new Book(bookName, authorName, description, genre, price, noOfBooks);
		bookService.saveBook(book);
		return "bookSaved";
	}

	@GetMapping("/show-books")  //Retrieve Books
	public String showBookData(HttpServletRequest request)
	{
		request.setAttribute("books", bookService.showBooks());
		request.setAttribute("bookobj", "BOOKS");
		return "adminWelcomePage";
	}

	@RequestMapping("/delete-book") //Delete Books
	public String deleteBook(@RequestParam int bookId, HttpServletRequest request)
	{
		bookService.deleteBookById(bookId);
		request.setAttribute("books", bookService.showBooks());
		request.setAttribute("bookobj", "BOOKS");
		return "adminWelcomePage";
	}

	@RequestMapping("/edit-book") //Update Books
	public String editBook(@RequestParam int bookId, HttpServletRequest request) 
	{
		request.setAttribute("book", bookService.editBookDetails(bookId));
		request.setAttribute("bookobj", "BOOK-UPDATE");
		return "adminWelcomePage";
	}

	@RequestMapping("updateBook")
	public ModelAndView saveUpdate(@ModelAttribute("bookobj") Book book) 
	{
		bookRepository.save(book);
		return new ModelAndView("adminWelcomePage");
	}

	@RequestMapping({ "/bookDetails" })  //Book Details for UserData without CRUD Operations
	public ModelAndView getDetails(HttpServletRequest request)
	{
		List<Book> booklist = bookService.showBooks();
		request.setAttribute("bookobj", "SEARCHDATA");
		return new ModelAndView("bookDetails", "bookdata", booklist);
	}
	
	@RequestMapping({ "/userData" }) 
	public ModelAndView getBook(HttpServletRequest request)
	{
		List<Book> booklist = bookService.showBooks();
		request.setAttribute("bookobj", "BOOKDATA");
		return new ModelAndView("tokenInfo", "bookdata", booklist);
	}


	@RequestMapping("/bookId")
	public ModelAndView getBookForm() 
	{
		return new ModelAndView("bookIdForm", "bookdata", new Book());
	}

	@RequestMapping("/searchByBookId") //Search By Id
	public ModelAndView SearchById(@RequestParam int bookId, HttpServletRequest request) throws IOException
	{
		List<Book> blist = bookRepository.search(bookId);
		request.setAttribute("bookobj", "SEARCHDATA");
		return new ModelAndView("bookDetails", "bookdata", blist);
	}

	@RequestMapping("/bookAuthor")
	public ModelAndView getBookAuthorForm()
	{
		return new ModelAndView("bookAuthorForm", "bookdata", new Book());
	}

	@RequestMapping("/searchByAuthorName") //Search By Author
	public ModelAndView SearchByName(@RequestParam String authorName, HttpServletRequest request) throws IOException
	{
		List<Book> blist = bookRepository.searchByAuthor(authorName);
		request.setAttribute("bookobj", "SEARCHDATA");
		return new ModelAndView("bookDetails", "bookdata", blist);
	}
	

	@RequestMapping("/bookTitle")
	public ModelAndView getBookTitleForm()
	{
		return new ModelAndView("bookTitleForm", "bookdata", new Book());
	}

	@RequestMapping("/searchByBookTitle") //Search By Title
	public ModelAndView SearchTitle(@RequestParam String bookName, HttpServletRequest request) throws IOException
	{
		List<Book> blist = bookRepository.searchByTitle(bookName);
		request.setAttribute("bookobj", "SEARCHDATA");
		return new ModelAndView("bookDetails", "bookdata", blist);
	}

	@RequestMapping("/bookPublication")
	public ModelAndView getBookPublication() 
	{
		return new ModelAndView("bookPublicationForm", "bookdata", new Book());
	}

	@RequestMapping("/searchByBookPublication") //Search By Publication
	public ModelAndView SearchGenre(@RequestParam String genre, HttpServletRequest request) throws IOException 
	{
		List<Book> blist = bookRepository.searchByPublication(genre);
		request.setAttribute("bookobj", "SEARCHDATA");
		return new ModelAndView("bookDetails", "bookdata", blist);
	}


	@RequestMapping("/searchByBookPrice")   //Search By Price Range
	public ModelAndView SearchPriceRange(HttpServletRequest request) throws IOException {
		List<Book> blist = bookRepository.findByPriceBetween(10, 80000);
		request.setAttribute("bookobj", "SEARCHDATA");
		return new ModelAndView("bookDetails", "bookdata", blist);
	}

	@RequestMapping("/searchByBookPriceLowToHigh") //sort
	public ModelAndView SearchPrice(HttpServletRequest request) throws IOException
	{
		List<Book> blist = bookRepository.findByOrderByPriceAsc();
		request.setAttribute("bookobj", "SEARCHDATA");
		return new ModelAndView("bookDetails", "bookdata", blist);
	}
	
	@RequestMapping("/addToFavorite") //Favorite
	public ModelAndView addToFavorite(HttpServletRequest request) 
	{
		List<Integer> userIdlist = userRepository.getUser();
		List<Integer> bookIdlist = bookRepository.getBook();
		FavouriteBooks orobj = new FavouriteBooks();
		orobj.setUserIdlist(userIdlist);
		orobj.setBookIdlist(bookIdlist);
		return new ModelAndView("favouriteBookForm", "bobj", orobj);
	}
	
	@PostMapping("/save-favouriteBook")
	public String favBookDetails(@ModelAttribute FavouriteBooks favBook, BindingResult bindingResult)
	{
		favBookService.saveFavBook(favBook);
		return "bookSaved";
	}

	@GetMapping("/saveFavBook")
	public String saveFavBook()
	{
		FavouriteBooks book = new FavouriteBooks();
		favBookService.saveFavBook(book);
		return "bookSaved";
	}

	@RequestMapping("/addToReadLater")  //ReadLater
	public ModelAndView addToReadLater(HttpServletRequest request) 
	{
		List<Integer> userIdlist = userRepository.getUser();
		List<Integer> bookIdlist = bookRepository.getBook();
		ReadLaterBooks orobj = new ReadLaterBooks();
		orobj.setUserIdlist(userIdlist);
		orobj.setBookIdlist(bookIdlist);
		return new ModelAndView("readLaterForm", "bobj", orobj);
	}
	
	@PostMapping("/save-ReadLaterBook")
	public String readBookDetails(@ModelAttribute ReadLaterBooks readLaterBook, BindingResult bindingResult) 
	{
		readLaterService.saveReadLaterBook(readLaterBook);
		return "bookSaved";
	}

	@GetMapping("/saveReadLater")
	public String saveReadLaterBook()
	{
		ReadLaterBooks book = new ReadLaterBooks();
		readLaterService.saveReadLaterBook(book);
		return "bookSaved";
	}
	
	@RequestMapping("/bookIdByAdmin")  //Search By BookId For Book With CRUD Operations
	public ModelAndView getBookIdForm() 
	{
		return new ModelAndView("bookIdFormByAdmin", "bookdata", new Book());
	}

	@RequestMapping("/searchByBId")
	public ModelAndView ById(@RequestParam int bookId, HttpServletRequest request) throws IOException 
	{
		List<Book> blist = bookRepository.search(bookId);
		request.setAttribute("bookobj", "SEARCHDATA");
		return new ModelAndView("adminWelcomePage", "bookdata", blist);
	}

	@RequestMapping("/byAuthorAdmin")
	public ModelAndView getAuthorForm() 
	{
		return new ModelAndView("authorFormByAdmin", "book", new Book());
	}

	@RequestMapping("/searchByAuthor")
	public ModelAndView ByName(@RequestParam String authorName, HttpServletRequest request) throws IOException
	{
		List<Book> blist = bookRepository.searchByAuthor(authorName);
		request.setAttribute("bookobj", "SEARCHDATA");
		return new ModelAndView("adminWelcomePage", "bookdata", blist);
	}

}
