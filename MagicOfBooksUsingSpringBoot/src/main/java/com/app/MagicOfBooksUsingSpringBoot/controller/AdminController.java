package com.app.MagicOfBooksUsingSpringBoot.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.app.MagicOfBooksUsingSpringBoot.model.Admin;
import com.app.MagicOfBooksUsingSpringBoot.repositories.AdminRepository;
import com.app.MagicOfBooksUsingSpringBoot.service.AdminService;
import com.app.MagicOfBooksUsingSpringBoot.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Controller
public class AdminController
{

	@Autowired
	AdminService adminService;

	@Autowired
	AdminRepository adminRepository;

	@Autowired
	UserService userService;

	// Swagger
	@ApiOperation(value = "Admin List", notes = "This method will retreive list from database", nickname = "getAdmin")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Server error"),
	@ApiResponse(code = 404, message = "Admin not found"),
	@ApiResponse(code = 200, message = "Successful retrieval", response = Admin.class, responseContainer = "List") })

	@RequestMapping("/") // Here, Web page starts
	public String Welcome() 
	{
		return "magicOfBooks";
	}

	@RequestMapping("/home") // Home
	public String Welcome(HttpServletRequest request) 
	{
		request.setAttribute("adminobj", "ADMIN_HOME");
		return "adminpage";
	}

	@RequestMapping("/adminHome")
	public String Admin(HttpServletRequest request) 
	{
		request.setAttribute("adminobj", "HOME");
		return "adminWelcomePage";
	}

	@RequestMapping("/registeration")
	public String registration(HttpServletRequest request) 
	{
		request.setAttribute("adminobj", "ADMIN_REGISTER");
		return "adminpage";
	}

	@RequestMapping("/save-admin") // Registration,checking that admin is present or not
	public String registerUser(@ModelAttribute Admin admin, BindingResult bindingResult, HttpServletRequest request) 
	{
		if (adminService.findByAdminName(admin.getAdminName()) != null) 
		{
			request.setAttribute("error", "AdminName already Exist");
			request.setAttribute("adminobj", "ADMIN_REGISTER");
			return "adminpage";
		} 
		else 
		{
			adminService.saveAdmin(admin);
			request.setAttribute("adminobj", "ADMIN_HOME");
			return "adminpage";
		}
	}

	@RequestMapping("/adminLogin")
	public String login(HttpServletRequest request)
	{
		request.setAttribute("adminobj", "ADMIN_LOGIN");
		return "adminpage";
	}

	@RequestMapping("/login-admin") // Checking Login Credentials
	public String loginUser(@ModelAttribute Admin admin, HttpServletRequest request) 
	{
		if (adminService.findByAdminNameAndAdminPassword(admin.getAdminName(), admin.getAdminPassword()) != null)
		{
			return "adminWelcomePage";
		} 
		else 
		{
			request.setAttribute("error", "Invalid Username or Password");
			request.setAttribute("adminobj", "ADMIN_LOGIN");
			return "adminpage";

		}
	}

	@RequestMapping("/saveadmin") // Here, Registration data should store
	public String saveUser(@RequestParam String adminName, @RequestParam String adminPassword) 
	{
		Admin admin = new Admin(adminName, adminPassword);
		adminService.saveAdmin(admin);
		return "Admin Saved";
	}
	
	@GetMapping("/getAdmins")
	public List<Admin> findAllAdmins() {
		return adminService.getAdmins();
	}
}
