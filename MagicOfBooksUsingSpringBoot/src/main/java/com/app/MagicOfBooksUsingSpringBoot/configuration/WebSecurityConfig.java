package com.app.MagicOfBooksUsingSpringBoot.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter
{

	@Autowired
	private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

	@Autowired
	private UserDetailsService jwtUserDetailsService;

	@Autowired
	private JwtRequestFilter jwtRequestFilter;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception
	{
	/* configure AuthenticationManager so that it knows from where to load user 
	* for matching credentialsUse BCryptPasswordEncoder*/
		auth.userDetailsService(jwtUserDetailsService).passwordEncoder(passwordEncoder());
	}

	@Bean
	public PasswordEncoder passwordEncoder()
	{
		return new BCryptPasswordEncoder();
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception
	{
		return super.authenticationManagerBean();
	}

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception 
	{
		httpSecurity.csrf().disable().authorizeRequests()
				.antMatchers("/", "/welcome", "/authenticate", "/UserDataRegister", "/register", "/login", "/home",
						"/adminHome", "/registeration", "/save-admin", "/adminLogin", "/login-admin", "/saveadmin",
						"/userHome", "/addBook", "/save-book", "/saveBook", "/show-books", "/delete-book", "/edit-book",
						"/updateBook", "/bookDetails", "/bookId", "/searchByBookId", "/bookAuthor",
						"/searchByAuthorName", "/bookTitle", "/searchByBookTitle", "/bookPublication",
						"/searchByBookPublication", "/searchByBookPrice", "/searchByBookPriceLowToHigh",
						"/addToFavorite", "/save-favouriteBook", "/saveFavBook", "/addToReadLater",
						"/save-ReadLaterBook", "/saveReadLater", "/bookIdByAdmin", "/searchByBId", "/byAuthorAdmin",
						"/searchByAuthor", "/userData","/assets/**","/css/**", "/js/**","/img/**","/static/**")
				.permitAll().anyRequest().authenticated().and().logout().invalidateHttpSession(true)
				.clearAuthentication(true).logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
				.logoutSuccessUrl("/").permitAll().and().exceptionHandling()
				.authenticationEntryPoint(jwtAuthenticationEntryPoint).and().sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS);

		// Add a filter to validate the tokens with every request
		httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
	}
}
