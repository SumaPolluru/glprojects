package com.app.MagicOfBooksUsingSpringBoot.repositories;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.app.MagicOfBooksUsingSpringBoot.model.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Integer> 
{

	@Query("Select book from Book book where book.bookId=?1")
	public List<Book> search(int bookId);

	@Query("Select book from Book book where book.authorName=?1")
	public List<Book> searchByAuthor(String authorName);

	@Query("Select book from Book book where book.bookName=?1")
	public List<Book> searchByTitle(String bookName);

	@Query("Select book from Book book where book.genre=?1")
	public List<Book> searchByPublication(String genre);

	public List<Book> findByPriceBetween(double start, double end);

	public List<Book> findByOrderByPriceAsc();

	@Query("Select book.bookId from Book book")
	public List<Integer> getBook();

}
