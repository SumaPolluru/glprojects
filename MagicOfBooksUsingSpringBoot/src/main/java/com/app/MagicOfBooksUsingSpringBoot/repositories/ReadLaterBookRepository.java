package com.app.MagicOfBooksUsingSpringBoot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.app.MagicOfBooksUsingSpringBoot.model.ReadLaterBooks;

@Repository
public interface ReadLaterBookRepository extends JpaRepository<ReadLaterBooks, Integer> 
{

}
