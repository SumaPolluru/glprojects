package com.app.MagicOfBooksUsingSpringBoot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.MagicOfBooksUsingSpringBoot.model.FavouriteBooks;

@Repository
public interface FavouriteBookRepository extends JpaRepository<FavouriteBooks, Integer> 
{

}
