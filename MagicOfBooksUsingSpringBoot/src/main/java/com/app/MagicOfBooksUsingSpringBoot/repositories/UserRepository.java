package com.app.MagicOfBooksUsingSpringBoot.repositories;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.app.MagicOfBooksUsingSpringBoot.model.UserData;

@Repository
public interface UserRepository extends JpaRepository<UserData, Integer> 
{
	public UserData findByUsername(String username);
	
	@Query("Select user.userId from UserData user")
	public List<Integer> getUser();

}
