package com.app.MagicOfBooksUsingSpringBoot.repositories;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.app.MagicOfBooksUsingSpringBoot.model.Admin;

@Repository
public interface AdminRepository extends JpaRepository<Admin, Integer> 
{
	public Admin findByAdminNameAndAdminPassword(String adminName, String adminPassword);
	
	public Admin findByAdminName(String adminName);

	@Query("Select adobj from Admin adobj where adobj.adminName=?1 and adobj.adminPassword=?2")
	public Admin getAdmin(String adminName,String adminPassword);
	
	List<Admin> findAll();
}

