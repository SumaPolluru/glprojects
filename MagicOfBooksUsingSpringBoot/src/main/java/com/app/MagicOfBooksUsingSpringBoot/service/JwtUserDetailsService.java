package com.app.MagicOfBooksUsingSpringBoot.service;

import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.app.MagicOfBooksUsingSpringBoot.model.UserDTO;
import com.app.MagicOfBooksUsingSpringBoot.model.UserData;
import com.app.MagicOfBooksUsingSpringBoot.repositories.UserRepository;

@Service
public class JwtUserDetailsService implements UserDetailsService
{
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException 
	{
		UserData user = userRepository.findByUsername(username);
		if (user == null) 
		{
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
				new ArrayList<>());
	}

	public UserData save(UserDTO user) 
	{
		System.out.println("Coming here");
		UserData newUser = new UserData();
		newUser.setUsername(user.getUsername());
		newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
		return userRepository.save(newUser);
	}
}