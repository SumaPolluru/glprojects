package com.app.MagicOfBooksUsingSpringBoot.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import com.app.MagicOfBooksUsingSpringBoot.model.Book;
import com.app.MagicOfBooksUsingSpringBoot.repositories.BookRepository;

@Service
@Transactional
public class BookService 
{

	private BookRepository bookRepository;

	public BookService(BookRepository bookRepository)
	{
		this.bookRepository = bookRepository;
	}

	public void saveBook(Book book)
	{
		bookRepository.save(book);
	}

	public List<Book> showBooks() 
	{
		List<Book> bobj = new ArrayList<Book>();
		for (Book book : bookRepository.findAll())
		{
			bobj.add(book);
		}
		return bobj;
	}

	public void deleteBookById(int bookId) 
	{
		bookRepository.deleteById(bookId);
	}

	public Book editBookDetails(int bookId) 
	{
		Optional<Book> bobj = bookRepository.findById(bookId);
		return bobj.isPresent() ? bobj.get() : null;
	}
}
