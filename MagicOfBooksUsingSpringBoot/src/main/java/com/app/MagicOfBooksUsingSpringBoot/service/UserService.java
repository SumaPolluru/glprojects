package com.app.MagicOfBooksUsingSpringBoot.service;

import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import com.app.MagicOfBooksUsingSpringBoot.model.UserData;
import com.app.MagicOfBooksUsingSpringBoot.repositories.UserRepository;

@Service
@Transactional
public class UserService {
	
	private UserRepository userRepository;
	
	public UserService(UserRepository userRepository)
	{
		this.userRepository=userRepository;
	}
	
	public void saveMyUser(UserData userData ) 
	{
		userRepository.save(userData);
	}
	
	/*public UserData findByUserNameAndPassword(String userName, String password)
	{
		return userRepository.findByUserNameAndPassword(userName, password);
	}*/
}
	

	