package com.app.MagicOfBooksUsingSpringBoot.service;

import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import com.app.MagicOfBooksUsingSpringBoot.model.ReadLaterBooks;
import com.app.MagicOfBooksUsingSpringBoot.repositories.ReadLaterBookRepository;

@Service
@Transactional
public class ReadLaterService
{

	private ReadLaterBookRepository readLaterBookRepository;

	public ReadLaterService(ReadLaterBookRepository readLaterBookRepository) 
	{
		this.readLaterBookRepository = readLaterBookRepository;
	}

	public void saveReadLaterBook(ReadLaterBooks readLaterBook) 
	{
		readLaterBookRepository.save(readLaterBook);
	}

}
