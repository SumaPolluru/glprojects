package com.app.MagicOfBooksUsingSpringBoot.service;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.app.MagicOfBooksUsingSpringBoot.model.Admin;
import com.app.MagicOfBooksUsingSpringBoot.repositories.AdminRepository;

@Service
@Transactional
public class AdminService 
{

	@Autowired
	private AdminRepository adminRepository;

	public AdminService(AdminRepository adminRepository)
	{
		this.adminRepository = adminRepository;
	}

	public Admin saveAdmin(Admin admin)
	{
		return adminRepository.save(admin);
	}

	public Admin findByAdminNameAndAdminPassword(String adminName, String adminPassword) 
	{
		return adminRepository.findByAdminNameAndAdminPassword(adminName, adminPassword);
	}

	// Register credentials checking
	public Admin findByAdminName(String adminName) 
	{
		return adminRepository.findByAdminName(adminName);
	}

	public List<Admin> getAdmins() 
	{
		List<Admin> admins = adminRepository.findAll();
		return admins;
	}


}
