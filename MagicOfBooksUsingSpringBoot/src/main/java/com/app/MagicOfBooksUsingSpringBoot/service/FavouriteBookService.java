package com.app.MagicOfBooksUsingSpringBoot.service;

import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import com.app.MagicOfBooksUsingSpringBoot.model.FavouriteBooks;
import com.app.MagicOfBooksUsingSpringBoot.repositories.FavouriteBookRepository;

@Service
@Transactional
public class FavouriteBookService 
{

	private FavouriteBookRepository favouriteBookRepository;

	public FavouriteBookService(FavouriteBookRepository favouriteBookRepository)
	{
		this.favouriteBookRepository = favouriteBookRepository;
	}


	public void saveFavBook(FavouriteBooks favBook) 
	{
		favouriteBookRepository.save(favBook);
	}

}
