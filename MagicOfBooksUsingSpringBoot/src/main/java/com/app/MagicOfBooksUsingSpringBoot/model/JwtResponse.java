package com.app.MagicOfBooksUsingSpringBoot.model;

import java.io.Serializable;

public class JwtResponse implements Serializable 
{
	//whenever u want to store state of any object
	private final String jwttoken;

	public JwtResponse(String jwttoken)
	{
		this.jwttoken = jwttoken;
	}

	public String getToken()
	{
		return this.jwttoken;
	}
}