package com.app.MagicOfBooksUsingSpringBoot.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "FavouriteBooks")
public class FavouriteBooks 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int FavouriteBookId;

	public int getFavouriteBookId() 
	{
		return FavouriteBookId;
	}

	public void setFavouriteBookId(int favouriteBookId)
	{
		FavouriteBookId = favouriteBookId;
	}

	public FavouriteBooks()
	{

	}

	@ManyToOne   //Establishing Many To One Relation
	private UserData userData;

	public UserData getUser() 
	{
		return userData;
	}

	public void setUser(UserData userData) 
	{
		this.userData = userData;
	}

	public java.util.List<Integer> getUserIdlist() 
	{
		return userIdlist;
	}

	public void setUserIdlist(java.util.List<Integer> userIdlist)
	{
		this.userIdlist = userIdlist;
	}

	@Transient
	private java.util.List<Integer> userIdlist;

	@ManyToOne
	private Book book;

	public UserData getBook()
	{
		return userData;
	}

	public void setBook(Book book) 
	{
		this.book = book;
	}

	public java.util.List<Integer> getBookIdlist() 
	{
		return bookIdlist;
	}

	public void setBookIdlist(java.util.List<Integer> bookIdlist) 
	{
		this.bookIdlist = bookIdlist;
	}

	@Transient
	private java.util.List<Integer> bookIdlist;

}
