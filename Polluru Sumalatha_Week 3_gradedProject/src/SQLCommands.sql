
mysql> create database BookManagementSystem;
Query OK, 1 row affected (0.01 sec)

mysql> use bookmanagementsystem;
Database changed
mysql> show tables;
Empty set (0.01 sec)

mysql> create table User(UserId varchar(20) primary key,UserName varchar(20) not null,UserPassword varchar(30) not null);
Query OK, 0 rows affected (0.09 sec)

mysql> desc User;
+--------------+-------------+------+-----+---------+-------+
| Field        | Type        | Null | Key | Default | Extra |
+--------------+-------------+------+-----+---------+-------+
| UserId       | varchar(20) | NO   | PRI | NULL    |       |
| UserName     | varchar(20) | NO   |     | NULL    |       |
| UserPassword | varchar(30) | NO   |     | NULL    |       |
+--------------+-------------+------+-----+---------+-------+
3 rows in set (0.01 sec)

mysql> create table Admin(AdminId varchar(20) primary key,AdminPassword varchar(30) not null,AdminName varchar(20) not null);
Query OK, 0 rows affected (0.07 se
2 rows in set (0.01 sec)
 desc admin;
+---------------+-------------+------+-----+---------+-------+
| Field         | Type        | Null | Key | Default | Extra |
+---------------+-------------+------+-----+---------+-------+
| AdminId       | varchar(20) | NO   | PRI | NULL    |       |
| AdminPassword | varchar(30) | NO   |     | NULL    |       |
| AdminName     | varchar(20) | NO   |     | NULL    |       |
+---------------+-------------+------+-----+---------+-------+

mysql> create table Books(BookId int primary key,BookName varchar(30) not null,BookAuthorName varchar(30) not null, BookDescription varchar(150) not null,BookGenre varchar(30),BookPrice decimal(10,2) not null,NoOfCopies int not null,NoOfCopiesSold int not null);
Query OK, 0 rows affected (0.18 sec)
6 rows in set (0.00 sec)
mysql> desc books;
+-----------------+---------------+------+-----+---------+-------+
| Field           | Type          | Null | Key | Default | Extra |
+-----------------+---------------+------+-----+---------+-------+
| BookId          | int           | NO   | PRI | NULL    |       |
| BookName        | varchar(30)   | NO   |     | NULL    |       |
| BookAuthorName  | varchar(30)   | NO   |     | NULL    |       |
| BookDescription | varchar(150)  | NO   |     | NULL    |       |
| BookGenre       | varchar(30)   | YES  |     | NULL    |       |
| BookPrice       | decimal(10,2) | NO   |     | NULL    |       |
| NoOfCopies      | int           | NO   |     | NULL    |       |
| NoOfCopiesSold  | int           | NO   |     | NULL    |       |
+-----------------+---------------+------+-----+---------+-------+
8 rows in set (0.00 sec)

mysql> create table NewBooks(UserId varchar(20),NewBookId int);
Query OK, 0 rows affected (0.09 sec)


mysql> alter table NewBooks add constraint fkUserId foreign key(UserId) references User(UserId);
Query OK, 0 rows affected (0.12 sec)
Records: 0  Duplicates: 0  Warnings: 0

2 rows in set (0.00 sec)

mysql> alter table NewBooks add constraint fkNewBookId foreign key(NewBookId) references Books(BookId);
Query OK, 0 rows affected (0.12 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> create table FavouriteBooks(UserId varchar(20),FavouriteBookId int);
Query OK, 0 rows affected (0.07 sec)

mysql> alter table FavouriteBooks add constraint fkFavouriteBId foreign key(UserId) references User(UserId);
Query OK, 0 rows affected (0.14 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> alter table FavouriteBooks add constraint fkFavouriteBookId foreign key(FavouriteBookId) references Books(BookId);
Query OK, 0 rows affected (0.18 sec)
Records: 0  Duplicates: 0  Warnings: 0


mysql> create table CompletedBooks(UserId varchar(20),CompletedBookId int);
Query OK, 0 rows affected (0.04 sec)

mysql> alter table CompletedBooks add constraint fkCUserId foreign key(UserId) references User(UserId);
Query OK, 0 rows affected (0.07 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> alter table CompletedBooks add constraint fkCompletedBookId foreign key(CompletedBookId) references Books(BookId);
Query OK, 0 rows affected (0.07 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> show tables;
+--------------------------------+
| Tables_in_bookmanagementsystem |
+--------------------------------+
| admin                          |
| books                           |
| completedbooks                 |
| favouritebooks                 |
| newbooks                       |
| user                           |
+--------------------------------+
6 rows in set (0.00 sec)

mysql> desc books;
+-----------------+---------------+------+-----+---------+-------+
| Field           | Type          | Null | Key | Default | Extra |
+-----------------+---------------+------+-----+---------+-------+
| BookId          | int           | NO   | PRI | NULL    |       |
| BookName        | varchar(30)   | NO   |     | NULL    |       |
| BookAuthorName  | varchar(30)   | NO   |     | NULL    |       |
| BookDescription | varchar(150)  | NO   |     | NULL    |       |
| BookGenre       | varchar(30)   | YES  |     | NULL    |       |
| BookPrice       | decimal(10,2) | NO   |     | NULL    |       |
| NoOfCopies      | int           | NO   |     | NULL    |       |
| NoOfCopiesSold  | int           | NO   |     | NULL    |       |
+-----------------+---------------+------+-----+---------+-------+
8 rows in set (0.00 sec)

mysql> desc completedbooks;
+-----------------+-------------+------+-----+---------+-------+
| Field           | Type        | Null | Key | Default | Extra |
+-----------------+-------------+------+-----+---------+-------+
| UserId          | varchar(20) | YES  | MUL | NULL    |       |
| CompletedBookId | int         | YES  | MUL | NULL    |       |
+-----------------+-------------+------+-----+---------+-------+
2 rows in set (0.00 sec)

mysql> desc favouritebooks;
+-----------------+-------------+------+-----+---------+-------+
| Field           | Type        | Null | Key | Default | Extra |
+-----------------+-------------+------+-----+---------+-------+
| UserId          | varchar(20) | YES  | MUL | NULL    |       |
| FavouriteBookId | int         | YES  | MUL | NULL    |       |
+-----------------+-------------+------+-----+---------+-------+
2 rows in set (0.00 sec)

mysql> desc newbooks;
+-----------+-------------+------+-----+---------+-------+
| Field     | Type        | Null | Key | Default | Extra |
+-----------+-------------+------+-----+---------+-------+
| UserId    | varchar(20) | YES  | MUL | NULL    |       |
| NewBookId | int         | YES  | MUL | NULL    |       |
+-----------+-------------+------+-----+---------+-------+
2 rows in set (0.00 sec)

mysql> desc user;
+--------------+-------------+------+-----+---------+-------+
| Field        | Type        | Null | Key | Default | Extra |
+--------------+-------------+------+-----+---------+-------+
| UserId       | varchar(20) | NO   | PRI | NULL    |       |
| UserName     | varchar(20) | NO   |     | NULL    |       |
| UserPassword | varchar(30) | NO   |     | NULL    |       |
+--------------+-------------+------+-----+---------+-------+
3 rows in set (0.00 sec)

mysql> insert into user values("9381749152","PolluruSuma","@Suma12");
Query OK, 1 row affected (0.01 sec)

mysql> insert into user values("9542044583","Chintu","@chintu12");
Query OK, 1 row affected (0.00 sec)

mysql> insert into user values("8008970246","Gayathri","@Gaye12");
Query OK, 1 row affected (0.00 sec)

insert into admin values(0987654321,"Admin","AdminSuma");

mysql> select * from admin;
+-----------+---------------+-----------+
| AdminId   | AdminPassword | AdminName |
+-----------+---------------+-----------+
| 987654321 | Admin         | AdminSuma |
+-----------+---------------+-----------+

mysql> select * from user;
+------------+-------------+--------------+
| UserId     | UserName    | UserPassword |
+------------+-------------+--------------+
| 8008970246 | Gayathri    | @Gaye12      |
| 9381749152 | PolluruSuma | @Suma12      |
| 9542044583 | Chintu      | @chintu12    |
+------------+-------------+--------------+
3 rows in set (0.00 sec)

1 row in set (0.00 sec)
insert into books values(10,"Wings of fire","Dr. Abdul Kalam","This book explore important issues of family","Science Friction",200,10,6);
insert into books values(20,"The Key to Success","Jim Rohn","This book focus with a positive attitude","Historical Friction",150,8,1);
insert into books values(30,"You Can","George Adams","This book promote personal growth & well-being","Dystopian",120,6,1);
insert into books values(40,"A Million Thoughts","Om Swami","This book shows how to meditate correctly","Young Adult",250,4,2);
insert into books values(50,"Believe in Yourself","Joseph Murphy","This book is a explore about self-esteem","Thriller",220,8,3);
insert into books values(60,"The Mind and its Control","Budhananda","This book is an excellent about Yoga.","Autobiography",260,5,1);
insert into books values(70,"Master Your Emotions","Leonard","This book is guide to overcome negativity","Thriller",140,9,2);
insert into books values(80,"Think and Grow Rich","Napoleon Hill","This book promote wealthy common habits","Autobiography",210,6,2);
insert into books values(90,"Atomic Habits","James Clear","This book guide to breaking bad behaviors","Fairy Tale",170,8,2);
insert into books values(100,"One Day life will Change","Umakanthan","This book about love to win life","Children�s Book",260,9,3);
insert into books values(110,"Do It Today","Darius Foroux","This book explore the goals is of big help","Autobiography",300,10,3);
insert into books values(120,"Mindest","Carol","This book about success comes from having the right mindset","Thriller",170,8,5);
insert into books values(130,"The Secret","George Adams","This book promote personal growth and well-being","Motivational",360,23,15);
insert into books values(140,"A Girl that had to be Strong","Om Swami","This book shows how to meditate correctly","Science",150,7,2);
insert into books values(150,"The Practicing Mind","Joseph","This book is a self-help book about confidence","Historical",250,9,6);

mysql> select * from books;
+--------+------------------------------+-----------------+-------------------------------------------------------------+---------------------+-----------+------------+----------------+
| BookId | BookName                     | BookAuthorName  | BookDescription                                             | BookGenre           | BookPrice | NoOfCopies | NoOfCopiesSold |
+--------+------------------------------+-----------------+-------------------------------------------------------------+---------------------+-----------+------------+----------------+
|     10 | Wings of fire                | Dr. Abdul Kalam | This book explore important issues of family                | Science Friction    |    200.00 |         10 |              6 |
|     20 | The Key to Success           | Jim Rohn        | This book focus with a positive attitude                    | Historical Friction |    150.00 |          8 |              1 |
|     30 | You Can                      | George Adams    | This book promote personal growth & well-being              | Dystopian           |    120.00 |          6 |              1 |
|     40 | A Million Thoughts           | Om Swami        | This book shows how to meditate correctly                   | Young Adult         |    250.00 |          4 |              2 |
|     50 | Believe in Yourself          | Joseph Murphy   | This book is a explore about self-esteem                    | Thriller            |    220.00 |          8 |              3 |
|     60 | The Mind and its Control     | Budhananda      | This book is an excellent about Yoga.                       | Autobiography       |    260.00 |          5 |              1 |
|     70 | Master Your Emotions         | Leonard         | This book is guide to overcome negativity                   | Thriller            |    140.00 |          9 |              2 |
|     80 | Think and Grow Rich          | Napoleon Hill   | This book promote wealthy common habits                     | Autobiography       |    210.00 |          6 |              2 |
|     90 | Atomic Habits                | James Clear     | This book guide to breaking bad behaviors                   | Fairy Tale          |    170.00 |          8 |              2 |
|    100 | One Day life will Change     | Umakanthan      | This book about love to win life                            | Children?s Book     |    260.00 |          9 |              3 |
|    110 | Do It Today                  | Darius Foroux   | This book explore the goals is of big help                  | Autobiography       |    300.00 |         10 |              3 |
|    120 | Mindest                      | Carol           | This book about success comes from having the right mindset | Thriller            |    170.00 |          8 |              5 |
|    130 | The Secret                   | George Adams    | This book promote personal growth and well-being            | Motivational        |    360.00 |         23 |             15 |
|    140 | A Girl that had to be Strong | Om Swami        | This book shows how to meditate correctly                   | Science             |    150.00 |          7 |              2 |
|    150 | The Practicing Mind          | Joseph          | This book is a self-help book about confidence              | Historical          |    250.00 |          9 |              6 |
+--------+------------------------------+-----------------+-------------------------------------------------------------+---------------------+-----------+------------+----------------+

 insert into newbooks values("9381749152",10);

 insert into newbooks values("9381749152",20);
Query OK, 1 row affected (0.00 sec)

mysql> insert into newbooks values("9381749152",30);
Query OK, 1 row affected (0.00 sec)

mysql> select * from user;
+------------+-------------+--------------+
| UserId     | UserName    | UserPassword |
+------------+-------------+--------------+
| 8008970246 | Gayathri    | @Gaye12      |
| 9381749152 | PolluruSuma | @Suma12      |
| 9542044583 | Chintu      | @chintu12    |
+------------+-------------+--------------+
3 rows in set (0.00 sec)

mysql> insert into newbooks values("9542044583",40);
Query OK, 1 row affected (0.02 sec)

mysql> insert into newbooks values("9542044583",50);
Query OK, 1 row affected (0.00 sec)

mysql> insert into newbooks values("9542044583",10);
Query OK, 1 row affected (0.01 sec)

mysql> insert into newbooks values("8008970246",30);
Query OK, 1 row affected (0.00 sec)

mysql> insert into newbooks values("8008970246",20);
Query OK, 1 row affected (0.00 sec)

mysql> insert into newbooks values("8008970246",40);
Query OK, 1 row affected (0.01 sec)

mysql>mysql> select * from newbooks;
+------------+-----------+
| UserId     | NewBookId |
+------------+-----------+
| 9381749152 |        10 |
| 9381749152 |        20 |
| 9381749152 |        30 |
| 9542044583 |        40 |
| 9542044583 |        50 |
| 9542044583 |        10 |
| 8008970246 |        30 |
| 8008970246 |        20 |
| 8008970246 |        40 |
+------------+-----------+
9 rows in set (0.00 sec)
mysql> select userid,count(newbookId)
    -> from newbooks
    -> group by userid;
+------------+------------------+
| userid     | count(newbookId) |
+------------+------------------+
| 8008970246 |                3 |
| 9381749152 |                3 |
| 9542044583 |                3 |
+------------+------------------+
3 rows in set (0.01 sec)
mysql> insert into completedbooks values("9381749152",60);
Query OK, 1 row affected (0.00 sec)

mysql> insert into completedbooks values("9381749152",70);
Query OK, 1 row affected (0.00 sec)

mysql> insert into completedbooks values("9381749152",30);
Query OK, 1 row affected (0.01 sec)

mysql> insert into completedbooks values("8008970246",80);
Query OK, 1 row affected (0.00 sec)

mysql> insert into completedbooks values("8008970246",70);
Query OK, 1 row affected (0.00 sec)

mysql> insert into completedbooks values("8008970246",90);
Query OK, 1 row affected (0.01 sec)
mysql> insert into completedbooks values("9542044583",100);
Query OK, 1 row affected (0.01 sec)

mysql> insert into completedbooks values("9542044583",70);
Query OK, 1 row affected (0.00 sec)

mysql> insert into completedbooks values("9542044583",20);
Query OK, 1 row affected (0.01 sec)

mysql> select * from completedbooks;
+------------+-----------------+
| UserId     | CompletedBookId |
+------------+-----------------+
| 9381749152 |              60 |
| 9381749152 |              70 |
| 9381749152 |              30 |
| 8008970246 |              80 |
| 8008970246 |              70 |
| 8008970246 |              90 |
| 9542044583 |             100 |
| 9542044583 |              70 |
| 9542044583 |              20 |
+------------+-----------------+
9 rows in set (0.00 sec)

mysql> insert into favouritebooks values("9381749152",110);
Query OK, 1 row affected (0.01 sec)

mysql> insert into favouritebooks values("9381749152",120);
Query OK, 1 row affected (0.00 sec)

mysql> insert into favouritebooks values("9381749152",130);
Query OK, 1 row affected (0.00 sec)

mysql> insert into favouritebooks values("8008970246",130);
Query OK, 1 row affected (0.00 sec)

mysql> insert into favouritebooks values("8008970246",10);
Query OK, 1 row affected (0.00 sec)

mysql> insert into favouritebooks values("8008970246",140);
Query OK, 1 row affected (0.00 sec)

mysql> insert into favouritebooks values("9542044583",140);
Query OK, 1 row affected (0.00 sec)

mysql> insert into favouritebooks values("9542044583",150);
Query OK, 1 row affected (0.02 sec)

mysql> insert into favouritebooks values("9542044583",120);
Query OK, 1 row affected (0.00 sec)

mysql> select * from favouritebooks;
+------------+-----------------+
| UserId     | FavouriteBookId |
+------------+-----------------+
| 9381749152 |             110 |
| 9381749152 |             120 |
| 9381749152 |             130 |
| 8008970246 |             130 |
| 8008970246 |              10 |
| 8008970246 |             140 |
| 9542044583 |             140 |
| 9542044583 |             150 |
| 9542044583 |             120 |
+------------+-----------------+
9 rows in set (0.00 sec)


mysql> select * from admin;
+-----------+---------------+-----------+
| AdminId   | AdminPassword | AdminName |
+-----------+---------------+-----------+
| 987654321 | Admin         | AdminSuma |
+-----------+---------------+-----------+