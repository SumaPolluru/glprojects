package com.gl.main.testing;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.gl.main.pojo.User;
import com.gl.main.service.UserServiceImpl;

public class UserCredentialsTest
{
	UserServiceImpl uobj;

	@Before
	public void setUp()
	{
		uobj = new UserServiceImpl();
	}

	@Test
	public void test() 
	{
		User u1 = new User();
		u1.setUserName("PolluruSuma");
		u1.setUserPassword("@Suma12");
		u1.setUserName("Chintu");
		u1.setUserPassword("@chintu12");
		u1.setUserName("Gayathri");
		u1.setUserPassword("@Gaye12");
		assertTrue(uobj.checkUser(u1));

	}

	@After
	public void showMessage() 
	{
		System.out.println("All test done");
	}

}
