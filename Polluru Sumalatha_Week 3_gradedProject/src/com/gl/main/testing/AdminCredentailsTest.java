package com.gl.main.testing;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.gl.main.pojo.Admin;
import com.gl.main.service.AdminServiceImpl;

public class AdminCredentailsTest
{
	AdminServiceImpl sobj;

	@Before
	public void setUp() 
	{
		sobj = new AdminServiceImpl();
	}

	@Test
	public void test() 
	{
		Admin a1 = new Admin();
		a1.setAdminName("AdminSuma");
		a1.setAdminPassword("Admin");
		assertTrue(sobj.checkAdmin(a1));

	}

	@After
	public void showMessage() 
	{
		System.out.println("All test done");
	}

}
