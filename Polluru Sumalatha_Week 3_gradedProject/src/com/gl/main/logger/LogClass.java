package com.gl.main.logger;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
//Creating log class to get the logger files into the system
public class LogClass
{
        public void logMethod() throws SecurityException, IOException
        {
        	boolean append=true;
        	FileHandler handler =new FileHandler("new.log",append);
        	Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
        	logger.addHandler(handler);
		
        	logger.warning("warning message");
		
        	logger.info("info message");
		
        	logger.config("config message");
		
        	logger.fine("fine message");
		
        	logger.finer("finer message");
		
        	logger.finest("finest message");
		
        	logger.info("data added");
		
        	logger.info("tasks added");
        }
}

