package com.gl.main.service;

import java.util.List;
import java.util.Scanner;
import com.gl.main.dao.AdminDAOImpl;
import com.gl.main.pojo.Admin;
import com.gl.main.pojo.Books;

public class AdminServiceImpl implements AdminService 
{
	private Scanner sc;
	Books bobj;
	AdminDAOImpl daoimpl;

	public AdminServiceImpl()
	{
		sc = new Scanner(System.in);
		bobj = new Books();
		daoimpl = new AdminDAOImpl();
	}
	@Override
     public void retreiveAdmin() 
	{
		List<Admin> adminlist = daoimpl.retreiveAdmin();
		for (Admin b1 : adminlist) 
		{
			System.out.println("..........................................");
			System.out.println("AdminName: " + b1.getAdminName());
			System.out.println("Admin Password: " + b1.getAdminPassword());
		}
	}
	@Override
	public boolean checkAdmin(Admin a1) //Testing
	{
		boolean result=false;
		List<Admin> adminList = daoimpl.retreiveAdmin();
		for (Admin obj : adminList) {
			if (obj.getAdminName().equals(a1.getAdminName()) && (obj.getAdminPassword().equals(a1.getAdminPassword())))
			{
			result=true;
			break;
			} 
			else 
			{
				return false;
			}
		}
		if(result) {
			return true;
		}
		else
			{
			return false;
			}
	}
	@Override
	public void insertBooks() {
		System.out.println("Enter no of Books u want to add ");
		int noofbooks = sc.nextInt();
		for (int x = 1; x <= noofbooks; x++) {
			Books bobj = new Books();
			System.out.println("Enter BookId");
			bobj.setBookId(sc.nextInt());
			System.out.println("Enter BookName ");
			bobj.setBookName(sc.next());
			System.out.println("Enter Book Author Name ");
			bobj.setBookAuthorName(sc.next());
			System.out.println("Enter Book Description");
			bobj.setBookDescription(sc.next());
			System.out.println("Enter Book Genre");
			bobj.setBookGenre(sc.next());
			System.out.println("Enter Book Price");
			bobj.setBookPrice(sc.nextDouble());
			System.out.println("Enter No.of Copies of Books are available");
			bobj.setNoOfCopies(sc.nextInt());
			System.out.println("Enter No.of Copies of Books are Sold ");
			bobj.setNoOfCopiesSold(sc.nextInt());
			daoimpl.insertBooks(bobj);
		}

	}

	@Override
	public void deleteBooks() {
		System.out.println("Enter Book Id which u want to delete");
		int bookId = sc.nextInt();
		Books deleteObj = new Books();
		deleteObj.setBookId(bookId);
		daoimpl.deleteBooks(deleteObj);

	}

	@Override
	public void updateBooks() {
		System.out.println("Enter BookId which u want to update");
		int bookId = sc.nextInt();
		System.out.println("Enter new BookName");
		String bookName=sc.next();
		System.out.println("Enter new Book Author");
		String bookAuthor=sc.next();
		System.out.println("Enter new Book Description");
		String bookDescription =sc.next();
		System.out.println("Enter new Book Genre");
		String bookGenre=sc.next();
		System.out.println("Enter new Price ");
		double newPrice = sc.nextDouble();
		System.out.println("Enter new NoOfCopies");
		int noOfCopies=sc.nextInt();
		System.out.println("Enter new NoOfCopiesSold");
		int noOfCopiesSold=sc.nextInt();
		Books updateObj = new Books();
		updateObj.setBookId(bookId);
		updateObj.setBookName(bookName);
		updateObj.setBookAuthorName(bookAuthor);
		updateObj.setBookDescription(bookDescription);
		updateObj.setBookGenre(bookGenre);
		updateObj.setBookPrice(newPrice);
		updateObj.setNoOfCopies(noOfCopies);
		updateObj.setNoOfCopiesSold(noOfCopiesSold);
		daoimpl.updateBooks(updateObj);
	}

	@Override
	public void retreiveBooks() {
		List<Books> blist = daoimpl.retreiveBooks();
		for (Books b1 : blist) {
			System.out.println("..........................................");
			System.out.println("BookId: " + b1.getBookId());
			System.out.println("Book Name: " + b1.getBookName());
			System.out.println("Book Author Name: " + b1.getBookAuthorName());
			System.out.println("Book Description: " + b1.getBookDescription());
			System.out.println("Book Genre: " + b1.getBookGenre());
			System.out.println("Book Price: " + b1.getBookPrice());
			System.out.println("No.Of Copies: " + b1.getNoOfCopies());
			System.out.println("No.Of Copies Sold: " + b1.getNoOfCopiesSold());
		}

	}

	@Override
	public void totalBooks()
	{
		System.out.println("Total book in Book Management System");
		System.out.println(".......................................");
		List<Books> blist = daoimpl.totalBooks();
		for (Books b1 : blist) {
			System.out.println("Total Books: " + b1.getBookId());
		}
	}

	@Override
	public void displayAutoBiographyBooks() 
	{
		System.out.println("Books in Autobiography Genre");
		System.out.println(".................................");
		List<Books> blist = daoimpl.displayAutoBioGraphyBooks();
		for (Books b1 : blist) {
			System.out.println(".................................");
			System.out.println("BookId: " + b1.getBookId());
			System.out.println("Book Name: " + b1.getBookName());
			System.out.println("Book genre: " + b1.getBookGenre());

		}
	}

	@Override
	public void priceLowToHighBooks() 
	{
		System.out.println("Price Low To High ");
		List<Books> blist = daoimpl.priceLowToHighBooks();
		for (Books b1 : blist) 
		{
			System.out.println("..............................");
			System.out.println("BookId: " + b1.getBookId());
			System.out.println("Book Name: " + b1.getBookName());
			System.out.println("Book Author Name: " + b1.getBookAuthorName());
			System.out.println("Book Description: " + b1.getBookDescription());
			System.out.println("Book Genre: " + b1.getBookGenre());
			System.out.println("Book Price: " + b1.getBookPrice());
			System.out.println("No.Of Copies: " + b1.getNoOfCopies());
			System.out.println("No.Of Copies Sold: " + b1.getNoOfCopiesSold());
		}

	}

	@Override
	public void priceHighToLowBooks()
	{
		System.out.println("Price High to Low");
		List<Books> blist = daoimpl.priceHighToLowBooks();
		for (Books b1 : blist) 
		{
			System.out.println("......................................");
			System.out.println("BookId: " + b1.getBookId());
			System.out.println("Book Name: " + b1.getBookName());
			System.out.println("Book Author Name: " + b1.getBookAuthorName());
			System.out.println("Book Description: " + b1.getBookDescription());
			System.out.println("Book Genre: " + b1.getBookGenre());
			System.out.println("Book Price: " + b1.getBookPrice());
			System.out.println("No.Of Copies: " + b1.getNoOfCopies());
			System.out.println("No.Of Copies Sold: " + b1.getNoOfCopiesSold());
		}

	}

	@Override
	public void bestSellingBooksWise() 
	{
		System.out.println("Best selling Books are ");
		List<Books> blist = daoimpl.bestSellingBooksWise();
		for (Books b1 : blist) 
		{
			System.out.println(".......................................");
			System.out.println("BookId: " + b1.getBookId());
			System.out.println("Book Name: " + b1.getBookName());
			System.out.println("No.Of Copies Sold: " + b1.getNoOfCopiesSold());
		}
}

}
