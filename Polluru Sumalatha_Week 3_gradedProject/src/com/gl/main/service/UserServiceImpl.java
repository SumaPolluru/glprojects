package com.gl.main.service;

import java.util.List;
import java.util.Scanner;

import com.gl.main.dao.UserDAO;
import com.gl.main.dao.UserDAOImpl;
import com.gl.main.pojo.Admin;
import com.gl.main.pojo.User;

public class UserServiceImpl implements UserService
{
	private Scanner sc;
	UserDAOImpl daoimpl;

	public UserServiceImpl()
	{
		sc = new Scanner(System.in);
		daoimpl = new UserDAOImpl();
	}

	public void registerUser()
	{
		User user = new User();
		System.out.println("Enter UserId(mobileno)");
		user.setUserId(sc.nextLong());
		System.out.println("Enter User Name");
		user.setUserName(sc.next());
		System.out.println("Enter Password ");
		user.setUserPassword(sc.next());
		daoimpl.registerUser(user);
	}

	@Override
	public void retreiveUser() 
	{
		List<User> userList = daoimpl.retreiveUser();
		for (User uobj : userList) 
		{
			System.out.println("..........................................");
			System.out.println("UserName: " + uobj.getUserName());
			System.out.println("User Password: " + uobj.getUserPassword());
		}
	}

	@Override
	public boolean checkUser(User u1) //Testing
	{
		boolean result = false;
		List<User> userList = daoimpl.retreiveUser();
		for (User uobj : userList) 
		{
			if (uobj.getUserName().equals(u1.getUserName()) && (uobj.getUserPassword().equals(u1.getUserPassword()))) {
				result = true;
				break;
			} 
			else 
			{
				result=false;
			}
		}
		if (result) 
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
}
