package com.gl.main.service;

import com.gl.main.pojo.User;

public interface UserService {
	public void registerUser();

	public void retreiveUser();

	public boolean checkUser(User u1);
}
