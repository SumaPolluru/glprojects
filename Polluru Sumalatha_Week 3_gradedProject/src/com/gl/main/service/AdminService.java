package com.gl.main.service;

import com.gl.main.pojo.Admin;

public interface AdminService 
{
	public void retreiveAdmin();
	public void insertBooks();
	public void deleteBooks();
	public void updateBooks();
	public void retreiveBooks();
	public void totalBooks();
	public void displayAutoBiographyBooks();
	public void priceLowToHighBooks();
	public void priceHighToLowBooks();
	public void bestSellingBooksWise();
	public boolean checkAdmin(Admin a1);
}
