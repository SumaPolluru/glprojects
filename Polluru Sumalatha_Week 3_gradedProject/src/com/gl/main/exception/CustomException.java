package com.gl.main.exception;

public class CustomException extends Exception {
	// Here,Custom Exception extends from the super class of Exception
	@Override
	public String getMessage() {
		return "BookId should not be negative";
	}
}
