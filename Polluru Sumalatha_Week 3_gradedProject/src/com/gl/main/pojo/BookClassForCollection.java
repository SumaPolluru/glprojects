package com.gl.main.pojo;
	//Here,The package name is com.gl.main.pojo
	public class BookClassForCollection
	{
		    public int bookId;
			public String bookName;
			public String authorName;
		    public String description;
		    
		    //Default constructor
		    BookClassForCollection()
		    {
		    	
		    }
		    
		    //Parameterized constructor to initialize the data into collection
			public BookClassForCollection(int bookId, String bookName, String authorName, String description) 
			{
				super();
				this.bookId = bookId;
				this.bookName = bookName;
				this.authorName = authorName;
				this.description = description;
			}
			
			//Using toString method to fetch the data from the collections rather then address of object
			@Override
			public String toString()
			{
				return "BookID:" + bookId +"\nBookName:" + bookName +" \nAuthorName:" + authorName + " \nDescription:"	+ description ;
			}	
	 }

