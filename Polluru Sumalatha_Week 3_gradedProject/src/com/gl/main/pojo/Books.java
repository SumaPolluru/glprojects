package com.gl.main.pojo;

public class Books 
{
	private int bookId;
	private String bookName;
	private String bookAuthorName;
	private String bookDescription;
	private String bookGenre;
	private double bookPrice;
	private int noOfCopies;
	private int noOfCopiesSold;
	
	public int getBookId() 
	{
		return bookId;
	}

	public void setBookId(int bookId) 
	{
		this.bookId = bookId;
	}

	public String getBookName()
	{
		return bookName;
	}

	public void setBookName(String bookName)
	{
		this.bookName = bookName;
	}

	public String getBookAuthorName() 
	{
		return bookAuthorName;
	}

	public void setBookAuthorName(String bookAuthorName)
	{
		this.bookAuthorName = bookAuthorName;
	}

	public String getBookDescription()
	{
		return bookDescription;
	}

	public void setBookDescription(String bookDescription)
	{
		this.bookDescription = bookDescription;
	}

	public String getBookGenre()
	{
		return bookGenre;
	}

	public void setBookGenre(String bookGenre)
	{
		this.bookGenre = bookGenre;
	}

	public double getBookPrice()
	{
		return bookPrice;
	}

	public void setBookPrice(double bookPrice)
	{
		this.bookPrice = bookPrice;
	}

	public int getNoOfCopies()
	{
		return noOfCopies;
	}

	public void setNoOfCopies(int noOfCopies) 
	{
		this.noOfCopies = noOfCopies;
	}

	public int getNoOfCopiesSold() 
	{
		return noOfCopiesSold;
	}

	public void setNoOfCopiesSold(int noOfCopiesSold)
	{
		this.noOfCopiesSold = noOfCopiesSold;
	}
}
