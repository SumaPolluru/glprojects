package com.gl.main.pojo;

import java.util.Objects;

public class Admin
{
	private long adminId;
	private String adminPassword;
	private String adminName;

	public long getAdminId() 
	{
		return adminId;
	}

	public void setAdminId(long adminId)
	{
		this.adminId = adminId;
	}

	public String getAdminPassword()
	{
		return adminPassword;
	}

	public void setAdminPassword(String adminPassword)
	{
		this.adminPassword = adminPassword;
	}

	public String getAdminName() 
	{
		return adminName;
	}

	public void setAdminName(String adminName)
	{
		this.adminName = adminName;
	}

	@Override
	public int hashCode() 
	{
		return Objects.hash(adminName, adminPassword);
	}

	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Admin other = (Admin) obj;
		return Objects.equals(adminName, other.adminName) && Objects.equals(adminPassword, other.adminPassword);
	}

}