package com.gl.main.pojo;

import java.util.Objects;

public class User 
{
	private long userId;
	private String userName;
	private String userPassword;

	public long getUserId() 
	{
		return userId;
	}

	public void setUserId(long userId)
	{
		this.userId = userId;
	}

	public String getUserName() 
	{
		return userName;
	}

	public void setUserName(String userName) 
	{
		this.userName = userName;
	}

	public String getUserPassword()
	{
		return userPassword;
	}

	public void setUserPassword(String userPassword)
	{
		this.userPassword = userPassword;
	}

	@Override
	public int hashCode() 
	{
		return Objects.hash(userName, userPassword);
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		return Objects.equals(userName, other.userName) && 
				Objects.equals(userPassword, other.userPassword);
	}
}
