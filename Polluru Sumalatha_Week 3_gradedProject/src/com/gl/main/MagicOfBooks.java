package com.gl.main;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import com.gl.main.dao.AdminDAOImpl;
import com.gl.main.dao.UserDAOImpl;
import com.gl.main.exception.CustomException;
import com.gl.main.logger.LogClass;
import com.gl.main.pojo.Admin;
import com.gl.main.pojo.User;
import com.gl.main.service.AdminServiceImpl;
import com.gl.main.service.UserServiceImpl;

public class MagicOfBooks extends Thread {
	static Scanner sc;

	public MagicOfBooks() // Constructor of class
	{
		sc = new Scanner(System.in);
	}
	public void run() // Thread
	{
		checkAdmin();
		checkUser();
	}

	public static void main(String[] args) throws SecurityException, IOException // Main Method
	{
		LogClass lobj = new LogClass();// Creating object for LogClass
		lobj.logMethod();
		MagicOfBooks mainobj = new MagicOfBooks(); // Creating object for MainClass
		Thread t = new Thread();// Creating Thread
		t.start();
		UserServiceImpl userobj = new UserServiceImpl();
		AdminServiceImpl adobj=new AdminServiceImpl();
		System.out.println("*****************************************************************");
		System.out.println("*****************************************************************");
		System.out.println("!-------------WELCOME TO BOOK MANAGEMENT SYSTEM-------------!");
		System.out.println("*****************************************************************");
		System.out.println("*****************************************************************");
		String ch = "null";
		int choice = 0;
		do {
			System.out.println("Please enter you are belongs to");
			System.out.println("1.Admin");
			System.out.println("2.User");
			System.out.println("3.New User(Register)");
			choice = sc.nextInt();
			switch (choice) {
			case 1:
					mainobj.checkAdmin();
						break;
			case 2:
					mainobj.checkUser();
						break;
			case 3:
					userobj.registerUser();
					System.out.println("Now,Login with your credentails");
					mainobj.checkUser();
					System.out.print("Then,Please see all books in Book Store");
					adobj.retreiveBooks();
					
						break;
			default:
					System.out.println("Invalid option");
			}
		} while (ch.equals("y"));
	}

	public void checkUser() //Validating User
	{ 
		UserDAOImpl daoimpl = new UserDAOImpl(); //Kindly Check the User Credentials in User Table
		System.out.println("Enter User name");
		String userName = sc.next();
		System.out.println("Enter User Password");
		String userPassword = sc.next();
		List<User> userlist = daoimpl.retreiveUser();
		boolean loginStatus = false;
		for (User obj : userlist) 
		{
			if (obj.getUserName().equals(userName) && (obj.getUserPassword().equals(userPassword)))
			{
				loginStatus = true;
				break;
			}
		}
		if (loginStatus)
		{
			System.out.println("User is valid: " + userName);
			userOperations();
		} 
		else
		{
			System.out.println("Login credentials are Invalid!,Please Try again/Register ");
		}
	}

	public void userOperations() {
		System.out.println("Please Enter password for further operations");
		/* Here,Using Password for More Secure instead of userName */
		String password = sc.next();
		switch (password) {
		case "@Suma12":
						user1Menu(); 
						   break;
		case "@Chintu": 
						user2Menu();
							break;
		case "@Gaye12":
						user3Menu();
							break;
		default:
					System.out.println("!------Your data is not present,Try again-------!");
		}
	}

	public void user1Menu() // User1 menu method
	{
		UserDAOImpl uobj = new UserDAOImpl();
		String ch = "y";
		while (ch.equals("y")) {
			System.out.println("1. See your new books ");
			System.out.println("2. See your favourite books");
			System.out.println("3. See your completed books");
			System.out.println("4. Select the book in the book list");
			System.out.println("5. Get the details of book");
			System.out.println("6. Logout");
			int option = sc.nextInt();
			switch (option) {

			case 1:
					uobj.seeNewBooksUser1(); // These, methods are calling from UserDAOImpl using object										
							break;
			case 2:
					uobj.seeFavouriteBooksUser1();
							break;
			case 3:
					uobj.seeCompletedBooksUser1();
							break;
			case 4:
					try 
					{
						uobj.selectBookUser1();
					} 
					catch (CustomException e)
					{
						System.out.println(e.getMessage());
					}
							break;
			case 5:
					uobj.getBookDetailsUser1();
							break;
			case 6:
					System.out.println("Successfully logout, Thank You!");
					System.exit(0);
							break;
			default:
					System.out.println("Invalid");
			}
			System.out.println("Do u want to continue(y/n)");
			ch = sc.next();
		}
	}

	public void user2Menu() // Here, declaring User2 Menu
	{
		UserDAOImpl uobj = new UserDAOImpl();
		String ch = "y";
		while (ch.equals("y")) {
			System.out.println("1. See your new books ");
			System.out.println("2. See your favourite books");
			System.out.println("3. See your completed books");
			System.out.println("4. Select the book in the book list");
			System.out.println("5. Get the details of book");
			System.out.println("6. Logout");
			int option = sc.nextInt();
			switch (option) {

			case 1:
					uobj.seeNewBooksUser2();
							break;
			case 2:
					uobj.seeFavouriteBooksUser2();
							break;
			case 3:
					uobj.seeCompletedBooksUser2();
							break;
			case 4:
					try 
					{
						uobj.selectBookUser2();
					} 
					catch (CustomException e)
					{
						System.out.println(e.getMessage());
					}
							 break;
			case 5:
					uobj.getBookDetailsUser2();
							 break;
			case 6:
					System.out.println("Successfully logout, Thank You!");
					System.exit(0);
							 break;
			default:
					System.out.println("Invalid");
			}
			System.out.println("Do u want to continue(y/n)");
			ch = sc.next();
		}
	}

	public void user3Menu() {
		UserDAOImpl uobj = new UserDAOImpl();
		String ch = "y";
		while (ch.equals("y")) {
			System.out.println("1. See your new books ");
			System.out.println("2. See your favourite books");
			System.out.println("3. See your completed books");
			System.out.println("4. Select the book in the book list");
			System.out.println("5. Get the details of book");
			System.out.println("6. Logout");
			int option = sc.nextInt();
			switch (option) {

			case 1:
					uobj.seeNewBooksUser3();
							break;
			case 2:
					uobj.seeFavouriteBooksUser3();
							break;
			case 3:
					uobj.seeCompletedBooksUser3();
							break;
			case 4:
					try
					{
						uobj.selectBookUser3();
					} 
					catch (CustomException e)
					{
						System.out.println(e.getMessage());
					}
							break;
			case 5:
					uobj.getBookDetailsUser3();
							break;
			case 6:
					System.out.println("Successfully logout, Thank You!");
					System.exit(0);
							break;
			default:
					System.out.println("Invalid");
			}
			System.out.println("Do u want to continue(y/n)");
			ch = sc.next();
		}
	}

	public void checkAdmin() //Kindly Check the Admin Credentials in Admin Table
	{
		AdminDAOImpl daoimpl = new AdminDAOImpl();
		System.out.println("Enter Admin Name");
		String adminName = sc.next();
		System.out.println("Enter Admin Password");
		String adminPassword = sc.next();
		List<Admin> adminlist = daoimpl.retreiveAdmin();
		for (Admin obj : adminlist) 
		{
			if (obj.getAdminName().equals(adminName) && (obj.getAdminPassword().equals(adminPassword)))
			{
				System.out.println("Admin is valid");
				adminMenu();
				break;
			} 
			else 
			{
				System.out.println("Login credentials are Invalid");
			}
		}
	}

	public void adminMenu() {
		AdminServiceImpl uobj = new AdminServiceImpl();
		String ch = "y";
		while (ch.equals("y")) {
			System.out.println("Admin Operations");
			System.out.println("..............................");
			System.out.println("1. Insert books ");
			System.out.println("2. Update books");
			System.out.println("3. Delete books");
			System.out.println("4. Display books");
			System.out.println("5. Total no.of books in System");
			System.out.println("6. Autobiography books");
			System.out.println("7. Price Low to High Books");
			System.out.println("8. Price High to Low");
			System.out.println("9. Best Selling Books Order");
			System.out.println("10.Logout");
			int option = sc.nextInt();
			switch (option) {
			case 1:
					uobj.insertBooks();
							break;
			case 2:
					uobj.updateBooks();
							break;
			case 3:
					uobj.deleteBooks();
					       break;
			case 4:
					uobj.retreiveBooks();
							break;
			case 5:
					uobj.totalBooks();
							break;
			case 6:
					uobj.displayAutoBiographyBooks();
							break;
			case 7:
					uobj.priceLowToHighBooks();
							break;
			case 8:
					uobj.priceHighToLowBooks();
							break;
			case 9:
					uobj.bestSellingBooksWise();
				            break;
			case 10:
				     System.out.println("Sucessfully Logout, Thank You!");
				     System.exit(0);
			default:
				System.out.println("Invalid option");
			}
			System.out.println("Do u want to continue(y/n)");
			ch = sc.next();
		}
	}
}

/*
 * As per my knowledge, I done this assessment Thank You!
 */
