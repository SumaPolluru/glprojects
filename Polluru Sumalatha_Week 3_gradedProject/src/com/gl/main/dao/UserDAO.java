package com.gl.main.dao;

import java.util.List;

import com.gl.main.exception.CustomException;
import com.gl.main.pojo.User;

public interface UserDAO 
{
	public void registerUser(User uobj);
	public List<User> retreiveUser();
	public void seeNewBooksUser1(); //These are the methods created for perform operations
	public void seeNewBooksUser2();
	public void seeNewBooksUser3();
	public void seeFavouriteBooksUser1();
	public void seeFavouriteBooksUser2();
	public void seeFavouriteBooksUser3();
	public void seeCompletedBooksUser1();
	public void seeCompletedBooksUser2();
	public void seeCompletedBooksUser3();
	public void selectBookUser1() throws CustomException;
	public void selectBookUser2() throws CustomException; 
	public void selectBookUser3() throws CustomException; 
	public void getBookDetailsUser1();
	public void getBookDetailsUser2();
	public void getBookDetailsUser3();
}
