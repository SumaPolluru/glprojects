package com.gl.main.dao;

import java.util.List;

import com.gl.main.pojo.Admin;
import com.gl.main.pojo.Books;

public interface AdminDAO 
{
	//Declaring the Methods in AdminDAO
	public void insertBooks(Books bobj);
	public void updateBooks(Books bobj);
	public void deleteBooks(Books bobj);
	public List<Books> retreiveBooks();
	public List<Books> totalBooks();
	public List<Books> displayAutoBioGraphyBooks();
	public List<Books> priceLowToHighBooks();
	public List<Books> priceHighToLowBooks();
	public List<Books> bestSellingBooksWise();
	public List<Admin> retreiveAdmin();
	public void insertUsingMap();
	
}
