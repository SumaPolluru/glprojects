package com.gl.main.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.gl.main.connect.DataConnect;
import com.gl.main.pojo.Admin;
import com.gl.main.pojo.BookClassForCollection;
import com.gl.main.pojo.Books;

public class AdminDAOImpl implements AdminDAO 
{

	private Connection con1;
	private PreparedStatement stat;
	private Map<Integer, BookClassForCollection> mapBook;
	Set<Admin> adminlist;

	public AdminDAOImpl() 
	{
		con1 = DataConnect.getConnect();
		adminlist = new HashSet<Admin>();
		mapBook = new HashMap<Integer, BookClassForCollection>();
	}

	@Override
	public void insertUsingMap() //Using Map(key,Value)
	{
		mapBook.put(10, new BookClassForCollection(10, "Do It", "Luther", "Motivation of life"));
		System.out.println(mapBook);

	}

	@Override
	public List<Admin> retreiveAdmin() //Displaying total Admin details
	{
		List<Admin> adminlist = new ArrayList<Admin>();
		try 
		{
			stat = con1.prepareStatement("select * from Admin");
			ResultSet result = stat.executeQuery();
			while (result.next()) 
			{
				Admin e1 = new Admin();
				e1.setAdminId(result.getLong(1));
				e1.setAdminPassword(result.getString(2));
				e1.setAdminName(result.getString(3));
				adminlist.add(e1);
			}
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return adminlist;
	}

	@Override
	public void insertBooks(Books bobj) //inserting values into table
	{
		try {
			stat = con1.prepareStatement("insert into Books values(?,?,?,?,?,?,?,?)");
			stat.setInt(1, bobj.getBookId());
			stat.setString(2, bobj.getBookName());
			stat.setString(3, bobj.getBookAuthorName());
			stat.setString(4, bobj.getBookDescription());
			stat.setString(5, bobj.getBookGenre());
			stat.setDouble(6, bobj.getBookPrice());
			stat.setInt(7, bobj.getNoOfCopies());
			stat.setInt(8, bobj.getNoOfCopiesSold());
			int result = stat.executeUpdate();
			if (result > 0) {
				System.out.println("Data inserted");
			}
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void updateBooks(Books bobj)
	{
		try {
			stat = con1.prepareStatement("update Books set BookName=?,BookAuthorName=?,BookDescription=?,BookGenre=?,BookPrice=?,NoOfCopies=?,NoOfCopiesSold=? where BookId=?");
			stat.setString(1, bobj.getBookName());
			stat.setString(2, bobj.getBookAuthorName());
			stat.setString(3, bobj.getBookDescription());
			stat.setString(4, bobj.getBookGenre());
			stat.setDouble(5, bobj.getBookPrice());
			stat.setInt(6, bobj.getNoOfCopies());
			stat.setInt(7, bobj.getNoOfCopiesSold());
			stat.setInt(8, bobj.getBookId());
			int result = stat.executeUpdate();
			if (result > 0) {
				System.out.println("Data updated success");
			}
		} 
		catch (Exception ex)
		{
			System.out.println(ex.getMessage());
		}
	}

	@Override
	public void deleteBooks(Books bobj) 
	{
		try {
			stat = con1.prepareStatement("delete from books where bookId=?");
			stat.setInt(1, bobj.getBookId());
			int result = stat.executeUpdate();
			if (result > 0) {
				System.out.println("Data deletion success");
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

	}

	@Override
	public List<Books> retreiveBooks() 
	{
		List<Books> booklist = new ArrayList<Books>();
		try {
			stat = con1.prepareStatement("select * from Books");

			ResultSet result = stat.executeQuery();
			while (result.next()) {
				Books e1 = new Books();
				e1.setBookId(result.getInt(1));
				e1.setBookName(result.getString(2));
				e1.setBookAuthorName(result.getString(3));
				e1.setBookDescription(result.getString(4));
				e1.setBookGenre(result.getString(5));
				e1.setBookPrice(result.getDouble(6));
				e1.setNoOfCopies(result.getInt(7));
				e1.setNoOfCopiesSold(result.getInt(8));
				booklist.add(e1);
			}
		} 
		catch (SQLException e) 
		{
			
			e.printStackTrace();
		}

		return booklist;
	}

	@Override
	public List<Books> totalBooks() //TotalBooks in Book Store
	{
		List<Books> booklist = new ArrayList<Books>();
		try {
			stat = con1.prepareStatement("select count(BookId) from Books");
			ResultSet result = stat.executeQuery();
			while (result.next()) 
			{
				Books e1 = new Books();
				e1.setBookId(result.getInt(1));
				booklist.add(e1);
			}
		} 
		catch (SQLException e)
		{
			
			e.printStackTrace();
		}

		return booklist;
	}

	@Override
	public List<Books> displayAutoBioGraphyBooks()
	{
		List<Books> booklist = new ArrayList<Books>();
		try {
			stat = con1.prepareStatement("select BooKId,BookName,BookGenre from Books where BookGenre='Autobiography'");
			ResultSet result = stat.executeQuery();
			while (result.next()) 
			{
				Books e1 = new Books();
				e1.setBookId(result.getInt(1));
				e1.setBookName(result.getString(2));
				e1.setBookGenre(result.getString(3));
				booklist.add(e1);
			}
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return booklist;
	}

	@Override
	public List<Books> priceLowToHighBooks() 
	{
		List<Books> booklist = new ArrayList<Books>();
		try {
			stat = con1.prepareStatement("select * from Books order by BookPrice asc");

			ResultSet result = stat.executeQuery();
			while (result.next()) {
				Books e1 = new Books();
				e1.setBookId(result.getInt(1));
				e1.setBookName(result.getString(2));
				e1.setBookAuthorName(result.getString(3));
				e1.setBookDescription(result.getString(4));
				e1.setBookGenre(result.getString(5));
				e1.setBookPrice(result.getDouble(6));
				e1.setNoOfCopies(result.getInt(7));
				e1.setNoOfCopiesSold(result.getInt(8));
				booklist.add(e1);
			}
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}

		return booklist;
	}

	@Override
	public List<Books> priceHighToLowBooks() 
	{
		List<Books> booklist = new ArrayList<Books>();
		try {
			stat = con1.prepareStatement("select * from Books order by BookPrice desc");

			ResultSet result = stat.executeQuery();
			while (result.next()) {
				Books e1 = new Books();
				e1.setBookId(result.getInt(1));
				e1.setBookName(result.getString(2));
				e1.setBookAuthorName(result.getString(3));
				e1.setBookDescription(result.getString(4));
				e1.setBookGenre(result.getString(5));
				e1.setBookPrice(result.getDouble(6));
				e1.setNoOfCopies(result.getInt(7));
				e1.setNoOfCopiesSold(result.getInt(8));
				booklist.add(e1);
			}
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
		}

		return booklist;
	}

	@Override
	public List<Books> bestSellingBooksWise() //Best Selling Books
	{
		List<Books> booklist = new ArrayList<Books>();
		try 
		{
			stat = con1	.prepareStatement("select BookId,BookName,NoOfCopiesSold from Books order by NoOfCopiesSold desc ");
			ResultSet result = stat.executeQuery();
			while (result.next()) {
				Books e1 = new Books();
				e1.setBookId(result.getInt(1));
				e1.setBookName(result.getString(2));
				e1.setNoOfCopiesSold(result.getInt(3));
				booklist.add(e1);
			}
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return booklist;
	}
}