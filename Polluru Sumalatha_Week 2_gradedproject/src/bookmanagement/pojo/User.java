package bookmanagement.pojo;
//Here,The package name is bookmanagement.pojo
public class User 
{
		private String userName;
		private int userId;
		private String email;
		private String password;
		
		public String getUserName()
		{
			return userName;
		}
		public void setUserName(String userName)
		{
			this.userName = userName;
		}
		public int getUserId()
		{
			return userId;
		}
		public void setUserId(int userId)
		{
			this.userId = userId;
		}
		public String getEmail()
		{
			return email;
		}
		public void setEmail(String email)
		{
			this.email = email;
		}
		public String getPassword()
		{
			return password;
		}
		public void setPassword(String password) 
		{
			this.password = password;
		}
}
