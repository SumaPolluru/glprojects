package bookmanagement.pojo;
//Here,The package name is bookmanagement.pojo
import java.util.Objects;
//Here,The Class name is Book
public class Book
{
	    public int bookId;
		public String bookName;
		public String authorName;
	    public String description;
	    
	    //Default constructor
	    Book()
	    {
	    	
	    }
	    
	    //Parameterized constructor to initialize the data into collection
		public Book(int bookId, String bookName, String authorName, String description) 
		{
			super();
			this.bookId = bookId;
			this.bookName = bookName;
			this.authorName = authorName;
			this.description = description;
		}
		
		//Using toString method to fetch the data from the collections rather then address of object
		@Override
		public String toString()
		{
			return "BookID:" + bookId +"\nBookName:" + bookName +" \nAuthorName:" + authorName + " \nDescription:"	+ description ;
		}	
 }

