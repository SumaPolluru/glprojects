package bookmanagement.dao;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import book.exceptionhandling.CustomException;
import bookmanagement.pojo.Book;
import bookmanagement.dao.*;
//Here,Class UserDAOImplDriverClass is implements from interface UserDAO
public class BookDAOImplDriverClass implements BookDAO{
	private List<Book> newBooks;  //Here,Using Collection Interface(List) to store Object Informations 
	private List<Book> favouriteBooks;
	private List<Book> completedBooks;
	private Scanner sc;
	public BookDAOImplDriverClass()  //Constructor for class
	{
		
			newBooks=new ArrayList<Book>();
			favouriteBooks=new ArrayList<Book>();
			completedBooks=new ArrayList<Book>();
			sc=new Scanner(System.in);
			
			//Initializing the collection with Book objects for NewBooks
			newBooks.add(new Book(10,"Wings of fire","Dr. Abdul Kalam","This book explore important issues of family"));
			newBooks.add(new Book(20,"The Key to Success","Jim Rohn","This book explores,focus with a positive attitude"));
			newBooks.add(new Book(30,"You Can","George Adams","This book promote personal growth and well-being"));	
			newBooks.add(new Book(40,"A Million Thoughts","Om Swami","This book shows how to meditate correctly"));	
			newBooks.add(new Book(50,"Believe in Yourself","Joseph Murphy","This book is a self-help book about self-esteem"));	
			newBooks.add(new Book(55,"You Can","George Adams","This book promote personal growth and well-being"));
			
			
			//Initializing the collection with Book objects for FavouriteBooks
			favouriteBooks.add(new Book(60,"The Mind and its Control","Swami Budhananda","This book is an excellent compendium of teachings from Yoga-sutras."));	
			favouriteBooks.add(new Book(70,"Master Your Emotions","Leonard","This book is practical guide to overcome negativity "));
			favouriteBooks.add(new Book(80,"Think and Grow Rich","Napoleon Hill","This book promote wealthy and successful people common habits"));	
			favouriteBooks.add(new Book(90,"Atomic Habits","James Clear","This book definitive guide to breaking bad behaviors"));
			favouriteBooks.add(new Book(100,"One Day life will Change","Saranya Umakanthan","This book about a story of love and inspiration to win life"));	
			favouriteBooks.add(new Book(105,"Think and Grow Rich","Napoleon Hill","This book promote wealthy and successful people common habits"));
			
			
			//Initializing the collection with Book objects for completedBooks
			completedBooks.add(new Book(110,"Do It Today","Darius Foroux","This book explore the goals is of big help"));	
		    completedBooks.add(new Book(120,"Mindest","Carol Dweck","This book explores,success comes from having the right mindset rather than intelligence"));
		    completedBooks.add(new Book(130,"The Secret","George Adams","This book promote personal growth and well-being"));	
		    completedBooks.add(new Book(140,"A Girl that had to be Strong","Om Swami","This book shows how to meditate correctly"));
		    completedBooks.add(new Book(150,"The Practicing Mind","Joseph Murphy","This book is a self-help book about self-esteem and confidence"));	
		    completedBooks.add(new Book(160,"Think and Grow Rich","Napoleon Hill","This book promote wealthy and successful people common habits"));
		   	    
		}
	
	@Override
	public void seeNewBooksUser1() //Methods using to declare and get objects 
	{
		System.out.println("Username : Suma");
		System.out.println("!--------Your New books Details--------!");
		System.out.println(newBooks.get(0));
		System.out.println(newBooks.get(1));
	
	}
	@Override
	public void seeNewBooksUser2() //By using these methods,We can fetch the data of Book object
	{
		System.out.println("Username : Polluru");
		System.out.println("!--------Your New books Details --------!");
		System.out.println(newBooks.get(2));
		System.out.println(newBooks.get(3));
	}	
	@Override
	public void seeNewBooksUser3() 
	{
		System.out.println("Username : Gayathri");
		System.out.println("!--------Your New books Details--------!");	
		System.out.println(newBooks.get(4));
		System.out.println(newBooks.get(5));
	}
	@Override
	public void seeFavouriteBooksUser1() 
	{
		System.out.println("Username : Suma");
		System.out.println("!--------Your Favourite books Details--------!");
		System.out.println(favouriteBooks.get(0));
		System.out.println(favouriteBooks.get(1));
	}	
	@Override
	public void seeFavouriteBooksUser2() 
	{
		System.out.println("Username : Polluru");
		System.out.println("!--------Your Favourite books Details--------!");
		System.out.println(favouriteBooks.get(2));
		System.out.println(favouriteBooks.get(3));
	}
	@Override
	public void seeFavouriteBooksUser3() 
	{
		System.out.println("Username : Gayathri");
		System.out.println("!--------Your Favourite books Details--------!");
		System.out.println(favouriteBooks.get(4));
		System.out.println(favouriteBooks.get(5));
	}
	@Override
	public void seeCompletedBooksUser1() 
	{
		System.out.println("Username : Suma");
		System.out.println("!--------Your Completed books Details--------!");
		System.out.println(completedBooks.get(0));
		System.out.println(completedBooks.get(1));
	}
	@Override
	public void seeCompletedBooksUser2() 
	{
		System.out.println("Username : Polluru");
		System.out.println("!--------Your Completed books Details--------!");
		System.out.println(completedBooks.get(2));
		System.out.println(completedBooks.get(3));
	}		
	@Override
	public void seeCompletedBooksUser3()
	{
		System.out.println("Username : Gayathri");
		System.out.println("!--------Your Completed books Details--------!");
		System.out.println(completedBooks.get(4));
		System.out.println(completedBooks.get(5));
	}	
	@Override
	public void getBookDetailsUser1()
	{
		System.out.println("Username : Suma");
		System.out.println("!--------Your Complete books details are--------!");
		System.out.println(newBooks.get(0));
		System.out.println(newBooks.get(1));
		System.out.println(favouriteBooks.get(0));
		System.out.println(favouriteBooks.get(1));
		System.out.println(completedBooks.get(0));
		System.out.println(completedBooks.get(1));
	}			
	@Override
	public void getBookDetailsUser2() 
	{
		System.out.println("Username : Polluru");
		System.out.println("!--------Your Complete books details are--------!");
		System.out.println(newBooks.get(2));
		System.out.println(newBooks.get(3));
		System.out.println(favouriteBooks.get(2));
		System.out.println(favouriteBooks.get(3));
		System.out.println(completedBooks.get(2));
		System.out.println(completedBooks.get(3));
	}			
	@Override
	public void getBookDetailsUser3() 
	{
		System.out.println("Username : Gayathri");
		System.out.println("!--------Your Complete books details are--------!");
		System.out.println(newBooks.get(4));
		System.out.println(newBooks.get(5));
		System.out.println(favouriteBooks.get(4));
		System.out.println(favouriteBooks.get(5));
		System.out.println(completedBooks.get(4));
		System.out.println(completedBooks.get(5));
	}			
	@Override
	public void selectBookUser1() throws CustomException
	{
	/*Here,we are declaring the array,due to the objects are not storing in single collection object
	*To fetch the data using for loop is not possible,due to storing in different collection list
	*like(NewBooks,FavouriteBooks,CompletedBooks).
	*So,We chosen this method.To get the details of book object.
	**/
		
	int arr[]= {10,20,60,70,110,120};
	System.out.println("Please enter Book ID");
	int bookID=sc.nextInt();
	if(bookID<0)
	{
		throw new CustomException();
	}
	switch(bookID)
	{
	case 10: 			
				System.out.println(newBooks.get(0));	
								break;
	case 20:
				System.out.println(newBooks.get(1));		
								break;			
	case 60:   
				System.out.println(favouriteBooks.get(0));
								break;
	case 70:   
				System.out.println(favouriteBooks.get(1));
								break;
	case 110:
				System.out.println(completedBooks.get(0));
								break;
	case 120:
				System.out.println(completedBooks.get(1));
								break;	
    default:
    			System.out.println("Book Id is not present");
	  }	
	}
	
	@Override
	public void selectBookUser2() throws CustomException {
	int arr[]= {30,40,80,90,130,140};
	System.out.println("Please enter Book ID");
	int bookID=sc.nextInt();
	if(bookID<0) 
	{
		throw new CustomException();
	}
	switch(bookID)
	{
	case 30:   
				System.out.println(newBooks.get(2));
								break;
	case 40:
				System.out.println(newBooks.get(3));
								break;
	case 80:
				System.out.println(favouriteBooks.get(2));
								break;
	case 90:
				System.out.println(favouriteBooks.get(3));
								break;
	case 130: 
				System.out.println(completedBooks.get(2));	
								break;
	case 140:  
				System.out.println(completedBooks.get(3));				
								break;
	default:
	    	   	System.out.println("Book Id is not present");			
		  }	
	}
	
	@Override
	public void selectBookUser3() throws CustomException
	{
	int arr[]= {50,55,100,105,150,160};
	System.out.println("Please enter Book ID");
	int bookID=sc.nextInt();
	if(bookID<0) 
	{
		throw new CustomException();
	}
	switch(bookID) 
	{ 
	case 50: 
				System.out.println(newBooks.get(4));		 	
							break;
	case 55:
				System.out.println(newBooks.get(5));
							break;
	case 100:
		 		System.out.println(favouriteBooks.get(4));
							break;
	case 105:  
				System.out.println(favouriteBooks.get(5));				
						    break;
	case 150:   
				System.out.println(completedBooks.get(4));			
							 break;
	case 160:
				System.out.println(completedBooks.get(5));
							break;
    default:
	    	    System.out.println("Book Id is not present");
			
		  }
	}
}
