package mainpackage; //MagicBooks is MainClass,Run this class
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import book.exceptionhandling.CustomException;
import bookmanagement.dao.BookDAOImplDriverClass;
import bookmanagement.pojo.User;
import loggerpackage.LogClass;

public  class MagicBooks extends Thread
{
		static Scanner sc;
		private List<User> userlist;
		
		static String user[]= new String[3]; //Here creating array for user information
		{
		user[0]="Suma";  //Here,Initializing the array
		user[1]="Polluru";
		user[2]="Gayathri"; 
		/*Please Use Username and password as these for entry to Book Management System
		 * Or else Its not possible for entry[Remember this statement] */
		}
		static String pwd[]= new String[3];
		{
			pwd[0]="Suma12";
			pwd[1]="Chintusree";
			pwd[2]="Gaye67";
		}
		
		public MagicBooks() //Constructor of class
		{
			sc=new Scanner(System.in);		
		}
		
		public void run() //Thread
		{
		userValidation(sc.next(),sc.next());
		}
		
public static void main(String[] args) throws SecurityException, IOException //Main Method
{ 
	    LogClass lobj=new LogClass();//Creating object for LogClass
	    lobj.logMethod();	    
	    MagicBooks mainobj=new MagicBooks(); //Creating object for MainClass    
	    Thread t=new Thread();//Creating Thread
		t.start();
		System.out.println("!-------------WELCOME TO BOOK MANAGEMENT SYSTEM-------------!");
		System.out.println("Please Enter username");
		String username = sc.next();
		System.out.println("please Enter password");
		String password=sc.next();
		mainobj.userValidation(username,password);
		
         // It's checking that username from the data for further process
		if(username.equalsIgnoreCase(user[0])&& password.equals(pwd[0]))
		{
				System.out.println("!--------------Welcome Suma-----------!");
				System.out.println("Please enter 1"); //Here,Using different numbers to be more secure	
		}
		else if(username.equalsIgnoreCase(user[1])&& password.equals(pwd[1]))
		{
				System.out.println("!............Welcome Polluru............!");
				System.out.println("Please Enter 5"); //Here,Using different numbers to be more secure
		}
		else if(username.equalsIgnoreCase(user[2])&& password.equals(pwd[2])) 
		{
				System.out.println("!..........Welcome Gayathri..........!");
				System.out.println("Please Enter 8"); //Here,Using different numbers to be more secure
		}
		else 
		{
			System.out.println("");
		}
		  int choice =sc.nextInt();
		  	switch(choice) 
		  	{
		  	case 1:  
						mainobj.user1Menu(); //	Here,Calling the method,which is declared in UserDAOImplDriverClass Class by using main method object
		        			break;
		  	case 5:                          //Here,Using different numbers to be more secure
			 	 		mainobj.user2Menu();
			 	 			break;
		  	case 8: 	
						mainobj.user3Menu();
							break;
		  	default:
		  			System.out.println("!------Invalid option,Please Try again-------!");
		  	}
		}

	   public void userValidation(String username,String password) //validating the User
	   {
		   boolean x=false; //Here,Its checking that entered username is present or not
		   {
			   for(int j=0;j<user.length;j++)
			   	{
				   if(username.equalsIgnoreCase(user[j])&& password.equals(pwd[j]))
				   {
					   x=true;
					   break;
				   }
			   	}    
			   if(x) 
			   {
				   System.out.println("Username and Password are Checked & Verified");
				   System.out.println(username+": You can proceed");
			   }
			   else 
			   {
				   System.out.println("Username & password doesnot exist");
				   System.out.println("The list of users are only accessible with correct password  "+user[0]+","+user[1]+","+user[2]);
				   System.out.println("Please re-run again and enter above username for entry");
				   String name=sc.next();
			   }
		   }
	   }
		public void user1Menu() //User1 menu method
		{
		BookDAOImplDriverClass uobj=new BookDAOImplDriverClass();
		String ch="y";
		while(ch.equals("y"))
		{
			System.out.println("1. See your new books ");
			System.out.println("2. See your favourite books");
			System.out.println("3. See your completed books");
			System.out.println("4. Select the book in the book list");
			System.out.println("5. Get the details of book");
			System.out.println("6. Exit");
			int option=sc.nextInt();
			switch(option)
			{

			case 1:
						uobj.seeNewBooksUser1(); //These, methods are calling from UserDAOImplDriverClass using UserDAOImplDriverClass object
								break;
			case 2:
						uobj.seeFavouriteBooksUser1();
								break;
			case 3:
						uobj.seeCompletedBooksUser1();
								break;
			case 4:  	try 
							{
							uobj.selectBookUser1();
							}
			         	catch(CustomException e)
							{
			         			System.out.println(e.getMessage());
							}
								break;
			case 5:
				    	uobj.getBookDetailsUser1();
				    			break;
			case 6:
						System.out.println("Successfully logout, Thank You!");
						System.exit(0);
								break;		
		    default:
		    			System.out.println("Invalid");
			}
		System.out.println("Do u want to continue(y/n)");
		ch=sc.next();
		}
		}
		
		public void user2Menu() //Here, declaring User2 Menu
		{
		BookDAOImplDriverClass uobj=new BookDAOImplDriverClass();
		String ch="y";
		while(ch.equals("y"))
		{
			System.out.println("1. See your new books ");
			System.out.println("2. See your favourite books");
			System.out.println("3. See your completed books");
			System.out.println("4. Select the book in the book list");
			System.out.println("5. Get the details of book");
			System.out.println("6. Exit");
			int option=sc.nextInt();
			switch(option)
			{

			case 1:
					uobj.seeNewBooksUser2();
							break;
			case 2:
				    uobj.seeFavouriteBooksUser2();
				    		break;
			case 3:
				   uobj.seeCompletedBooksUser2();
				   			break;
			case 4: 	try
						{
							uobj.selectBookUser2();
						}
						catch(CustomException e) 
						{
							System.out.println(e.getMessage());
						}
								break;
			case 5:
						uobj.getBookDetailsUser2();
								break;
			case 6:
				 		System.out.println("Successfully logout, Thank You!");
				 		System.exit(0);
				 				break;		
		    default:
		    			System.out.println("Invalid");
			}
		System.out.println("Do u want to continue(y/n)");
		ch=sc.next();
		}
		}
		public void user3Menu()
		{
		BookDAOImplDriverClass uobj=new BookDAOImplDriverClass();
		String ch="y";
		while(ch.equals("y"))
		{
			System.out.println("1. See your new books ");
			System.out.println("2. See your favourite books");
			System.out.println("3. See your completed books");
			System.out.println("4. Select the book in the book list");
			System.out.println("5. Get the details of book");
			System.out.println("6. Exit");
			int option=sc.nextInt();
		    switch(option)
			{

			case 1:
						uobj.seeNewBooksUser3();
								  break;
			case 2:
						uobj.seeFavouriteBooksUser3();
								  break;
			case 3:
						uobj.seeCompletedBooksUser3();
								  break;
			case 4: 
						try 
						{
							uobj.selectBookUser3();
						}
						catch(CustomException e) 
						{
							System.out.println(e.getMessage());
		                 }
				 				   break;
			case 5:
				    	uobj.getBookDetailsUser3();
				    				break;
			case 6:
				 		System.out.println("Successfully logout, Thank You!");
				 		System.exit(0);
				 					 break;		
		    default:
		    	 		System.out.println("Invalid");
			}
		System.out.println("Do u want to continue(y/n)");
		ch=sc.next();
		}
		}
}

/*As per my knowledge, I done this assessment
                  Thank You!*/

