package book.exceptionhandling;
//Here,Custom Exception extends from the super class of Exception
	public class CustomException extends Exception
	{
		@Override
		public String getMessage()
		{
			return "BookId should not be negative";
		}
	}
