<%@taglib uri = "http://www.springframework.org/tags/form"
 prefix = "form"%>

<html>
   <head>
      <title>Spring MVC Form Handling</title>
   </head>

   <body>
   <div align="center">
  <h1> <form >***Item Form Page***</form></h1>
      <form:form method = "POST" action = "itemsSave" modelAttribute="itemData">
         <table>
           
            <tr>
               <td><form:label path = "itemName">itemName</form:label></td>
               <td><form:input path = "itemName" /></td>
            </tr>
          
            <tr>
               <td><form:label path = "prize">prize</form:label></td>
               <td><form:input path = "prize" /></td>
            </tr>
            <tr>
               <td colspan = "2">
                  <input type = "submit" value = "SAVE-ITEM"/>
               </td>
            </tr>
         </table>  
      </form:form>
    </div>
   </body>
   
</html>