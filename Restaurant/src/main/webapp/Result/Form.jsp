<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<html>
<head>
<title>Spring MVC Form Handling</title>
</head>

<body>
	<div align="center">
<h2>***** Enter User Id to get user bills*****</h2>
		<form:form method="POST" action="/check"
			modelAttribute="data">
			<table>
				<tr>
					<td><form:label path="userid">userId</form:label></td>
					<td><form:input path="userid" /></td>
				</tr>


				<tr>
					<td colspan="2">
						<center>
							<input type="submit" value="CHECK" />
						</center>
					</td>
				</tr>
			</table>
		</form:form>
				
		
	</div>
</body>

</html>