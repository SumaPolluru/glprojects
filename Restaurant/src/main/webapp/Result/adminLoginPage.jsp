<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<html>
<head>
<title>Spring MVC Form Handling</title>
</head>

<body>
	<div align="center">
<h2>***** Admin Login Page*****</h2>
		<form:form method="POST" action="validateAdminLogin"
			modelAttribute="adminLoginData">
			<table>
				<tr>
					<td><form:label path="email">Email</form:label></td>
					<td><form:input path="email" /></td>
				</tr>

				<tr>
					<td><form:label path="Password">Password</form:label></td>
					<td><form:input path="Password" /></td>
				</tr>

				<tr>
					<td colspan="2">
						<center>
							<input type="submit" value="LOGIN" />
						</center>
					</td>
				</tr>
			</table>
		</form:form>
		<h2 style="color: red;">${LoginErrorMessage}</h2>
	</div>
</body>

</html>