<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div align="center">
		<h1>****** User List ******</h1>
		<h3>
			<a href="userForm">New User</a>
		</h3>
		<table border="1">
			
			<th>userId</th>
			<th>firstName</th>
			<th>lastName</th>
			<th>Password</th>
			<th>Email</th>
			<th>Action</th>

			<c:forEach var="contact" items="${userList}" varStatus="status">
				<tr>

					<td>${contact.userId}</td>
					<td>${contact.firstName}</td>
					<td>${contact.lastName}</td>
					<td>${contact.password}</td>
					<td>${contact.email}</td>
					<td><a href="/updateUser/${contact.userId}">Edit</a>
					 &nbsp;&nbsp;&nbsp;&nbsp;
					 <a href="/deleteuser/${contact.userId}">Delete</a></td>
				</tr>
				
			</c:forEach>
		</table>
		<br>
		<h3><a href="backTotoAdminDashBoard">Back To Admin DashBoard</a></h3>
	</div>

</body>
</html>