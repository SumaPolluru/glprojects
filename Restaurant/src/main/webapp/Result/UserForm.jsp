<%@taglib uri = "http://www.springframework.org/tags/form"
 prefix = "form"%>

<html>
   <head>
      <title>Spring MVC Form Handling</title>
   </head>

   <body>
   <div align="center">
  <h1> <form >***User Form***</form></h1>
      <form:form method = "POST" action = "/saveUser" modelAttribute="userData">
         <table>
            <tr>
               <td><form:label path = "firstName">FirstName</form:label></td>
               <td><form:input path = "firstName" /></td>
            </tr>
            <tr>
               <td><form:label path = "lastName">LastName</form:label></td>
               <td><form:input path = "lastName" /></td>
            </tr>
            <tr>
               <td><form:label path = "email">Email</form:label></td>
               <td><form:input path = "email" /></td>
            </tr>
            <tr>
               <td><form:label path = "password">Password</form:label></td>
               <td><form:input path = "password" /></td>
            </tr>
            <tr>
               <td colspan = "2">
                  <input type = "submit" value = "Save"/>
               </td>
            </tr>
         </table>  
      </form:form>
    </div>
   </body>
   
</html>