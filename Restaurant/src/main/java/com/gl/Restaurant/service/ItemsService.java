package com.gl.Restaurant.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.gl.Restaurant.Repositories.ItemRepositories;

import com.gl.Restaurant.model.Items;

@Service // service where we write all business logic
public class ItemsService {
	@Autowired
	ItemRepositories itemsRep;

	// method to get item

	public Items getItemId(int id) {
		return itemsRep.getById(id);

	}

	// method to save item
	public Items saveItems(Items item) {
		return itemsRep.save(item);
	}

	// get all items
	public List<Items> getAllItems() {
		List<Items> itemsList = new ArrayList<Items>();
		itemsRep.findAll().forEach(item -> itemsList.add(item));
		return itemsList;
	}

	// getting a specific record by using the method findById() of jpaRepository
	public Items getItemssById(int id) {
		return itemsRep.findById(id).get();

	}

	// deleting a specific record by using the method deleteById() of jpaRepository
	public void deleteItem(int id) {
		itemsRep.deleteById(id);
	}

	// update
//	public void updateItem1( Items item) {
//		List<Items> itemList = getAllItems();
//		for (Items i : itemList) {
//			if(i.getItemId() == item.getItemId()) {
//				itemList.remove(item);
//				break;
//			}
//			itemList.add(item);
//			itemsRep.save(item);
//		}
//		
//	}
	// update a order from item table
	public void updateItem(Items item) {
		itemsRep.save(item);
	}

}
