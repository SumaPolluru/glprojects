package com.gl.Restaurant.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gl.Restaurant.Repositories.UserRepositories;

import com.gl.Restaurant.model.User;

@Service // service where we write all business logic
public class UserCrudService {

	@Autowired
	UserRepositories userRep;

	// method to get item
	public User getUserId(int id) {
		return userRep.getById(id);
	}

	// method to save item
	public User saveUser(User u) {
		return userRep.save(u);
	}

	// get all users
	public List<User> getAllUsers() {
		List<User> uList = new ArrayList<User>();
		userRep.findAll().forEach(user -> uList.add(user));
		return uList;
	}

	// getting a specific record by using the method findById() of jpaRepository
	public User getUserById(int id) {
		return userRep.findById(id).get();
	}

	// deleting a specific record by using the method deleteById() of jpaRepository
	public void deleteUser(int id) {
		userRep.deleteById(id);
	}

	// to update a user
	public void updateUser(User user) {
		userRep.save(user);
	}

//	public void updateUser(User user) {
//		List<User> userList=getAllUsers();
//		for(User u:userList) {
//			if(u.getUserId()==user.getUserId()) {
//				userList.remove(user);
//				break;
//			}
//			userList.add(user);
//			userRep.save(user);
//		}
//	}	
//	

//	//update a order from user
//		public User updateUser(User user) {
//			Integer userid=user.getUserId();
//			User u= userRep.findById(userid).get();
//			u.setUserId(user.getUserId());
//			u.setFirstName(user.getFirstName());
//			u.setLastName(user.getLastName());
//			u.setPassword(user.getPassword());
//			return userRep.save(u);
//		}
//		

}
