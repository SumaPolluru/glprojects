package com.gl.Restaurant.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.gl.Restaurant.Repositories.UserRepositories;
import com.gl.Restaurant.model.Items;
import com.gl.Restaurant.model.User;
import com.gl.Restaurant.service.ItemsService;

import java.util.List;

import javax.servlet.http.HttpSession;

@Controller
public class UserController {
	@Autowired // getting object of user Repositories
	UserRepositories userRep;

	@Autowired // getting object of item service
	ItemsService itemService;

	// mapping to get welcome page
	@GetMapping("/")
	public String getwelcomePage() {
		return "welcome";
	}

	// mapping to get user dashboard
	@RequestMapping("/dashBoard")
	public ModelAndView getUserDashBoard(HttpSession session) {

		User u = (User) session.getAttribute("user");

		System.out.println("============");
		System.out.println("id is---->" + u.getUserId());
		System.out.println("============");

		List<Items> itemList = itemService.getAllItems();

		return new ModelAndView("UserDashBoard", "itemList", itemList);
	}

	// mapping to get login page
	@RequestMapping("/loginPage")
	public ModelAndView getUserLoginPage(@ModelAttribute("LoginErrorMessage") String errorMessage) {
		ModelAndView modelAndView = new ModelAndView("userLoginPage", "loginData", new User());
		if (errorMessage == null) {

			return modelAndView;
		} else {
			modelAndView.addObject("LoginErrorMessage", errorMessage);
			return modelAndView;
		}
	}

	// mapping to validateuser
	@RequestMapping("/validateUserLogin")
	public ModelAndView validateUser(@ModelAttribute("loginData") User u, HttpSession session) {

		java.util.Optional<User> uList = userRep.userValidation(u.getEmail(), u.getPassword());

		System.out.println("--------");

		User obj = uList.get();

		session.setAttribute("user", obj);// storing login information

		System.out.println(obj.getUserId());
		System.out.println(obj.getEmail());
		System.out.println(obj.getPassword());

		User uFind = null;
		if (uList.isPresent()) {

			System.out.println("user found");

			uFind = uList.get();
			return new ModelAndView("redirect:/dashBoard");

		} else {
			System.out.println("User not found");
			return new ModelAndView("userLoginPage", "LoginErrorMessage", "Invalid username or password");
		}

	}

	// mapping to get registration
	@GetMapping("/registration")
	public ModelAndView getRegistration_Form() {

		return new ModelAndView("userRegisterPage", "registerData", new User());
	}

	// mapping to register
	@PostMapping("/Register")
	public ModelAndView saveRegistrtionData(@ModelAttribute("registerData") User u) {
		userRep.save(u);
		return new ModelAndView("redirect:/loginPage");
	}

	// mapping to logout
	@RequestMapping("/logout")
	public String logout(HttpSession session) {
		User u = (User) session.getAttribute("user");
		session.invalidate();
		return "redirect:/loginPage";
	}

	// mapping to to get bact to user dashboard
	@RequestMapping("/backTotoUserDashBoard")
	public String backTotoUserDashBoard(HttpSession session) {
		User u = (User) session.getAttribute("user");
		return "redirect:/dashBoard";
	}
}
