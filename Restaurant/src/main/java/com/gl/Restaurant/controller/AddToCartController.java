package com.gl.Restaurant.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.gl.Restaurant.Repositories.AddToCartRepositories;
import com.gl.Restaurant.model.AddToCart;
import com.gl.Restaurant.model.Items;
import com.gl.Restaurant.model.User;
import com.gl.Restaurant.service.AddToCartService;
import com.gl.Restaurant.service.ItemsService;

@Controller
public class AddToCartController {

	@Autowired // getting object of AddToCartService
	AddToCartService addToCartService;

	@Autowired // getting object of AddToCartRepositories
	AddToCartRepositories cartRep;
	@Autowired // getting object of item service
	ItemsService itemService;

	// mapping to addToCart-DashBoard
	@RequestMapping("/addToCart-DashBoard")
	public ModelAndView addToCartDashBoard(HttpSession session) {
		User u = (User) session.getAttribute("user");
		List<AddToCart> c = cartRep.getcartDetails(u); // addToCartService.getAllOrders();
		ModelAndView model = new ModelAndView("edit");

		int id = u.getUserId();
		model.addObject("u", id);
		model.addObject("addToCart", c);
		model.setViewName("addToCartDashBoard");

		System.out.println("userid of a cart =" + u.getUserId());

		System.out.println("tp-->" + cartRep.getTotalPrice(u));
		System.out.println("");
		System.out.println("");
		return model;

	}

	// mapping to buy a item
	@RequestMapping("/buy/{itemId}")
	public ModelAndView buy(@PathVariable("itemId") int id, HttpSession session) {

		Items i = itemService.getItemssById(id);
		AddToCart carts = new AddToCart();

		User u = (User) session.getAttribute("user");
		System.out.println("uid when buy " + u.getUserId());
		carts.getOrderId();

		carts.setItemId(i.getItemId());
		carts.setItemName(i.getItemName());
		carts.setPrize((int) i.getPrize());
		carts.setQuntity(0);
		carts.setDateCreated(LocalDate.now());
		carts.setStatus("Available");
//		carts.setUserid(u);
		return new ModelAndView("orderForm", "orderDetails", carts);
	}

	// mapping to SAVEORDER
	@RequestMapping("/SAVEORDER")
	public String saveOrder(@ModelAttribute("orderDetails") AddToCart cart, HttpSession session) {
		User u = (User) session.getAttribute("user");
		int tprice = 0;
		tprice = cart.getPrize() * cart.getQuntity();
		System.out.println("total price=" + tprice);
		cart.setTotalPrice(tprice);
		cart.setUserid(u);
		cart.getOrderId();
		cart.getPrize();
		addToCartService.saveOrder(cart);

		return "redirect:/addToCart-DashBoard";
	}

	// mapping to deleteOrder
	@RequestMapping("/deleteOrder/{orderId}")
	public ModelAndView deleteOrder(@PathVariable("orderId") int orderId, HttpSession session) {
		User u = (User) session.getAttribute("user");
		System.out.println("order deleted");
		addToCartService.deleteOrder(orderId);
		return new ModelAndView("redirect:/addToCart-DashBoard");
	}

	// mapping to updateORDER form
	@RequestMapping("/updateOrder/{orderId}")
	public ModelAndView updateOrder(@PathVariable("orderId") int orderId) {
		AddToCart addToCart = addToCartService.getOrderId(orderId);
		return new ModelAndView("updateOrder", "uOrderDetails", addToCart);
	}

	// mapping to updateORDER
	@RequestMapping("/updateORDER")
	public String updateOrder(@ModelAttribute("uOrderDetails") AddToCart cart, HttpSession session) {
		User u = (User) session.getAttribute("user");
		addToCartService.saveOrder(cart);
		System.out.println("order update*");
		return "redirect:/addToCart-DashBoard";
	}

	// mapping to order items
	@RequestMapping("/orderNow")
	public ModelAndView orderNow(HttpSession session) {
		User u = (User) session.getAttribute("user");
		int price = cartRep.getTotalPrice(u);
		System.out.println("tp-->" + cartRep.getTotalPrice(u));

		ModelAndView model = new ModelAndView();
		model.addObject("userDetails", u);
		model.addObject("price", price);
		model.setViewName("CheckOut");
		return model;

	}
}
