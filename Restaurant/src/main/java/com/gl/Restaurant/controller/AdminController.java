package com.gl.Restaurant.controller;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.gl.Restaurant.Repositories.AddToCartRepositories;
import com.gl.Restaurant.Repositories.AdminRepositories;
import com.gl.Restaurant.model.AddToCart;
import com.gl.Restaurant.model.Admin;
import com.gl.Restaurant.model.User;
import com.gl.Restaurant.service.AddToCartService;
import com.gl.Restaurant.service.UserCrudService;

@Controller
public class AdminController {
	@Autowired // getting object of
	AdminRepositories adminRep;

	@Autowired // getting object of AddToCartService
	AddToCartService cartService;

	@Autowired // getting object of AddToCartRepositories
	AddToCartRepositories cartRep;

	@Autowired // getting object of userCrudservice
	UserCrudService userService;

	// mapping to adminDashBoard
	@RequestMapping("/adminDashBoard")
	public ModelAndView getadminDashBoard() {
		return new ModelAndView("adminDashBoard");
	}

	// mapping to login page
	@RequestMapping("/adminLogin")
	public ModelAndView getAdminLoginPage(@ModelAttribute("LoginErrorMessage") String errorMessage) {
		ModelAndView modelAndView = new ModelAndView("adminLoginPage", "adminLoginData", new Admin());
		if (errorMessage == null) {
			return modelAndView;
		} else {
			modelAndView.addObject("LoginErrorMessage", errorMessage);
			return modelAndView;
		}
	}

	// mapping to validateAdminLogin
	@RequestMapping("/validateAdminLogin")
	public ModelAndView validateAdmin(@ModelAttribute("adminLoginData") Admin a, HttpSession session) {
		Optional<Admin> adminList = adminRep.adminValidation(a.getEmail(), a.getPassword());
		Admin aFind = null;
		if (adminList.isPresent()) {
			System.out.println("Admin  found");
			aFind = adminList.get();
			return new ModelAndView("redirect:/adminDashBoard");
		} else {
			System.out.println("Admin not found");
			return new ModelAndView("adminLoginPage", "LoginErrorMessage", "Invalid username or password");
		}
	}

	// mapping to get Registration_Form
	@RequestMapping("/adminRegistration")
	public ModelAndView getRegistration_Form() {

		return new ModelAndView("adminRPage", "registerData", new Admin());
	}

	// mapping to save Register data
	@RequestMapping("/dminRegister")
	public ModelAndView saveRegistrtionData(@ModelAttribute("registerData") Admin a) {
		adminRep.save(a);
		return new ModelAndView("redirect:/adminLogin");
	}

	// mapping to logout
	@RequestMapping("/adminLogout")
	public String adminLogout(HttpSession session) {
		session.invalidate();
		return "redirect:/adminLogin";
	}

	// mapping to backTotoAdminDashBoard
	@RequestMapping("/backTotoAdminDashBoard")
	public String backTotoAdminDashBoard() {
		return "redirect:/adminDashBoard";
	}

	// mapping to todayBills
	@RequestMapping("/todayBills")
	public ModelAndView todayBills() {
		List<AddToCart> todayBill = cartRep.getTodayBill(LocalDate.now());
		
		int tBill = 0;
		for (AddToCart a : todayBill) {
			tBill = tBill + (a.getPrize() * a.getQuntity());
		}
		System.out.println("TodayBill==" + tBill);
		ModelAndView model = new ModelAndView();
		model.addObject("tBill", tBill);
		model.addObject("cart", todayBill);
		model.setViewName("TodayBills");
		return model;

	}

	// mapping to monthlyBills
	@RequestMapping("/monthlyBills")
	public ModelAndView monthlyBills() {
		Date date = new Date();
		LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		int month = localDate.getMonthValue();
		List<AddToCart> cart = cartRep.getMonthlyBills(month);
		int tbill = 0;
		for (AddToCart a : cart) {

			tbill = tbill + (a.getPrize() * a.getQuntity());

		}
		System.out.println("1." + cart.toString());
		System.out.println("2." + month);
		System.out.println("3." + tbill);
		ModelAndView model = new ModelAndView();
		model.addObject("tBill", tbill);
		model.addObject("cart", cart);
		model.setViewName("TodayBills");
		return model;

	}

	// mapping to get orderDoneBySpecificUser
	@RequestMapping("/orderDoneBySpecificUser")
	public ModelAndView orderDoneBySpecificUser(HttpSession session) {
		return new ModelAndView("Form", "data", new AddToCart());
	}

	@RequestMapping("/check")
	public ModelAndView getData(@ModelAttribute("data") AddToCart addToCart) {
		List<AddToCart> addtocart = cartRep.ordersBySpecificUser(addToCart.getUserid().getUserId());
		
		int tbill = 0;
		for (AddToCart a : addtocart) {
			System.out.println("iddddd***="+a.getUserid().getFirstName());
			tbill = tbill + (a.getPrize() * a.getQuntity());
		}

		System.out.println("1. " + addtocart.toString());
		System.out.println("3. " + tbill);
		ModelAndView model = new ModelAndView();
		model.addObject("tbill", tbill);
		model.addObject("addtocart", addtocart);
		model.setViewName("SpecificUserPrice");
		return model;

	}

	// insert into admin values(1,"admin@gmail.com","admin","1","admin");

}
