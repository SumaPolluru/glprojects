package com.gl.Restaurant.Repositories;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.gl.Restaurant.model.User;

@Repository
public interface UserRepositories extends JpaRepository<User,Integer> {

	// hibernate query to validate user login
	@Query("select u from User u where u.email=?1 and u.password=?2 ")
	public Optional<User> userValidation(String email, String password);
	
	// hibernate query to update user 
	@Modifying
	@Query("update User a set firstName=?1,lastName=?2,email=?3,password=?4 where userId=?5")
	public void updateu(String fn,String ln,String e,String pass,int userid);
}
