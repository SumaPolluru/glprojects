package com.gl.Restaurant.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gl.Restaurant.model.Items;

@Repository // that indicates that the present class is a repository
public interface ItemRepositories extends JpaRepository<Items, Integer> {

}
