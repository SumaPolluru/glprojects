package com.app.RestaurantBillingSystemUsingSpringBoot.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "CartDetails")
public class CartDetails 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int cartId;
       
	public int getCartId() 
	{
		return cartId;
	}

	public void setCartId(int cartId) 
	{
		this.cartId = cartId;
	}

	@ManyToOne
	private User user;

	public User getUser() 
	{
		return user;
	}

	public void setUser(User user) 
	{
		this.user= user;
	}


	public java.util.List<String> getUserIdlist() 
	{
		return userIdlist;
	}

	public void setUserIdlist(java.util.List<String> userIdlist) 
	{
		this.userIdlist = userIdlist;
	}

	@Transient
	private java.util.List<String> userIdlist;
	
	
	@ManyToOne
	private Menu menu;

	public Menu getMenu() 
	{
		return menu;
	}

	public void setMenu(Menu menu) 
	{
		this.menu= menu;
	}

	public java.util.List<String> getDishNamelist() 
	{
		return dishNamelist;
	}

	public void setDishNamelist(java.util.List<String> dishNamelist) 
	{
		this.dishNamelist = dishNamelist;
	}

	@Transient
	private java.util.List<String> dishNamelist;

}
