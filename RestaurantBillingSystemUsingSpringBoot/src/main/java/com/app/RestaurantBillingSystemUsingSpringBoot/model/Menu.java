package com.app.RestaurantBillingSystemUsingSpringBoot.model;


import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Menu")
public class Menu 
{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int dishId;
	private String dishName;
	private double pricePerPlate;
	private int quantityInPlates;
	private LocalDate date;

	public Menu() 
	{

	}

	public Menu(String dishName, double pricePerPlate) 
	{

		this.dishName = dishName;
		this.pricePerPlate = pricePerPlate;
	}

	public Menu(String dishName, double pricePerPlate, int quantityInPlates, LocalDate date) 
	{
		super();
		this.dishName = dishName;
		this.pricePerPlate = pricePerPlate;
		this.quantityInPlates = quantityInPlates;
		this.date = date;
	}

	public int getDishId() 
	{
		return dishId;
	}

	public void setDishId(int dishId) 
	{
		this.dishId = dishId;
	}

	public String getDishName() 
	{
		return dishName;
	}

	public void setDishName(String dishName) 
	{
		this.dishName = dishName;
	}

	public double getPricePerPlate() 
	{
		return pricePerPlate;
	}

	public void setPricePerPlate(double pricePerPlate)
	{
		this.pricePerPlate = pricePerPlate;
	}

	public int getQuantityInPlates() 
	{
		return quantityInPlates;
	}

	public void setQuantityInPlates(int quantityInPlates) 
	{
		this.quantityInPlates = quantityInPlates;
	}

	public LocalDate getDate()
	{
		this.date = LocalDate.now();
		return date;
	}

	public void setDate(LocalDate date) 
	{
		this.date = date;
	}

	@Override
	public String toString() 
	{
		return "DishId: " + dishId + " \nDishName: " + dishName + " \nPricePerPlate: " + pricePerPlate ;
	}

	

}
