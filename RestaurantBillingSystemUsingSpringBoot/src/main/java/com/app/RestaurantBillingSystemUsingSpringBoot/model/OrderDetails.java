package com.app.RestaurantBillingSystemUsingSpringBoot.model;

import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name = "OrderDetails")
public class OrderDetails 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int orderId;
	private int noOfPlates;
	private LocalDate orderedDate;
	private double price;

	
	public double getPrice() 
	{
		return price;
	}
	public void setPrice(double price)
	{
		this.price = price;
	}
	public OrderDetails() 
	{
		
	}
   
	public OrderDetails( int noOfPlates, LocalDate orderedDate, double price) 
	{
		super();
		this.noOfPlates = noOfPlates;
		this.orderedDate = orderedDate;
		this.price = price;
	}
	
	public int getOrderId() 
	{
		return orderId;
	}

	public void setOrderId(int orderId)
	{
		this.orderId = orderId;
	}

	public int getNoOfPlates() 
	{
		return noOfPlates;
	}

	public void setNoOfPlates(int noOfPlates)
	{
		this.noOfPlates = noOfPlates;
	}

	public LocalDate getOrderedDate() 
	{
	   this.orderedDate = LocalDate.now();
		return orderedDate;
	}

	public void setOrderedDate(LocalDate orderedDate) 
	{
		this.orderedDate = LocalDate.now();
	}

	@ManyToOne
	private User user;

	public User getUser() 
	{
		return user;
	}

	public void setUser(User user)
	{
		this.user= user;
	}

	public java.util.List<String> getUserIdlist()
	{
		return userIdlist;
	}

	public void setUserIdlist(java.util.List<String> userIdlist)
	{
		this.userIdlist = userIdlist;
	}

	@Transient
	private java.util.List<String> userIdlist;
	
	
	@ManyToOne
	private Menu menu;

	public Menu getMenu() 
	{
		return menu;
	}

	public void setMenu(Menu menu) 
	{
		this.menu= menu;
	}

	public java.util.List<String> getDishNamelist() 
	{
		return dishNamelist;
	}

	public void setDishNamelist(java.util.List<String> dishNamelist)
	{
		this.dishNamelist = dishNamelist;
	}

	@Transient
	private java.util.List<String> dishNamelist;

}
