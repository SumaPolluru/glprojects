package com.app.RestaurantBillingSystemUsingSpringBoot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2 //Swagger 
public class RestaurantBillingSystemUsingSpringBootApplication 
{

	public static void main(String[] args) 
	{
		SpringApplication.run(RestaurantBillingSystemUsingSpringBootApplication.class, args);
	}

	@Bean
	public Docket restaurantApi() 
	{
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.app.RestaurantBillingSystemUsingSpringBoot")).build();
	}

}
