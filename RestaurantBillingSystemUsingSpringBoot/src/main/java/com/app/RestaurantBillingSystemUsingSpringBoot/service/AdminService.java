package com.app.RestaurantBillingSystemUsingSpringBoot.service;

import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import com.app.RestaurantBillingSystemUsingSpringBoot.model.Admin;
import com.app.RestaurantBillingSystemUsingSpringBoot.repostories.AdminRepository;

@Service
@Transactional
public class AdminService 
{
	private AdminRepository adminRepository;
	
	public AdminService(AdminRepository adminRepository) 
	{
		this.adminRepository=adminRepository;
	}
	
	public void saveAdmin(Admin admin ) //Save Data
	{
		adminRepository.save(admin);
	}
	
	//login
	public Admin findByAdminNameAndAdminPassword(String adminName, String adminPassword) 
	{
		return adminRepository.findByAdminNameAndAdminPassword(adminName, adminPassword);
	}

	//register credentials checking
	public Admin findByAdminName(String adminName)
	{
		return adminRepository.findByAdminName(adminName);
	}
	
 }
