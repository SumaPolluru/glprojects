package com.app.RestaurantBillingSystemUsingSpringBoot.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import com.app.RestaurantBillingSystemUsingSpringBoot.model.User;
import com.app.RestaurantBillingSystemUsingSpringBoot.repostories.UserRepository;

@Service
@Transactional
public class UserService {
	
	private UserRepository userRepository;
	
	public UserService(UserRepository userRepository) {
		this.userRepository=userRepository;
	}
	
	//Business Logic for CRUD Operations On User
	public void saveMyUser(User user ) {
		userRepository.save(user);
	}
	
	public User findByUserIdAndPassword(String userId, String password) {
		return userRepository.findByUserIdAndPassword(userId, password);
	}
	public List<User> showAllUsers(){
		List<User> users = new ArrayList<User>();
		for(User user : userRepository.findAll()) {
			users.add(user);
		}
		
		return users;
	}
	
	public void deleteMyUser(String userId) {
		userRepository.deleteById(userId);
	}
	
	public User editUser(String userId) {
		Optional<User> uobj=userRepository.findById(userId);
		return uobj.get();
	}

	public User findByUserId(String userId) {
		return userRepository.findByUserId(userId);
	}
	}

	