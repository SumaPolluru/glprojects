package com.app.RestaurantBillingSystemUsingSpringBoot.service;

import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

import com.app.RestaurantBillingSystemUsingSpringBoot.model.CartDetails;
import com.app.RestaurantBillingSystemUsingSpringBoot.repostories.CartRepository;

@Service
@Transactional
public class CartService 
{

	private CartRepository cartRepository;

	public CartService(CartRepository cartRepository) 
	{
		this.cartRepository = cartRepository;
	}

	//Saving Cart Details
	public void saveCart(CartDetails cart) 
	{
		cartRepository.save(cart);
	}
}
