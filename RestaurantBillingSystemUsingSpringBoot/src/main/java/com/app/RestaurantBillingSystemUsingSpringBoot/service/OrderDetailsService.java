package com.app.RestaurantBillingSystemUsingSpringBoot.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import com.app.RestaurantBillingSystemUsingSpringBoot.model.OrderDetails;
import com.app.RestaurantBillingSystemUsingSpringBoot.repostories.OrderDetailsRepository;

@Service
@Transactional
public class OrderDetailsService
{

	private OrderDetailsRepository orderRepository;
	
	public OrderDetailsService(OrderDetailsRepository orderRepository) 
	{
		this.orderRepository=orderRepository;
	}
	
	//Business Logic for CRUD Operations On Order
	public void saveOrder(OrderDetails order) 
	{
		orderRepository.save(order);
	}
	
	public OrderDetails getOrder(int orderId) 
	{
		Optional<OrderDetails> uobj=orderRepository.findById(orderId);
		return uobj.get();
	}
	
	public List<OrderDetails> findByOrderedDate(LocalDate orderedDate) 
	{
		return orderRepository.findByOrderedDate(orderedDate);
	}
	
	public List<OrderDetails> showOrders()
	{
		List<OrderDetails> mobj = new ArrayList<OrderDetails>();
		for(OrderDetails order : orderRepository.findAll())
		{
			mobj.add(order);
		}
		return mobj;
	}
	
	public void deleteOrderById(int orderId) 
	{
		orderRepository.deleteById(orderId);
	}
	
}
