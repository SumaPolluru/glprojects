package com.app.RestaurantBillingSystemUsingSpringBoot.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import com.app.RestaurantBillingSystemUsingSpringBoot.model.Menu;
import com.app.RestaurantBillingSystemUsingSpringBoot.repostories.MenuRepository;


@Service
@Transactional
public class MenuService 
{

	
	private MenuRepository menuRepository;
	
	public MenuService(MenuRepository menuRepository) 
	{
		this.menuRepository=menuRepository;
	}
	
	//Business Logic For CRUD Operations on Menu
	public void saveMenu(Menu menu ) 
	{
		menuRepository.save(menu);
	}

	public List<Menu> showMenu()
	{
		List<Menu> mobj = new ArrayList<Menu>();
		for(Menu menu : menuRepository.findAll()) 
		{
			mobj.add(menu);
		}
		return mobj;
	}
	
	public void deleteMenuById(int dishId) 
	{
		menuRepository.deleteById(dishId);
	}
	
	public Menu editMenuDetails(int dishId) 
	{
		Optional<Menu> mobj=menuRepository.findById(dishId);
		return mobj.isPresent() ? mobj.get() : null;
	}
	
}
