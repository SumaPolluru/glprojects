package com.app.RestaurantBillingSystemUsingSpringBoot.controller;

import java.time.LocalDate;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.app.RestaurantBillingSystemUsingSpringBoot.model.Menu;
import com.app.RestaurantBillingSystemUsingSpringBoot.repostories.MenuRepository;
import com.app.RestaurantBillingSystemUsingSpringBoot.service.MenuService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Controller
public class MenuController 
{

	@Autowired
	MenuService menuService;

	@Autowired
	private MenuRepository menuRepository;
	
    //Swagger
	@ApiOperation(value = "Menu List", notes = "This method will retreive list from database", nickname = "getMenu")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Server error"),
	@ApiResponse(code = 404, message = "Dish not found"),
	@ApiResponse(code = 200, message = "Successful retrieval", response = Menu.class, responseContainer = "List") })

	//Menu CRUD Operations
	@RequestMapping("/addMenu")
	public String addMenuDetails(HttpServletRequest request)
	{
		request.setAttribute("menuobj", "ADD_MENU");
		return "adminWelcomePage";
	}

	@PostMapping("/save-menu")
	public String menuDetails(@ModelAttribute Menu menu, BindingResult bindingResult, HttpServletRequest request) 
	{
		menuService.saveMenu(menu);
		request.setAttribute("menuobj", "ADMIN_HOME");
		return "adminWelcomePage";
	}

	@GetMapping("/saveMenu")
	public String saveMenuDate(@RequestParam String dishName, @RequestParam double pricePerPlate,
			@RequestParam int quantityInPlates) 
	{
		LocalDate date=LocalDate.now();
		Menu menu = new Menu(dishName, pricePerPlate, quantityInPlates, date);
		menuService.saveMenu(menu);
		return "Menu Saved";
	}

	@GetMapping("/show-menu")
	public String showMenuData(HttpServletRequest request)
	{
		request.setAttribute("menus", menuService.showMenu());
		request.setAttribute("menuobj", "MENU");
		return "adminWelcomePage";
	}

	@RequestMapping("/delete-menu")
	public String deleteMenu(@RequestParam int dishId, HttpServletRequest request) 
	{
		menuService.deleteMenuById(dishId);
		request.setAttribute("menus", menuService.showMenu());
		request.setAttribute("menuobj", "MENU");
		return "adminWelcomePage";
	}

	@RequestMapping("/edit-menu")
	public String editMenu(@RequestParam int dishId, HttpServletRequest request)
	{
		request.setAttribute("menu", menuService.editMenuDetails(dishId));
		request.setAttribute("menuobj", "MENU-UPDATE");
		return "adminWelcomePage";
	}

	@RequestMapping("updateMenu")
	public ModelAndView saveUpdate(@ModelAttribute("menuobj") Menu menu) 
	{
		menuRepository.save(menu);
		return new ModelAndView("adminWelcomePage");
	}

	//Today Menu
	@RequestMapping("/todayMenu")
	public ModelAndView getDetails(HttpServletRequest request) 
	{
		LocalDate date=LocalDate.now();
			List<Menu> menulist = menuRepository.getTodayMenu(date);
			request.setAttribute("menuobj", "TODAY-MENU");
			return new ModelAndView("userOperations", "menudata", menulist);
	}
}

