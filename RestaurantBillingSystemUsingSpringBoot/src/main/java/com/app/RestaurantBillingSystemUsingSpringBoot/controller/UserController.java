package com.app.RestaurantBillingSystemUsingSpringBoot.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.app.RestaurantBillingSystemUsingSpringBoot.model.User;
import com.app.RestaurantBillingSystemUsingSpringBoot.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Controller
public class UserController 
{

	@Autowired
	UserService userService;
	
	//Swagger
	@ApiOperation(value = "User List", notes = "This method will retreive list from database", nickname = "getUser")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Server error"),
	@ApiResponse(code = 404, message = "User not found"),
	@ApiResponse(code = 200, message = "Successful retrieval", response = User.class, responseContainer = "List") })

	@RequestMapping("/welcome")
	public String Welcome(HttpServletRequest request) 
	{
		request.setAttribute("mode", "MODE_HOME");
		return "welcomepage";
	}

	@RequestMapping("/register")   //User Register
	public String registration(HttpServletRequest request)
	{
		request.setAttribute("mode", "MODE_REGISTER");
		return "welcomepage";
	}

	@PostMapping("/save-user")   //Checking Register Credentials,The User is Present or Not
	public String registerUser(@ModelAttribute User user, BindingResult bindingResult, HttpServletRequest request)
	{
	if (userService.findByUserId(user.getUserId()) != null) 
	{
		request.setAttribute("error", "UserId already Exist");
		request.setAttribute("mode", "MODE_REGISTER");
		return "welcomepage";
	}
	else
	{
		userService.saveMyUser(user);
		request.setAttribute("mode", "MODE_HOME");
		return "welcomepage";
	}
}

	@RequestMapping("/login")
	public String login(HttpServletRequest request) 
	{
		request.setAttribute("mode", "MODE_LOGIN");
		return "welcomepage";
	}

	@RequestMapping("/login-user")  //Checking Login Credentials
	public String loginUser(@ModelAttribute User user, HttpServletRequest request) 
	{
		if (userService.findByUserIdAndPassword(user.getUserId(), user.getPassword()) != null)
		{
			return "userOperations";
		}
		else
		{
			request.setAttribute("error", "Invalid Username or Password");
			request.setAttribute("mode", "MODE_LOGIN");
			return "welcomepage";

		}
	}

	//Logout
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpServletRequest request) 
	{
		HttpSession httpSession = request.getSession();
		httpSession.invalidate();
		return "redirect:/";
	}

	
	@GetMapping("/saveuser")  //Saving Registration Data
	public String saveUser(@RequestParam String userId, @RequestParam String firstname, @RequestParam String lastname,
			@RequestParam String mobileNo, @RequestParam String address, @RequestParam String password)
	{
		User user = new User(userId, firstname, lastname, mobileNo, address, password);
		userService.saveMyUser(user);
		return "User Saved";
	}
	
	//User Home
	@RequestMapping("/userHome")
	public String Admin(HttpServletRequest request)
	{
		request.setAttribute("userobj", "HOME");
		return "userOperations";
	}
}