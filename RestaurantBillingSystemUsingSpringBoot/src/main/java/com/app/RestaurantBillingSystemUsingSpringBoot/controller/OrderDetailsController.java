package com.app.RestaurantBillingSystemUsingSpringBoot.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.app.RestaurantBillingSystemUsingSpringBoot.model.CartDetails;
import com.app.RestaurantBillingSystemUsingSpringBoot.model.OrderDetails;
import com.app.RestaurantBillingSystemUsingSpringBoot.model.User;
import com.app.RestaurantBillingSystemUsingSpringBoot.repostories.CartRepository;
import com.app.RestaurantBillingSystemUsingSpringBoot.repostories.MenuRepository;
import com.app.RestaurantBillingSystemUsingSpringBoot.repostories.OrderDetailsRepository;
import com.app.RestaurantBillingSystemUsingSpringBoot.repostories.UserRepository;
import com.app.RestaurantBillingSystemUsingSpringBoot.service.CartService;
import com.app.RestaurantBillingSystemUsingSpringBoot.service.OrderDetailsService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Controller
public class OrderDetailsController 
{

	@Autowired
	OrderDetailsService orderDetailsService;
	
	@Autowired
	CartService cartService;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private OrderDetailsRepository orderRepository;
	
	@Autowired
	private CartRepository cartRepository;
	
	@Autowired
	private MenuRepository menuRepository;
	
	@ApiOperation(value = "OrderDetails List", notes = "This method will retreive list from database", nickname = "getOrderDetails")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Server error"),
	@ApiResponse(code = 404, message = "OrderDetails not found"),
	@ApiResponse(code = 200, message = "Successful retrieval", response = OrderDetails.class, responseContainer = "List") })

	//Order Operation
	@RequestMapping("/order")
	public ModelAndView addOrderDetails(HttpServletRequest request) 
	{
		List<String> userIdlist = userRepository.getUser();
		List<String> dishNamelist = menuRepository.getMenu();
		OrderDetails orobj = new OrderDetails();
		orobj.setUserIdlist(userIdlist);
		orobj.setDishNamelist(dishNamelist);
		return new ModelAndView("orderpage", "orderobj", orobj);
	}

	@PostMapping("/save-order")
	public String menuDetails(@ModelAttribute OrderDetails order, BindingResult bindingResult,
			HttpServletRequest request) 
	{
		orderDetailsService.saveOrder(order);
		return "userOperations";
	}

	@GetMapping("/saveOrder")
	public String saveMenuDate(@RequestParam int noOfPlates,
			@RequestParam LocalDate orderedDate, @RequestParam double price)
	{
		OrderDetails order = new OrderDetails(noOfPlates, orderedDate, price);
		orderDetailsService.saveOrder(order);
		return "Order Saved";

	}
 
	//Retrieve Orders
	@RequestMapping("/RetrieveDetails")
	public ModelAndView getDetails(HttpServletRequest request) 
	{
		List<OrderDetails> orderlist = orderRepository.findAll();
		request.setAttribute("orderobj", "ALL_ORDERES");
		return new ModelAndView("adminWelcomePage", "orderdata", orderlist);
	}

	@RequestMapping("/delete-order")
	public String deleteOrder(@RequestParam int orderId, HttpServletRequest request) 
	{
		orderDetailsService.deleteOrderById(orderId);
		request.setAttribute("orders", orderDetailsService.showOrders());
		request.setAttribute("orderobj", "ORDER");
		return "adminWelcomePage";
	}
	
	//Cart
	@RequestMapping("/addToCart") 
	public ModelAndView addToFavorite(HttpServletRequest request) 
	{
		List<String> userIdlist = userRepository.getUser();
		List<String> dishNamelist = menuRepository.getMenu();
		CartDetails orobj = new CartDetails();
		orobj.setUserIdlist(userIdlist);
		orobj.setDishNamelist(dishNamelist);
		return new ModelAndView("cartForm", "orderobj", orobj);
	}
	
	@PostMapping("/save-cart")
	public String CartDetails(@ModelAttribute CartDetails cart, BindingResult bindingResult)
	{
		cartService.saveCart(cart);
		return "userOperations";
	}

	@GetMapping("/saveCartDetails")
	public String saveCartDetails(HttpServletRequest request)
	{
		CartDetails cart = new CartDetails();
		cartService.saveCart(cart);
		request.setAttribute("userobj", "CART-SUCCESS");
		return "userOperations";
	}

	//Total Bill Calculation
	@RequestMapping("/TotalBill")
	public ModelAndView getOrderForm(HttpServletRequest request) 
	{
		List<String> userIdlist = userRepository.getUser();
		OrderDetails orobj = new OrderDetails();
		orobj.setUserIdlist(userIdlist);
		return new ModelAndView("totalBillUserId", "orderobj", orobj);
	}

	@RequestMapping("/finalBill")
	public ModelAndView finalBill(@RequestParam User user,HttpServletRequest request) throws IOException 
	{
		LocalDate orderedDate=LocalDate.now();
		List<OrderDetails> orderlist = orderRepository.getOrderList(user, orderedDate);
		request.setAttribute("orderobj", "USER_ORDERES");
		return new ModelAndView("userOperations", "orderdata", orderlist);

	}
	
	//Specific User Orders
	@RequestMapping("/userOrders")
	public ModelAndView getUserIdForm() 
	{
		List<String> userIdlist = userRepository.getUser();
		OrderDetails orobj = new OrderDetails();
		orobj.setUserIdlist(userIdlist);
		return new ModelAndView("userIdHistoryForm", "orderobj", orobj);
	}

	@RequestMapping("/orderHistory")
	public ModelAndView getOrderHistory(@RequestParam User user, HttpServletRequest request, HttpServletResponse response)
			throws IOException 
	{
		List<OrderDetails> orderlist = orderRepository.getOrderHistory(user);
		request.setAttribute("orderobj", "USER_ORDERES");
		return new ModelAndView("userOperations", "orderdata", orderlist);
	}
	
	//Retrieving Cart Details
	@RequestMapping("/cartDetails")
	public ModelAndView getCartForm() 
	{
		List<String> userIdlist = userRepository.getUser();
		CartDetails orobj = new CartDetails();
		orobj.setUserIdlist(userIdlist);
		return new ModelAndView("userIdCartForm", "orderobj", orobj);
	}

	@RequestMapping("/userCart")
	public ModelAndView getCart(@RequestParam User user, HttpServletRequest request, HttpServletResponse response)
			throws IOException 
	{
		List<CartDetails> orderlist = cartRepository.getUserCart(user);
		request.setAttribute("orderobj", "USER-CART");
		return new ModelAndView("userOperations", "orderdata", orderlist);
	}

	//Specific User Order History For Admin
	@RequestMapping("/history")
	public ModelAndView getUserForm()
	{
		List<String> userIdlist = userRepository.getUser();
		OrderDetails orobj = new OrderDetails();
		orobj.setUserIdlist(userIdlist);
		return new ModelAndView("userIdForm", "orderobj", orobj);
	}

	@RequestMapping("/userHistory")
	public ModelAndView getHistory(@RequestParam User user, HttpServletRequest request, HttpServletResponse response)
			throws IOException 
	{
		List<OrderDetails> orderlist = orderRepository.getOrderHistory(user);
		request.setAttribute("orderobj", "ALL_ORDERES");
		return new ModelAndView("adminWelcomePage", "orderdata", orderlist);
	}

	//Today Bills Operation
	@RequestMapping("/todayBillsHistory")
	public ModelAndView getHistory(HttpServletRequest request) throws IOException 
	{
		LocalDate orderedDate = LocalDate.now();
		List<OrderDetails> orderlist = orderRepository.findByOrderedDate(orderedDate);
		request.setAttribute("orderobj", "ALL_ORDERES");
		return new ModelAndView("adminWelcomePage", "orderdata", orderlist);
	}

	//Total Sale of the Month Operation
	@RequestMapping("/totalSaleOfMonth")
	public ModelAndView getMonthSale(HttpServletRequest request) throws IOException
	{
		LocalDate firstDayOfMonth = LocalDate.now().with(TemporalAdjusters.firstDayOfMonth());
		LocalDate lastDayOfMonth = LocalDate.now().with(TemporalAdjusters.lastDayOfMonth());
		List<OrderDetails> orderlist = orderRepository.getSaleOfMonth(firstDayOfMonth, lastDayOfMonth);
		request.setAttribute("orderobj", "ALL_ORDERES");
		return new ModelAndView("adminWelcomePage", "orderdata", orderlist);

	}

}
