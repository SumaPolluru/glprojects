package com.app.RestaurantBillingSystemUsingSpringBoot.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.app.RestaurantBillingSystemUsingSpringBoot.model.Admin;
import com.app.RestaurantBillingSystemUsingSpringBoot.model.User;
import com.app.RestaurantBillingSystemUsingSpringBoot.repostories.UserRepository;
import com.app.RestaurantBillingSystemUsingSpringBoot.service.AdminService;
import com.app.RestaurantBillingSystemUsingSpringBoot.service.UserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Controller
public class AdminController
{

	@Autowired
	AdminService adminService;

	@Autowired
	UserService userService;

	@Autowired
	private UserRepository userRepository;
	
	//Swagger
	@ApiOperation(value = "Admin List", notes = "This method will retreive list from database", nickname = "getAdmin")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Server error"),
	@ApiResponse(code = 404, message = "Admin not found"),
	@ApiResponse(code = 200, message = "Successful retrieval", response = Admin.class, responseContainer = "List") })
	
	//Start 
	@RequestMapping("/")
	public String Welcome() 
	{
		return "restaurantHomepage";
	}


	@RequestMapping("/home")
	public String Welcome(HttpServletRequest request)
	{
		request.setAttribute("adminobj", "ADMIN_HOME");
		return "adminpage";
	}
	
	@RequestMapping("/registeration")
	public String registration(HttpServletRequest request)
	{
		request.setAttribute("adminobj", "ADMIN_REGISTER");
		return "adminpage";
	}

	@RequestMapping("/save-admin") //Registration,checking that admin is present or not
	public String registerUser(@ModelAttribute Admin admin, BindingResult bindingResult, HttpServletRequest request)
	{
		if (adminService.findByAdminName(admin.getAdminName()) != null) 
		{
			request.setAttribute("error", "AdminName already Exist");
			request.setAttribute("adminobj", "ADMIN_REGISTER");
			return "adminpage";
		}
		else 
		{
		adminService.saveAdmin(admin);
		request.setAttribute("adminobj", "ADMIN_HOME");
		return "adminpage";
  	    }
	}

	@RequestMapping("/adminLogin")
	public String login(HttpServletRequest request)
	{
		request.setAttribute("adminobj", "ADMIN_LOGIN");
		return "adminpage";
	}

	@RequestMapping("/login-admin")  //Checking Login Credentials
	public String loginUser(@ModelAttribute Admin admin, HttpServletRequest request) 
	{
		if (adminService.findByAdminNameAndAdminPassword(admin.getAdminName(), admin.getAdminPassword()) != null)
		{
			return "adminWelcomePage";
		}
		else
		{
			request.setAttribute("error", "Invalid AdminName or Password");
			request.setAttribute("adminobj", "ADMIN_LOGIN");
			return "adminpage";

		}
	}

	@RequestMapping("/saveadmin")  //Here, Registration data should store
	public String saveUser(@RequestParam String adminName, @RequestParam String adminPassword) 
	{
			Admin admin = new Admin(adminName,adminPassword);
			adminService.saveAdmin(admin);
			return "Admin Saved";
	}
	
	@RequestMapping("/adminHome")
	public String home(@ModelAttribute Admin admin, BindingResult bindingResult, HttpServletRequest request) 
	{
		adminService.saveAdmin(admin);
		request.setAttribute("adminobj", "HOME");
		return "adminWelcomePage";
	}
	
	@GetMapping("/show-users") //CRUD Operations on User
	public String showAllUsers(HttpServletRequest request) 
	{
		request.setAttribute("users", userService.showAllUsers());
		request.setAttribute("mode", "ALL_USERS");
		return "adminWelcomePage";
	}

	@RequestMapping("/delete-user")
	public String deleteUser(@RequestParam String userId, HttpServletRequest request) 
	{
		userService.deleteMyUser(userId);
		request.setAttribute("users", userService.showAllUsers());
		request.setAttribute("mode", "ALL_USERS");
		return "adminWelcomePage";
	}

	@RequestMapping("/edit-user")
	public String editUser(@RequestParam String userId, HttpServletRequest request) 
	{
		request.setAttribute("user", userService.editUser(userId));
		request.setAttribute("mode", "MODE_UPDATE");
		return "adminWelcomePage";
	}

	@RequestMapping("/saveUpdation")
	public ModelAndView saveUpdates(@ModelAttribute("mode") User user) 
	{
		userRepository.save(user);
		return new ModelAndView("adminWelcomePage");
	}

}
