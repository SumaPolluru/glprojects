package com.app.RestaurantBillingSystemUsingSpringBoot.repostories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.app.RestaurantBillingSystemUsingSpringBoot.model.Admin;

@Repository
public interface AdminRepository extends CrudRepository<Admin, Integer> 
{
   //Retrieving the data based on some properties
	public Admin findByAdminNameAndAdminPassword(String adminName, String adminPassword);

	public Admin findByAdminName(String adminName);

}
