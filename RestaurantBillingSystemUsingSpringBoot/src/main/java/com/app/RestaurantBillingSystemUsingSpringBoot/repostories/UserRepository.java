package com.app.RestaurantBillingSystemUsingSpringBoot.repostories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.app.RestaurantBillingSystemUsingSpringBoot.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, String> 
{
	//Queries
	public User findByUserIdAndPassword(String userId, String password);
	
	public User findByUserId(String userId);
	
	@Query("Select user.userId from User user")
	public List<String> getUser();

}
