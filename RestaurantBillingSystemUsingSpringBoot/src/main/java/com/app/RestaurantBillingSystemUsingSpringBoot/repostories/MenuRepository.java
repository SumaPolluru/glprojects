package com.app.RestaurantBillingSystemUsingSpringBoot.repostories;

import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.app.RestaurantBillingSystemUsingSpringBoot.model.Menu;

@Repository
public interface MenuRepository extends JpaRepository<Menu, Integer>
{
	//Queries
	@Query("Select menu.dishId from Menu menu")
	public List<String> getMenu();

	@Query("Select menu from Menu menu where date=?1")
	public List<Menu> getTodayMenu(LocalDate date);
	
}
