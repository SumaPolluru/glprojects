package com.app.RestaurantBillingSystemUsingSpringBoot.repostories;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.app.RestaurantBillingSystemUsingSpringBoot.model.CartDetails;
import com.app.RestaurantBillingSystemUsingSpringBoot.model.User;

@Repository
public interface CartRepository extends JpaRepository<CartDetails, Integer> 
{
   //Queries
	@Query("Select cart from CartDetails cart where cart.user=?1")
	public List<CartDetails> getUserCart(User user);
	
}
