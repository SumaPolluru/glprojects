package com.app.RestaurantBillingSystemUsingSpringBoot.repostories;

import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.app.RestaurantBillingSystemUsingSpringBoot.model.OrderDetails;
import com.app.RestaurantBillingSystemUsingSpringBoot.model.User;

@Repository
public interface OrderDetailsRepository extends JpaRepository<OrderDetails, Integer> 
{
	//Queries
	@Query("Select orderobj from OrderDetails orderobj")
	public List<OrderDetails> findAll();

	@Query("Select orderobj from OrderDetails orderobj where orderobj.user=?1 and orderobj.orderedDate=?2")
	public List<OrderDetails> getOrderList(User user,LocalDate orderedDate);

	@Query("Select orderobj from OrderDetails orderobj where orderobj.user=?1")
	public List<OrderDetails> getOrderHistory(User user);
	
	public List<OrderDetails> findByOrderedDate(LocalDate orderedDate);
	
	@Query("Select orderobj from OrderDetails orderobj where orderobj.orderedDate between ?1 and ?2")
	public List<OrderDetails> getSaleOfMonth(LocalDate firstDayOfMonth, LocalDate lastDayOfMonth);

}
