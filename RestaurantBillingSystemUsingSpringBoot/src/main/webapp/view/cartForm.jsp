<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<title>Cart Form</title>
</head>
<style>
body {
	background-color: #E8E8E8;
	text-size: 5px;
}
</style>

<body>
<center>
	<h1>Add To Cart</h1>
	<form:form method="POST" action="save-cart" modelAttribute="orderobj">
		<table>
			<tr><font color=Black size=5>
				UserId:</font>
				<form:select path="user">

					<form:options items="${orderobj.userIdlist}"  />

				</form:select>
			</tr>

			<tr><font color=Black size=5>
				Dish Id:</font>
				<form:select path="menu">

					<form:options items="${orderobj.dishNamelist}" />

				</form:select>
			</tr>
			<td colspan="2"><input type="submit" value="Add to Cart" /></td>
			</tr>
		</table>
	</form:form>
	</center>
</body>

</html>