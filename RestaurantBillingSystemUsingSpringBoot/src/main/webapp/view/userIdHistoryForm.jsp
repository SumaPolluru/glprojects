<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<title>User Form</title>
</head>
<style>
body {
	background-color: #E8E8E8;
	text-size: 5px;
}
</style>

<body>
<center>
	<h1>Order History</h1>
	<form:form method="POST" action="orderHistory" modelAttribute="orderobj">
		<table>
			<tr><font color=Black size=5>
				UserId:</font>
				<form:select path="user">

					<form:options items="${orderobj.userIdlist}" />

				</form:select>
			</tr>
			<td colspan="2"><input type="submit" value="Order History" /></td>
			</tr>
		</table>
	</form:form>
	</center>
</body>

</html>