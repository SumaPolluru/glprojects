<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<title>Order Page</title>
</head>
<style>
body {
	background-color: #E8E8E8;
	text-size: 5px;
}
td{
text-size: 5px;
}
</style>

<body>
	<h1>Place Order</h1>
	<form:form method="POST" action="save-order" modelAttribute="orderobj">
		<table>
			<tr><font color=Black size=5>
				Dish Id:</font>
				<form:select path="menu">

					<form:options items="${orderobj.dishNamelist}" />

				</form:select>
			</tr>
			<tr>
				<td><form:label path="noOfPlates">
						<font color=Black size=5>No Of Plates</font>
					</form:label></td>
				<td><form:input path="noOfPlates"/></td>
			</tr>
			<div class="form-group">
				<label class="control-label col-md-3"><font color=Black size=5>Date</font></label>
				<div class="col-md-7">
					<input type="date" class="form-control" name="orderedDate"
						value="${order.orderedDate}" required/>
				</div>
			</div>
			<tr>
				<td><form:label path="price">
						<font color=Black size=5>Price</font>
					</form:label></td>
				<td><form:input path="price" /></td>
			</tr>
			<tr><font color=Black size=5>
				UserId:</font>
				<form:select path="user">

					<form:options items="${orderobj.userIdlist}" />

				</form:select>
			</tr>
			<td colspan="2"><input type="submit" value="confirm order" /></td>
			</tr>
		</table>
	</form:form>
</body>

</html>