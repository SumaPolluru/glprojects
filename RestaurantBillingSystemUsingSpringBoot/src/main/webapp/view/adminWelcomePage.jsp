m<!DOCTYPE html >
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="sat, 01 Dec 2001 00:00:00 GMT">
<title>Welcome</title>
<link href="static/css/bootstrap.min.css" rel="stylesheet">
<link href="static/css/style.css" rel="stylesheet">

</head>
<style>
th {
	background-color: black;
	color:white;
	text-align: center;
	text-size:5px;
}
</style>
<body>
	<div role="navigation">
		<div class="navbar navbar-inverse">
			<a href="/adminHome" class="navbar-brand"><font color=white size=4px>Welcome-Admin</font></a>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="/show-users"><font color=white size=4px>Show Users</font></a></li>
					<li><a href="/show-menu"><font color=white size=4px>Show Menu</font></a></li>
					<li><a href="/RetrieveDetails"><font color=white size=4px>Show Orders</font></a></li>
					<li><a href="/todayBillsHistory"><font color=white size=4px>Today Bills</font></a></li>
					<li><a href="/history"><font color=white size=4px>Specific User History</font></a></li>
					<li><a href="/totalSaleOfMonth"><font color=white size=4px>Total Sale Of The Month</font></a></li>
					<li><a href="/logout"><font color=white size=4px>Logout</font></a></li>
				</ul>
			</div>
		</div>
	</div>
	<c:choose>
		<c:when test="${adminobj=='HOME' }">
			<div class="container" id="homediv">
				<div class="jumbotron text-center">
					<h2>
						<font size=8px>Welcome to Admin Operations Page</font>
					</h2>
					<h3>These Operations are Performed By Admin</h3>
				</div>
			</div>
		</c:when>

		<c:when test="${mode=='ALL_USERS' }">
			<div class="container text-center" id="tasksDiv">
				<h3>All Users</h3>
				<hr>
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>SNo.</th>
								<th>User Id</th>
								<th>First Name</th>
								<th>LastName</th>
								<th>MobileNo</th>
								<th>Address</th>
								<th>Delete</th>
								<th>Edit</th>
								<th>Bill</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="user" items="${users}" varStatus="status">
								<tr>
									<td>${status.index + 1}</td>
									<td>${user.userId}</td>
									<td>${user.firstName}</td>
									<td>${user.lastName}</td>
									<td>${user.mobileNo}</td>
									<td>${user.address}</td>
									<td><a href="/delete-user?userId=${user.userId }"><span
											class="glyphicon glyphicon-trash"></span></a></td>
									<td><a href="/edit-user?userId=${user.userId }"><span
											class="glyphicon glyphicon-pencil"></span></a></td>
									<td><a href="/userHistory?user=${user.userId}">Order History</a>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</c:when>
		
		<c:when test="${mode=='MODE_UPDATE' }">
			<div class="container text-center">
				<h3>Update User</h3>
				<hr>
				<form class="form-horizontal" method="POST" action="saveUpdation">
				<div class="form-group">
						<label class="control-label col-md-3"></label>
						<div class="col-md-7">
							<input type="hidden" class="form-control" name="userId"
								value="${user.userId }" placeholder="Email" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">First Name</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="firstName"
								value="${user.firstName }" placeholder="First Name" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Last Name</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="lastName"
								value="${user.lastName }" placeholder="Last Name" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">MobileNo</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="mobileNo"
								value="${user.mobileNo }" placeholder="Mobile No" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Address</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="address"
								value="${user.address }" placeholder="Address" required />
						</div>
					</div>
					<div class="form-group ">
						<input type="submit" class="btn btn-primary" value="Update" />
					</div>
				</form>
			</div>
		</c:when>

		<c:when test="${menuobj=='ADD_MENU' }">
			<div class="container text-center">
				<h3>Add Menu</h3>
				<hr>
				<form class="form-horizontal" method="POST" action="save-menu">
					<input type="hidden" name="dishId" value="${admin.dishId }" />
					<div class="form-group">
						<label class="control-label col-md-3">Dish Name</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="dishName"
								value="${menu.dishName }" placeholder="Dish Name" required />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">Price Per plate</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="pricePerPlate"
								value="${menu.pricePerPlate}" placeholder="Price" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Quantity In Plates</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="quantityInPlates"
								value="${menu.quantityInPlates}"
								placeholder="Quantity In Plates" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Date</label>
						<div class="col-md-7">
							<input type="date" class="form-control" name="date"
								value="${menu.date}" placeholder="Date" required />
						</div>
					</div>
					<div class="form-group ">
						<input type="submit" class="btn btn-primary" value="Add menu" />
					</div>
				</form>
			</div>
		</c:when>
		<c:when test="${menuobj=='MENU' }">
			<div class="container text-center" id="tasksDiv">
				<h3>Menu</h3>
				<hr>
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>SNo.</th>
								<th>Dish Id</th>
								<th>Dish Name</th>
								<th>Price Per Plate</th>
								<th>Quantity In Plates</th>
								<th>Delete</th>
								<th>Edit</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="menu" items="${menus}" varStatus="status">
								<tr>
									<td>${status.index + 1}</td>
									<td>${menu.dishId}</td>
									<td>${menu.dishName}</td>
									<td>${menu.pricePerPlate}</td>
									<td>${menu.quantityInPlates}</td>
									<td><a href="/delete-menu?dishId=${menu.dishId }"><span
											class="glyphicon glyphicon-trash"></span></a></td>
									<td><a href="/edit-menu?dishId=${menu.dishId }"><span
											class="glyphicon glyphicon-pencil"></span></a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<center>

				<a href="/addMenu"><font size=5px>Add Menu</font></a>

			</center>
		</c:when>
		<c:when test="${menuobj=='MENU-UPDATE' }">
			<div class="container text-center">
				<h3>Update Menu</h3>
				<hr>
				<form class="form-horizontal" method="POST" action="updateMenu">
					<input type="hidden" name="dishId" value="${menu.dishId }" />
					<div class="form-group">
						<label class="control-label col-md-3">Dish Name</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="dishName"
								value="${menu.dishName }" placeholder="Dish Name" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Price Per Plate</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="pricePerPlate"
								value="${menu.pricePerPlate }" placeholder="Price" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">QuantityInPlates</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="quantityInPlates"
								value="${menu.quantityInPlates }" placeholder="Quantity"
								required />
						</div>
					</div>
					<div class="form-group ">
						<input type="submit" class="btn btn-primary" value="Update" />
					</div>
				</form>
			</div>
		</c:when>
		<c:when test="${orderobj=='ALL_ORDERES' }">
			<div class="container text-center" id="tasksDiv">
				<h3>Orders</h3>
				<hr>
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>SNo.</th>
								<th>Order Id</th>
								<th>Ordered Dish Name</th>
								<th>Price</th>
								<th>No.Of Plates</th>
								<th>Ordered Date</th>
								<th>User Details</th>
								<th>Bill</th>
								<th>Sum of Sale</th>
								<th>Delete</th>

							</tr>
						</thead>
						<tbody>
							<c:forEach var="orderdata" items="${orderdata}"
								varStatus="status">
								<tr>
									<td>${status.index + 1}</td>
									<td>${orderdata.orderId}</td>
									<td>${orderdata.menu}</td>
									<td>${orderdata.price}</td>
									<td>${orderdata.noOfPlates}</td>
									<td>${orderdata.orderedDate}</td>
									<td>${orderdata.user}</td>
									<td>${bill=orderdata.price*orderdata.noOfPlates}</td>
									<td>${sum = bill+sum}</td>
									<td><a href="/delete-order?orderId=${orderdata.orderId }"><span
											class="glyphicon glyphicon-trash"></span></a></td>
								</tr>
							</c:forEach>
							<table>
								<h4>Total Sale</h4><tr><h4>${sum}</h4></tr>
								</table>
						</tbody>
					</table>
				</div>
			</div>
		</c:when>
			
	</c:choose>
	<script src="static/js/jquery-1.11.1.min.js"></script>
	<script src="static/js/bootstrap.min.js"></script>
</body>
</html>