<!DOCTYPE html >
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="sat, 01 Dec 2001 00:00:00 GMT">
<title>Welcome User</title>
<link href="static/css/bootstrap.min.css" rel="stylesheet">
<link href="static/css/style.css" rel="stylesheet">
</head>
<style>
th {
	background-color: black;
	color:white;
	text-align: center;
	text-size:5px;
}
</style>
<body>
	<div role="navigation">
		<div class="navbar navbar-inverse">
			<a href="/userHome" class="navbar-brand"><font color=white size=4px>Welcome-User</font></a>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="/todayMenu"><font color=white size=4px>Show Menu</font></a></li>
					<li><a href="/order"><font color=white size=4px>Place Order</font></a></li>
					<li><a href="/TotalBill"><font color=white size=4px>Final Bill</font></a></li>
					<li><a href="/userOrders"><font color=white size=4px>My Order History</font></a></li>
					<li><a href="/cartDetails"><font color=white size=4px>My Cart</font></a></li>
					<li><a href="/logout"><font color=white size=4px>Logout</font></a></li>
				</ul>
			</div>
		</div>
	</div>
	<c:choose>
		<c:when test="${userobj=='HOME' }">
			<div class="container" id="homediv">
				<div class="jumbotron text-center">
					<h2>
						<font size=8px>Welcome to User Operations Page</font>
					</h2>
					<h3>These Operations are Performed By User</h3>
				</div>
			</div>
		</c:when>

		<c:when test="${userobj=='CART-SUCCESS' }">
			<div class="container text-center">
				<h3>Successfully Add to Cart!!</h3>
				<hr>
				<form class="form-horizontal" method="POST" action="/show-menu">
					<div class="form-group ">
						<input type="submit" class="btn btn-primary" value="OK" />
					</div>
				</form>
			</div>
		</c:when>

		<c:when test="${menuobj=='TODAY-MENU' }">
			<div class="container text-center" id="tasksDiv">
				<h3>Today Menu</h3>
				<hr>
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>SNo.</th>
								<th>Dish Id</th>
								<th>Dish Name</th>
								<th>Price Per Plate</th>
								<th>Date</th>
								<th>Cart</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="menudata" items="${menudata}" varStatus="status">
								<tr>
									<td>${status.index + 1}</td>
									<td>${menudata.dishId}</td>
									<td>${menudata.dishName}</td>
									<td>${menudata.pricePerPlate}</td>
									<td>${menudata.date}</td>
									<td><a href="/addToCart">Add to Cart</a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
         </c:when>
         
			<c:when test="${orderobj=='USER_ORDERES' }">
				<div class="container text-center" id="tasksDiv">
					<h3>Orders</h3>
					<hr>
					<div class="table-responsive">
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>SNo.</th>
									<th>Order Id</th>
									<th>Dish Details</th>
									<th>Price</th>
									<th>No.Of Plates</th>
									<th>Ordered Date</th>
									<th>User Details</th>
									<th>Bill</th>
									<th>Sum of Bill</th>

								</tr>
							</thead>
							<tbody>
								<c:forEach var="orderdata" items="${orderdata}"
									varStatus="status">
									<tr>
										<td>${status.index + 1}</td>
										<td>${orderdata.orderId}</td>
										<td>${orderdata.menu}</td>
										<td>${orderdata.price}</td>
										<td>${orderdata.noOfPlates}</td>
										<td>${orderdata.orderedDate}</td>
										<td>${orderdata.user}</td>
										<td>${bill=orderdata.price*orderdata.noOfPlates}</td>
										<td>${sum = bill+sum}</td>
									</tr>
								</c:forEach>
								<table>
								<h4>Final Bill </h4><tr><h4>${sum}</h4></tr>
								</table>
							</tbody>
						</table>
					</div>
				</div>
			</c:when>
			
			<c:when test="${orderobj=='USER-CART' }">
				<div class="container text-center" id="tasksDiv">
					<h3>Cart Details</h3>
					<hr>
					<div class="table-responsive">
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>SNo.</th>
									<th>Cart Id</th>
									<th>Dish Details</th>
									<th>User Details</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="orderdata" items="${orderdata}"
									varStatus="status">
									<tr>
										<td>${status.index + 1}</td>
										<td>${orderdata.cartId}</td>
										<td>${orderdata.menu}</td>
										<td>${orderdata.user}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</c:when>
		
	</c:choose>
	

	<script src="static/js/jquery-1.11.1.min.js"></script>
	<script src="static/js/bootstrap.min.js"></script>
</body>
</html>