package com.example.LoginApplication.repositories;

import org.springframework.data.repository.CrudRepository;

import com.example.LoginApplication.model.User;

public interface UserRepository extends CrudRepository<User, Integer> {

	public User findByUsernameAndPassword(String username, String password);


}
